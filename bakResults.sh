#!/bin/bash -x
output=backup/results.`date +%s`.tar.gz
find . -name "*.out" -or -name "*.lp" -or -name "*.sol" -or -name "*.log" -or -name "*.dat" -or -name "*.png" | xargs tar czvf $output
# tar czvf $output *.out *.log --exclude="*.csv" config/
echo -e "\n'$output' created."
