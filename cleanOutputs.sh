#!/bin/bash -x
rm -f *.out *.lp *.sol *.log
find config/ -name "*.out" -or -name "*.lp" -or -name "*.sol" -or -name "*.log" -or -name "*.dat" -or -name "*.png" | xargs rm -f
find config/ -name "*LP_GREEDY" -or -name "*ILP_SIMPLE_MODEL" | xargs rm -rf
