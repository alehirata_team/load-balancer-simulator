#!/bin/bash

if [ $# -ne 2 ]; then
    echo -e "Usage: $0 <test path> <num repetitions> \n e.g. $0 config/test01\n"
    exit 1
fi

test_path=$1
num_repetitions=$2
test_id=`basename $test_path`
output_folder=$test_path
qos_factor=""
FLOAT_PRECISION=5

getCurQoSFactor(){
    qosOutput=$1
    if [ ! -f $qosOutput ]; then
        curQoS="0.0"
    else
        curQoS=`cat $qosOutput`
    fi
    echo "$curQoS"
}

updateQoSFactor(){
    qosOutput=$1
    testOutput=$2
    curQoS=`getCurQoSFactor $qosOutput`
    newQoS=`grep "Attended UE Percentage: " $testOutput | cut -d':' -f2 | cut -d' ' -f2`
    if [ "$(echo "$newQoS > $curQoS" | bc)" == "1" ]; then
        echo "$newQoS" > $qosOutput
    fi
}


#
# Main
#

for strategy in $(./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)"); do
    for i in `seq 1 $num_repetitions`; do
        idx=`printf %02d $i`
        echo "Strategy: $strategy"
        output=${output_folder}/${test_id}_${idx}_${strategy}.out
        qos_output=${output_folder}/${test_id}_${idx}_qos.out
        qos_factor=`getCurQoSFactor $qos_output`
        if [ `./listBinSearchStrategies.sh | grep -c "$strategy"` -eq 0 ]; then
            echo "time ./simulator.sh $strategy ${test_path}/${test_id}_${idx} $qos_factor > $output 2>&1"
            time ./simulator.sh $strategy ${test_path}/${test_id}_${idx} $qos_factor > $output 2>&1
            updateQoSFactor $qos_output $output
        else
            echo "time ./binSearch.sh $strategy ${test_path}/${test_id}_${idx} > ${output}_BINSEARCH.out 2>&1"
            time ./binSearch.sh $strategy ${test_path}/${test_id}_${idx} $output > ${output}_BINSEARCH.out 2>&1
        fi
        echo ""
        grep "### Statistics ###" -A1000 $output
        echo -e "\n--------------------------------------------------------------------------------\n"
    done
done
