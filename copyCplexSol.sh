#!/bin/bash

output=`echo $0 | sed 's/\.sh//g'`.out
script="plot.sh"
confidence_level=0.95
test_folder=config
output_folder=config

echo "Running $0"
echo "Start Time: `date`"

if [ -f "$output" ]; then
    rm -f "$output"
fi

testList=""
ndashes=$(grep -o "-" <<<"$(ls $test_folder | grep -v '\.properties' | grep simul | head -1)" | wc -l)

for i in `ls $test_folder | grep -v '\.properties' | grep simul | sort -n -t'-' -k $(expr $ndashes \+ 1)`; do
    testInput=$test_folder/$i
    testOutput=$output_folder/$i
    mkdir -p $testOutput
    for j in `ls $testInput/*BINSEARCH*`; do 
    	outFile=`grep cp $j | tail -1 | awk '{print $3}'`
    	outputFilePrefix=`dirname $outFile | xargs basename`
    	for k in .lp _cplex.sol; do
    		srcFile=$(ls `echo $outFile | sed "s/\.out/*$k/"`)
    		cp $srcFile $testOutput/$outputFilePrefix$k
        done
    done

    echo -e "\n================================================================================"
    ls $testOutput
    echo -e "================================================================================\n"
done

echo "$script: Done!" >> $output
echo "End Time: `date`"
echo "$0: Done!"
