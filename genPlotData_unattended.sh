#!/bin/bash

if [ $# -ne 2 ]; then
    echo -e "Usage: $0 <confidence level> <test path>"
    exit 1
fi

confidence_level=$1
test_path=$2
test_id=`basename $test_path`
output_dir=$test_path
UNATTENDED_UE_PREFFIX="Total Unattended Reachable UE"
#CELL_TYPE="MACRO PICO FEMTO"
CELL_TYPE="MACRO PICO"
STRATEGIES_LIST=`./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)"`
CALC_CONFINTERVAL="java -cp ./libs/*:bin util.ConfidenceIntervalApp"
PLOTDATAFILE_PREFFIX=$output_dir/$test_id

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

#
# Creating the Header
#

HEADER="Cell_Type"
for i in $CELL_TYPE; do
    HEADER=$HEADER" ${i} ${i}_CI"
done

datafile=${PLOTDATAFILE_PREFFIX}_unattended.dat
echo "$HEADER" > $datafile


#
# Fill in the datafile with mean and confidence interval for each strategy
#

for strategy in $STRATEGIES_LIST; do
    line="$strategy"
    stat=""
    for file in `$LS $output_dir/$test_id*${strategy}.out`; do
        val=`grep "${UNATTENDED_UE_PREFFIX}:" $file | cut -d':' -f2 | cut -d'(' -f1 | sed 's/\s//' | sed 's/,/\./'`
        if [ -n "$val" ]; then
            stat=$stat" $val"
        fi
    done
    #echo -e "DEBUG: $strategy, $cell_type, $stat"
    line=$line" `$CALC_CONFINTERVAL $confidence_level $stat`"
    echo $line >> $datafile
    #echo $line
done

sed -i 's/,/\./g' $datafile
