#!/bin/bash

MIN_VAL=1
MAX_VAL=100
BEST_SOLUTION=0

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

curOptimum=1

curPercAttendedUE=0
curMiddle=0


if [ $# -ne 3 ]; then
    echo -e "Usage: $0 <strategy> <test case> <final output>"
    echo -e "eg: $0 LP_GREEDY config/test_simul-04-03-10/test_simul-04-03-10_01"
    exit 1
fi

strategy=$1
testCase=$2
finalOutput=$3
outputDir=${testCase}_${strategy}


function fpcalc(){
    expr=$*
    echo "scale=5; $expr" | bc | sed 's/^\./0\./'
}

function createSymLinks(){
    testPath=$1
    folder=$2
    qos=$3
    curTestId=`basename $testPath`
    newTestId="${curTestId}-$(printf %03d $qos)"
    mkdir -p $folder
    for i in `$LS $testPath*.csv`; do
        cd $folder
        filename=`basename $i`
        ln -sf ../${filename} $(echo $filename | sed "s/$curTestId/$newTestId/")
        cd -
    done

    echo "$folder/$newTestId"
}

#
# Main = Binary Search
#
min=$MIN_VAL
max=$MAX_VAL
while [ $max -ge $min ]; do
    mid=$(expr $min \+ \( $max - $min \) \/ 2)
    qos_factor=$(fpcalc $mid/100)
    newTestId=$(createSymLinks $testCase $outputDir $mid | tail -1)
    output=${newTestId}.out
    cplexSolFile=${newTestId}_${strategy}_cplex.sol
    echo -e "time ./simulator.sh $strategy $newTestId $qos_factor > $output 2>&1"
    echo -e "\nDEBUG: min=$min; mid=$mid; max=$max"
    time ./simulator.sh $strategy $newTestId $qos_factor > $output 2>&1

    if [ -f $cplexSolFile ]; then
        isFeasible=true
        solution=`grep objectiveValue $cplexSolFile | cut -d '"' -f2`
        echo -e "\nQoS ='$qos_factor'; Solution feasible = '$solution'\n"
        grep "### Statistics ###" -A1000 $output
        cp -f $output $finalOutput
        echo -e "\ncp -f $output $finalOutput\n"
    else
        isFeasible=false
        echo -e "\nQoS = '$qos_factor'; Solution infeasible"
    fi

    echo "############   DEBUG: solution=$solution fpcalc=$(fpcalc $solution == $BEST_SOLUTION)"
    if [ "$isFeasible" == "true" ]&&[ $(fpcalc $qos_factor == $BEST_SOLUTION) -eq 1 ]; then
        echo -e "\n--------------------------------------------------------------------------------\n"
        echo "Optimum solution was found!"
        exit 0
    # determine which subarray to search
    elif [ "$isFeasible" == "true" ]; then
        # change min index to search upper subarray
        min=$(($mid + 1))
    else
        # change max index to search lower subarray
        max=$(($mid - 1))
    fi
    echo -e "\n--------------------------------------------------------------------------------\n"
done

echo -e "WARNING: No optimum solution was found for Load Balancing!"
echo -e "\n--------------------------------------------------------------------------------\n"
