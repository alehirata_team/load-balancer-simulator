#!/bin/bash

OUTPUT=`echo $0 | sed 's/\.sh//g'`.out
SCRIPT="runStrategies.sh"
NUM_REPETITIONS=5
TEST_FOLDER=config
TEST_CASE=test_simul-04-03-50

if [ -f "$OUTPUT" ]; then
    rm -f "$OUTPUT"
fi

echo "Running $0"
echo "Start Time: `date`" 

testInput=$TEST_FOLDER/$TEST_CASE
echo -e "\n\n================================================================================" >> $OUTPUT
echo "$testInput" >> $OUTPUT
echo -e "================================================================================\n" >> $OUTPUT
./$SCRIPT $testInput $NUM_REPETITIONS >> $OUTPUT 2>&1

echo "$SCRIPT: Done!" >> $OUTPUT
echo "End Time: `date`"
echo "$0: Done!"
