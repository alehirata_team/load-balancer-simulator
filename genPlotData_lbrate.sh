#!/bin/bash

if [ $# -lt 3 ]; then
    echo -e "Usage: $0 <confidence level> <output> <test folder 1> ... <test folder n>"
    exit 1
fi

confidence_level=$1
output=$2
shift 2
test_list="$*"
#LB_SOL_PREFFIX="Load Balance Solution"
MAX_LOAD_PREFFIX="Max Downlink Load Factor"
MIN_LOAD_PREFFIX="Min Downlink Load Factor"
STRATEGIES_LIST=`./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)"`
CALC_CONFINTERVAL="java -cp ./libs/*:bin util.ConfidenceIntervalApp"
datafile=${output}

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

#
# Float calculator
#
FLOAT_PRECISION=3
function fpcalc()
{
    echo "scale=$FLOAT_PRECISION; $*" | bc
}

#
# Creating the Header
#

HEADER="Total_UE"
for i in $STRATEGIES_LIST; do
    HEADER=$HEADER" ${i} ${i}_CI"
done

echo "$HEADER" > $datafile


#
# Fill in the datafile with mean and confidence interval for each strategy
#

for test_path in $test_list; do
    test_id=`basename $test_path`
    numUE="${test_id##*-}"
    line="$numUE"
    for strategy in $STRATEGIES_LIST; do
        stat=""
        for file in `$LS $test_path/$test_id*${strategy}.out`; do
            max=`grep "${MAX_LOAD_PREFFIX}:" $file | cut -d':' -f2 | cut -d' ' -f2 | sed 's/\s//'`
            min=`grep "${MIN_LOAD_PREFFIX}:" $file | cut -d':' -f2 | cut -d' ' -f2 | sed 's/\s//'`
            val=$(echo "`fpcalc $min / $max`"  | sed 's/,/\./')
            if [ -n "$val" ]; then
                stat=$stat" $val"
            fi
        done
        #echo -e "DEBUG: $strategy, $cell_type, $stat"
        line=$line" `$CALC_CONFINTERVAL $confidence_level $stat`"
        #echo $line
    done
    echo $line >> $datafile
done

sed -i 's/,/\./g' $datafile
