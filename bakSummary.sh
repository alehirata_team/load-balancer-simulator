#!/bin/bash
output=backup/summary.`date +%s`.tgz
find config/ -name "*.png" -or -name "*.dat" | xargs tar czvf $output *.out
echo -e "\n'$output' created!"
