#!/bin/bash

output=`echo $0 | sed 's/\.sh//g'`.out
script="plot.sh"
confidence_level=0.95
test_folder=config

echo "Running $0"
echo "Start Time: `date`"

if [ -f "$output" ]; then
    rm -f "$output"
fi

testList=""

for i in `ls $test_folder | grep -v '\.properties'`; do
    testInput=$test_folder/$i
    testList="$testList $testInput"
    echo -e "\n\n================================================================================" >> $output
    echo "$i" >> $output
    echo -e "================================================================================\n" >> $output
    ./$script $confidence_level $testInput >> $output 2>&1
done

./plot_attended.sh $confidence_level $test_folder $testList
./plot_loadbalance.sh $confidence_level $test_folder $testList

echo "$script: Done!" >> $output
echo "End Time: `date`"
echo "$0: Done!"
