#!/bin/bash

if [ $# -ne 3 ]; then
    echo -e "Usage: $0 <confidence level> <test folder> <output>"
    exit 1
fi

confidence_level=$1
test_path="$2"
output=$3
LB_SOL_PREFFIX="Load Balance Solution"
QOS_PREFFIX="QoS Factor"
CALC_CONFINTERVAL="java -cp ./libs/*:bin util.ConfidenceIntervalApp"
datafile=$output

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi


function fpcalc(){
    expr=$*
    echo "scale=5; $expr" | bc | sed 's/^\./0\./'
}

#
# Main
#

if [ -f $datafile ]; then
    rm -f $datafile
fi

simulOutFiles=`$LS $test_path/*.sol | sed 's/_LP_GREEDY_cplex\.sol/\.out/'`
numSimulOutFiles=0
for i in $simulOutFiles; do
    if [ ! -f $i ]; then
        echo -e "ERROR: File '$i' does not exist. Fix this script."
        exit 1
    fi
    numSimulOutFiles=$(($numSimulOutFiles+1))
done


#
# Creating the Header
#

HEADER="Attended_UE LoadBalance"
echo "$HEADER" > $datafile


#
# Fill in the datafile for each scenario
#

for file in $simulOutFiles; do
    #test_id=`basename $file .out`
    qos=`grep "${QOS_PREFFIX}:" $file | tail -1 | cut -d':' -f2 | cut -d' ' -f2 | sed 's/\s//' | sed 's/,/\./'`
    qos=`fpcalc $qos \* 100 | sed 's/^\./0\./' | xargs printf %0.1f`
    sol=`grep "${LB_SOL_PREFFIX}:" $file | cut -d':' -f2 | cut -d' ' -f2 | sed 's/\s//' | sed 's/,/\./'`
    line="$qos $sol"
    #echo $line
    echo $line >> $datafile
done

sed -i 's/,/\./g' $datafile
