#!/bin/bash

if [ $# -ne 2 ]; then
    echo -e "Usage: $0 <confidence level> <test path>"
    exit 1
fi

confidence_level=$1
test_path=$2
test_id=`basename $test_path`
output_dir=$test_path
#CELL_TYPE="MACRO PICO FEMTO"
CELL_TYPE="MACRO PICO"
STRATEGIES_LIST=`./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)" | sed  's/_/\\_/g'`
LP_GREEDY_STR="LP\\_GREEDY"

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

datafile_attended_cell=$output_dir/${test_id}_attended_cell.dat
chartfile_attended_cell=$output_dir/${test_id}_attended_cell.png

datafile_unattended=$output_dir/${test_id}_unattended.dat
chartfile_unattended=$output_dir/${test_id}_unattended.png


#
# Calculating total UE
#

totalUE=`wc -l $test_path/$test_id*01_ue.csv | cut -d' ' -f1 | xargs -I{} expr {} \- 1`


#
# Calculate the means and confidence intervals to be plotted
#

./genPlotData_attended_cell.sh $confidence_level $test_path
./genPlotData_unattended.sh $confidence_level $test_path

if [ ! -f $datafile_attended_cell ]; then
    echo -e "ERROR: $datafile_attended_cell could not be generated!"
    exit 1
fi


#
# Mapping idx to strategy name to fill in x-axis values
#

idx=0
xtics=""
for i in $STRATEGIES_LIST; do
    xtics=$xtics", \"${i}\" ${idx}"
    idx=`expr $idx \+ 1`
done
num_strategies=$(($idx-1))
xtics=`echo $xtics | sed "s/^, //"`


#
# Plot as many columns as we have strategies
#

cols_str=""
col_idx=4
for i in `seq 2 $num_strategies`; do
    col_str=$col_str" '' u $col_idx:$(($col_idx+1)) ti col,"
    col_idx=$((col_idx+2))
done
col_str=`echo $col_str | sed 's/,$//'`


#
# Calling gnuplot with the script to plot the chart
#

gnuplot << EOF
set term png
set output '$chartfile_attended_cell'
set title "Accepted User Equipments\n(Total: $totalUE UEs)"
set xlabel "Strategy"
set ylabel "Accepted UE"
set key outside
set boxwidth 0.9 absolute
set style fill solid 1.00 border -1
set style histogram clustered gap 1 title offset character 0, 0, 0
set datafile missing '-'
set style data histograms
set style histogram errorbars gap 2 lw 1
set xtics border in scale 1,0.5 nomirror rotate by -45 offset character 0, 0, 0
set xtics ($xtics)
set offsets 0, 0, 2, 0
#set yrange [ 0 : 3000 ] noreverse nowriteback
plot '$datafile_attended_cell' using 2:3:xtic(1) ti col, $col_str
EOF

if [ ! -s $chartfile_attended_cell ]; then
    echo -e "ERROR: $chartfile_attended_cell could not be generated"
    exit 1
fi

echo "$chartfile_attended_cell created."


#
# Calling gnuplot with the script to plot the chart
#

gnuplot << EOF
set term png
set output '$chartfile_unattended'
set title "Unaccepted User Equipments\n(Total: $totalUE UEs)"
set xlabel "Strategy"
set ylabel "Unaccepted UE"
set key off
set boxwidth 0.9 absolute
set style fill solid 1.00 border -1
set style histogram clustered gap 1 title offset character 0, 0, 0
set datafile missing '-'
set style data histograms
set style histogram errorbars gap 2 lw 1
set xtics border in scale 1,0.5 nomirror rotate by -45 offset character 0, 0, 0
set xtics ($xtics)
set offsets 0, 0, 2, 0
#set yrange [ 0 : 3000 ] noreverse nowriteback
plot '$datafile_unattended' using 2:3:xtic(1) ti col
EOF

if [ ! -s $chartfile_unattended ]; then
    echo -e "ERROR: $chartfile_unattended could not be generated"
    exit 1
fi

echo "$chartfile_unattended created."


#
# Generating charts that compares the LP_GREEDY solutions
#

if [ `echo $STRATEGIES_LIST | grep -c $LP_GREEDY_STR` -eq 1 ]; then
    for i in `$LS $test_path | grep LP_GREEDY | grep -v .out | sort -u`; do
        ./plot_lpGreedyCompare.sh $confidence_level $test_path/$i
    done
fi
