#!/bin/bash

if [ $# -ne 2 ]; then
    echo -e "Usage: $0 <confidence level> <test path>"
    exit 1
fi

confidence_level=$1
test_path=$2
test_id=`basename $test_path`
datafile=$test_path/${test_id}.dat
chartfile=$test_path/${test_id}.png

./genPlotData_lpGreedyCompare.sh $confidence_level $test_path $datafile

if [ ! -f $datafile ]; then
    echo -e "ERROR: $datafile could not be generated!"
    exit 1
fi


#
# Calling gnuplot with the script to plot the chart
#
gnuplot << EOF
set term png
set output '$chartfile'
set title "Load Balance\n($test_id)"
set xlabel "Attended UE (%)"
set ylabel "Lmax - Lmin (%)"
set key outside
set boxwidth 0.9 absolute
set style fill solid 1.00 border -1
set datafile missing '-'
set style data errorlines
set xtics border in scale 1
set offsets 0.25, 0.25, 1, 1
plot '$datafile' u 0:(\$2*100):xtic(1) noti
EOF

if [ ! -s $chartfile ]; then
    echo -e "ERROR: $chartfile could not be generated"
    exit 1
fi

echo "$chartfile created."
