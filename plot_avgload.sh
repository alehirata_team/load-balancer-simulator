#!/bin/bash

if [ $# -lt 3 ]; then
    echo -e "Usage: $0 <confidence level> <output folder> <test folder 1> ... <test folder n>"
    exit 1
fi

confidence_level=$1
output_dir=$2
shift 2
test_list=$*
STRATEGIES_LIST=`./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)"`
NUM_STRATEGIES=`echo $STRATEGIES_LIST | sed 's/ /\n/g' | wc -l`
STRATEGIES_LIST=`echo $STRATEGIES_LIST | sed 's/\n/ /g' `
datafile=$output_dir/avgload.dat
chartfile=$output_dir/avgload.png


#
# Calculate the means and confidence intervals to be plotted
#

./genPlotData_avgload.sh $confidence_level $datafile $test_list

if [ ! -f $datafile ]; then
    echo -e "ERROR: $datafile could not be generated!"
    exit 1
fi


#
# Mapping idx to strategy name to fill in x-axis values
#

idx=0
x_numList=""
for test_id in $test_list; do
    x_numList=$x_numList" ${test_id##*-}"
    idx=`expr $idx \+ 1`
done
num_tests=$idx
x_numList=`echo $x_numList | sed "s/^ //"`


#
# Plot as many columns as we have strategies
#

cols_str=""
col_idx=4
for i in `seq 2 $NUM_STRATEGIES`; do
    col_str=$col_str" '' u 0:(\$$col_idx*100):(\$$(($col_idx+1))*100):xtic(1) ti \"$(echo $STRATEGIES_LIST | cut -d' ' -f$i)\","
    col_idx=$((col_idx+2))
done
col_str=`echo $col_str | sed 's/,$//'`


#
# Calling gnuplot with the script to plot the chart
#

firstStrategy=`echo "$STRATEGIES_LIST" | cut -d' ' -f1`
#cat > gnuplot.in << EOF
gnuplot << EOF
set term png
set output '$chartfile'
set title "Average Load"
set xlabel "Total UE"
set ylabel "Load (%)"
set key outside
set boxwidth 0.9 absolute
set style fill solid 1.00 border -1
set datafile missing '-'
set style data errorlines
set xtics border in scale 1,0.5 nomirror rotate by -45 offset character 0, 0, 0
set offsets 0.25, 0.25, 10, 10
plot '$datafile' u 0:(\$2*100):(\$3*100):xtic(1) ti "$firstStrategy", $col_str
EOF

if [ ! -s $chartfile ]; then
    echo -e "ERROR: $chartfile could not be generated"
    exit 1
fi

echo "$chartfile created."

exit 0

