#!/bin/bash -x
OUTPUT=`echo $0 | sed 's/\.sh//g'`.out
PROPERTIES=properties.tmp
TEST_FOLDER=config


if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi


function createProperty(){
	strategy=$1
	picoCellBias=$2
	testFile=$3
	propertiesFile=$4

    if [ "$strategy" == "RATE_BIAS" ]; then
        str="rate"
        rateBiasMacroCell=1.0
        rateBiasPicoCell=$picoCellBias
        rateBiasFemtoCell=1.88
    else
        str="sinr"
        sinrBiasMacroCell=1.0
        sinrBiasPicoCell=$picoCellBias
        sinrBiasFemtoCell=11.9
    fi

    macro_bias="${str}BiasMacroCell"
    pico_bias="${str}BiasPicoCell"
    femto_bias="${str}BiasFemtoCell"

	cat > $propertiesFile << EOF
defaultStrategy=$strategy
defaultInputTest=$testFile
defaultSimulMode=SOLVE
defaultTestInterval=10
defaultQoSFactor=1.0
${macro_bias}=${!macro_bias}
${pico_bias}=${!pico_bias}
${femto_bias}=${!femto_bias}
EOF
}


#
# Main
#

ndashes=$(grep -o "-" <<<"$($LS $TEST_FOLDER | grep -v '\.properties' | grep simul | head -1)" | wc -l)

for i in `seq 1 10`; do
    for strategy in RATE_BIAS RANGE_EXPANSION; do
    	if [ "$strategy" == "RATE_BIAS" ]; then
    		picoCellBias="1.$i"
        else
    		picoCellBias="$i.0"
        fi
        
        for j in `$LS $TEST_FOLDER | grep -v '\.properties' | grep simul | sort -n  -t'-' -k $(expr $ndashes \+ 1) | egrep '(500|000)'`; do
            testInput=$TEST_FOLDER/$j/${j}_01
            createProperty $strategy $picoCellBias $testInput $PROPERTIES

            echo -e "\n\n================================================================================" >> $OUTPUT
            echo "$testInput" >> $OUTPUT
            echo -e "================================================================================\n" >> $OUTPUT
            java -Djava.library.path=/usr/lib/jni/ -cp ./libs/*:bin Simulator $PROPERTIES SOLVE >> $OUTPUT
        done
   done
done
