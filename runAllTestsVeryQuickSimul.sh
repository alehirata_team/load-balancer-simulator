#!/bin/bash

OUTPUT=`echo $0 | sed 's/\.sh//g'`.out
SCRIPT="runStrategies.sh"
NUM_REPETITIONS=30
TEST_FOLDER=config

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

if [ -f "$OUTPUT" ]; then
    rm -f "$OUTPUT"
fi

echo "Running $0"
echo "Start Time: `date`"

ndashes=$(grep -o "-" <<<"$($LS $TEST_FOLDER | grep -v '\.properties' | grep simul | egrep -v '([1-9][0-9]00|[3-9]00)' | head -1)" | wc -l)

for i in `$LS $TEST_FOLDER | grep -v '\.properties' | grep simul | egrep -v '([1-9][0-9]00|[3-9]00)' | sort -n  -t'-' -k $(expr $ndashes \+ 1)`; do
    testInput=$TEST_FOLDER/$i
    echo -e "\n\n================================================================================" >> $OUTPUT
    echo "$testInput" >> $OUTPUT
    echo -e "================================================================================\n" >> $OUTPUT
    ./$SCRIPT $testInput $NUM_REPETITIONS >> $OUTPUT 2>&1
done

echo "$SCRIPT: Done!" >> $OUTPUT
echo "End Time: `date`"
echo "$0: Done!"
