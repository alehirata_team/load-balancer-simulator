package lb_simul.util;

import org.junit.Test;

import util.ConfidenceIntervalApp;

public class ConfidenceIntervalTest {

    @Test
    public void test() {
        String args[] = {"0.95", "1"};
        ConfidenceIntervalApp.main(args);
    }
    
    @Test
    public void test2() {
        String args[] = {"0.95", "1", "2"};
        ConfidenceIntervalApp.main(args);
    }

}
