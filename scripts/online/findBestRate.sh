#!/bin/bash

curDir=`pwd`
cd `dirname $0`

OUTPUT=`echo $0 | sed 's/\.sh//g'`.out
SCRIPT="runStrategies.sh"
#NUM_REPETITIONS=30
NUM_REPETITIONS=5
#TEST_FOLDER=../../config
TEST_FOLDER=../../scenarios

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

if [ -f "$OUTPUT" ]; then
    rm -f "$OUTPUT"
fi

echo "Running $0"
echo "Start Time: `date`"

for i in `$LS $TEST_FOLDER | grep -v '\.properties'`; do
    testInput=$TEST_FOLDER/$i
    testOutput=$TEST_FOLDER/$i/$i.out
    echo -e "\n\n================================================================================" >> $OUTPUT
    echo "$testInput" >> $OUTPUT
    echo -e "================================================================================\n" >> $OUTPUT
    echo "Start Time: `date`" >> $OUTPUT
    if [ -f "$testOutput" ]; then
        rm -f "$testOutput"
    fi
    ./$SCRIPT $testInput $NUM_REPETITIONS >> $testOutput 2>&1
    echo "End Time: `date`" >> $OUTPUT
done

echo "$SCRIPT: Done!" >> $OUTPUT
echo "End Time: `date`"
echo "$0: Done!"

cd $curDir
