#!/bin/bash

curDir=`pwd`
cd `dirname $0`

confidence_level=0.95
#test_folder=config
test_folder=../../scenarios
FILTER="[2-4]0$"

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

echo "Running $0"
echo "Start Time: `date`"

testList=""
ndashes=$(grep -o "-" <<<"$($LS $test_folder | grep -v '\.properties' | grep simul | tail -1)" | wc -l)
regexp="test_simul-04-03-10$"

#for i in `$LS $test_folder | grep $FILTER | grep -v '\.properties' | grep simul | egrep "($regexp)" | sort -n -t'-' -k $(expr $ndashes \+ 1)`; do
for i in `$LS $test_folder | grep -v '\.properties' | grep simul | egrep "($regexp)" | sort -n -t'-' -k $(expr $ndashes \+ 1)`; do
    testInput=$test_folder/$i
    #./plot_attended_time.sh $confidence_level $testInput $testInput
    #./plot_loadbalance_time.sh $confidence_level $testInput $testInput
    ./plot_avgload_time.sh $confidence_level $testInput $testInput
done

echo "End Time: `date`"
echo "$0: Done!"

cd $curDir
