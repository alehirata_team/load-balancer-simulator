#!/bin/bash

if [ $# -lt 3 ]; then
    echo -e "Usage: $0 <confidence level> <output> <test folder 1> ... <test folder n>"
    exit 1
fi

curDir=`pwd`
cd `dirname $0`

confidence_level=$1
output=$2
shift 2
test_list="$*"
LB_SOL_PREFFIX="Load Balance Solution"
STRATEGIES_LIST=`./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)"`
RELATIVE_ROOT_PATH=../..
JAVA_OPTS="-cp ${RELATIVE_ROOT_PATH}/libs/*:${RELATIVE_ROOT_PATH}/bin"
CALC_CONFINTERVAL="java $JAVA_OPTS util.ConfidenceIntervalApp"
datafile=${output}

if [ "$(uname)" == "Darwin" ]; then
    OS=MAC
	LS="ls"
else
    OS=LINUX
	LS="ls --color=never"
fi

#
# Creating the Header
#

HEADER="UE_MIN"
for i in $STRATEGIES_LIST; do
    HEADER=$HEADER" ${i} ${i}_CI"
done

echo "$HEADER" > $datafile


#
# Fill in the datafile with mean and confidence interval for each strategy
#

for test_path in $test_list; do
    test_id=`basename $test_path`
    rateUEperMin="${test_id##*-}"
    line="$rateUEperMin"
    for strategy in $STRATEGIES_LIST; do
        stat=""
        for file in `$LS $test_path/$test_id*${strategy}.out`; do
            val=`grep "${LB_SOL_PREFFIX}:" $file | cut -d':' -f2 | cut -d' ' -f2 | cut -d'%' -f1 | sed 's/\s//' | awk '{ total += $1 } END { print total/NR }' | sed 's/,/\./'`
            if [ -n "$val" ]; then
                stat=$stat" $val"
            fi
        done
        #echo -e "DEBUG: $strategy, $cell_type, $stat"
        line=$line" `$CALC_CONFINTERVAL $confidence_level $stat`"
        #echo $line
    done
    echo $line >> $datafile
done

sed -i 's/,/\./g' $datafile
