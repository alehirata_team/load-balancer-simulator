#!/bin/bash -x
rm -f *.out *.lp *.sol *.log
find ../../scenarios/ -name "*.out" -or -name "*.lp" -or -name "*.sol" -or -name "*.log" -or -name "*.dat" -or -name "*.png" | xargs rm -f
