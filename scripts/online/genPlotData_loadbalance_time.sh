#!/bin/bash

if [ $# -lt 3 ]; then
    echo -e "Usage: $0 <confidence level> <output> <test folder>"
    exit 1
fi

curDir=`pwd`
cd `dirname $0`

confidence_level=$1
output=$2
#shift 2
test_path=$3
ITERATION_NUM_FMT=%06d
CHART_INTERVAL=60
CHART_TEMP_DAT_SUFFIX="_avg_loadbalance_time.dat"
LABEL_LOAD_BALANCE_UE="Load Balance Solution"
STRATEGIES_LIST=`./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)"`
RELATIVE_ROOT_PATH=../..
JAVA_OPTS="-cp ${RELATIVE_ROOT_PATH}/libs/*:${RELATIVE_ROOT_PATH}/bin"
CALC_CONFINTERVAL="java $JAVA_OPTS util.ConfidenceIntervalApp"
datafile=${output}

if [ "$(uname)" == "Darwin" ]; then
    OS=MAC
	LS="ls"
else
    OS=LINUX
	LS="ls --color=never"
fi

#
# Helpers
#

function fpcalc(){
        expr=$*
            echo "scale=5; $expr" | bc | sed 's/^\./0\./'
}

function getIterationStr(){
    iter=`expr 0 + $1`
    echo "{ITER_`printf $ITERATION_NUM_FMT $iter`} "
}

function findLastIteration(){
    path=$1
    egrep -o "(ITER_[0-9]*} )" $path/*.out | cut -d':' -f2 | cut -d'_' -f2 | cut -d'}' -f1 | sort -u | tail -1
    #eventFile=$1
    #echo `expr $(wc -l $eventFile | awk '{print $1}') - 1`
}

function printStats(){
    test_path=$1
    testOutput=$2
    iter=$(findLastIteration ${test_path})
    marker="{ITER_"`printf %06d $iter`"}"
    echo -e "### Statistics ###\n"
    grep "$marker" -A1000 $testOutput
}

function getTimestamp(){
    file=$1
    iter=$2
    marker="{ITER_"`printf $ITERATION_NUM_FMT $iter`"} Timestamp:"
    grep "$marker" $file | cut -d':' -f2 | sed 's/\s//' | sed 's/,/\./'
}

function getTimestampFromEvtFile(){
    file=$1
    iter=$2
    sed -n "$(expr $iter \+ 1)p" $file | cut -d'"' -f2
}

function getValue(){
    file=$1
    iter=$2
    label=$3
    marker="{ITER_"`printf %06d $iter`"} $label:"
    result=$(grep "$marker" $file | cut -d':' -f2 | sed 's/ //' | sed 's/,/\./')
    if [ -z $result ]; then
        echo 0
    else
        echo $result
    fi
}

# Calculate the average value of each timeframe
function genAverageDataFile(){
    avg_outputfile=$1
    totalIter=$2
    t_max=60
    count=0
    total=0
    cat /dev/null > $avg_outputfile
    for iter in $(seq 1 $totalIter); do
        timestamp=$(getTimestamp $file $iter)
        if [ `expr $timestamp '>=' t_max` ]; then
            echo "${t_max};$(fpcalc $total \/ $count)" >> $avg_outputfile
            count=0
            total=0
            t_max=$(expr t_max + $CHART_INTERVAL)
        fi
        count=$(expr $count + 1)
        val=$(getValue $file $iter $LABEL_LOAD_BALANCE_UE)
        total=$(expr $total + $val)
    done
}


#
# Creating the Header
#

HEADER="Time"
for i in $STRATEGIES_LIST; do
    HEADER=$HEADER" ${i} ${i}_CI"
done

echo "$HEADER" > $datafile


# #
# # Generate the average values within each timeframe
# #
#  
# test_id=`basename $test_path`
#  
# for strategy in $STRATEGIES_LIST; do
#     test_idx_path=$test_path/$test_id
#     totalIter=$(findLastIteration ${test_idx_path}_01_evt.csv)
#     for file in `$LS ${test_idx_path}*${strategy}.out`; do
#         echo 
#         #avg_outputfile=${file}${CHART_TEMP_DAT_SUFFIX}
#         #genAverageDataFile $avg_outputfile $totalIter
#     done
# done
#  
#  
# #
# # Fill in the datafile with mean and confidence interval for each strategy
# #
#  
# for timestamp in `cut -d';' -f1 $avg_outputfile`; do
#     line="$timestamp"
#     for strategy in $STRATEGIES_LIST; do
#         #echo -e "DEBUG: $strategy, $cell_type, $stat"
#         line=$line" `grep "^${timestamp};" *${strategy}*${CHART_TEMP_DAT_SUFFIX} | cut -d';' -f2 | xargs $CALC_CONFINTERVAL $confidence_level`"
#     done
#     echo $line >> $datafile
# done


test_id=`basename $test_path`
test_idx_path=$test_path/$test_id
totalIter=$(findLastIteration $test_path)
baseOutputFile=$(grep -l `getIterationStr $totalIter` $test_path/*.out | head -1)

for iter in $(seq 1 $totalIter); do
    timestamp=$(getTimestamp $baseOutputFile $iter)
    line="$timestamp"
    for strategy in $STRATEGIES_LIST; do
        for file in `$LS ${test_idx_path}*${strategy}.out`; do
            val=`getValue $file $iter "$LABEL_LOAD_BALANCE_UE"`
            if [ -n "$val" ]; then
                stat=$stat" $val"
            fi
        done
        line=$line" `$CALC_CONFINTERVAL $confidence_level $stat`"
        stat=""
        #line=$line" `grep "^${timestamp};" *${strategy}*${CHART_TEMP_DAT_SUFFIX} | cut -d';' -f2 | xargs $CALC_CONFINTERVAL $confidence_level`"
    done
    echo $line >> $datafile
done
 
echo "dataFile=$datafile" 

# for timestamp in `cut -d';' -f1 $avg_outputfile`; do
#     line="$timestamp"
#     for strategy in $STRATEGIES_LIST; do
#         for file in `$LS $test_path/$test_id*${strategy}.out`; do
#         #echo -e "DEBUG: $strategy, $cell_type, $stat"
#         line=$line" `grep "^${timestamp};" *${strategy}*${CHART_TEMP_DAT_SUFFIX} | cut -d';' -f2 | xargs $CALC_CONFINTERVAL $confidence_level`"
#         done
#     done
#     echo $line >> $datafile
# done

# #
# # Fill in the datafile with mean and confidence interval for each strategy
# #
# 
# test_id=`basename $test_path`
# numUE="${test_id##*-}"
# line="$numUE"
# for strategy in $STRATEGIES_LIST; do
#     stat=""
#     for file in `$LS $test_path/$test_id*${strategy}.out`; do
#         val=`grep "${ATTENDED_UE_PREFFIX}:" $file | cut -d':' -f2 | cut -d'(' -f1 | sed 's/\s//' | sed 's/,/\./'`
#         if [ -n "$val" ]; then
#             stat=$stat" $val"
#         fi
#     done
#     #echo -e "DEBUG: $strategy, $cell_type, $stat"
#     line=$line" `$CALC_CONFINTERVAL $confidence_level $stat`"
#     #echo $line
# done
# echo $line >> $datafile


if [ "$OS" == "MAC" ]; then
    sed -i.bak 's/,/\./g' $datafile && rm -f $datafile.bak
else
    sed -i 's/,/\./g' $datafile
fi

cd $curDir
exit 0
