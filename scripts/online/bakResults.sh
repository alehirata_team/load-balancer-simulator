#!/bin/bash -x

ROOT_FOLDER=../..
BACKUP_FOLDER=backup
OUTPUT_FILE=results.`date +%s`.tar.gz

curDir=`pwd`
cd `dirname $0`

cd $ROOT_FOLDER
find . -name "*.out" -or -name "*.lp" -or -name "*.sol" -or -name "*.log" -or -name "*.dat" -or -name "*.png" | xargs tar czvf $BACKUP_FOLDER/$OUTPUT_FILE
echo -e "\n'$BACKUP_FOLDER/$OUTPUT_FILE' created."

cd $curDir
