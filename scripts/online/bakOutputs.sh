#!/bin/bash -x

ROOT_FOLDER=../..
SCENARIOS_FOLDER=scenarios
BACKUP_FOLDER=backup
OUTPUT_FILE=outputs.`date +%s`.tar.gz

curDir=`pwd`
cd `dirname $0`

cd $ROOT_FOLDER
tar czvf $BACKUP_FOLDER/$OUTPUT_FILE $SCENARIOS_FOLDER *.out *.log

cd $curDir
