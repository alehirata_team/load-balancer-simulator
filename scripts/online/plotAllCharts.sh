#!/bin/bash -x

curDir=`pwd`
cd `dirname $0`

confidence_level=0.95
#test_folder=config
test_folder=../../scenarios
FILTER="[0-1][0-9][0-9]$"

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

echo "Running $0"
echo "Start Time: `date`"

testList=""
ndashes=$(grep -o "-" <<<"$($LS $test_folder | grep -v '\.properties' | grep simul | tail -1)" | wc -l)
regexp=".*$(for i in `seq 1 $ndashes`; do echo -n '-.*'; done)"

for i in `$LS $test_folder | grep $FILTER | grep -v '\.properties' | grep simul | egrep "($regexp)" | sort -n -t'-' -k $(expr $ndashes \+ 1)`; do
    testInput=$test_folder/$i
    testList="$testList $testInput"
    # echo -e "\n\n================================================================================" >> $output
    # echo "$i" >> $output
    # echo -e "================================================================================\n" >> $output
    # ./plot_attended_time.sh $confidence_level $testInput $testInput
    # ./plot_loadbalance_time.sh $confidence_level $testInput $testInput
    # ./plot_avgload_time.sh $confidence_level $testInput $testInput
done

./plot_attended.sh $confidence_level $test_folder $testList
./plot_avgload.sh $confidence_level $test_folder $testList
./plot_avgmaxload.sh $confidence_level $test_folder $testList
./plot_loadbalance.sh $confidence_level $test_folder $testList

#./plot_attended.sh $confidence_level $curDir $testList
#./plot_avgload.sh $confidence_level $curDir $testList
#./plot_avgmaxload.sh $confidence_level $curDir $testList
#./plot_loadbalance.sh $confidence_level $curDir $testList

echo "End Time: `date`"
echo "$0: Done!"

cd $curDir
