#!/bin/bash -x
cur_dir=`pwd`
scenarios_dir=../../scenarios/
#testIput=test_simul-04-03-10000
testIput=test_simul-04-03-100
minRate=10
maxRate=120
incrRate=40
minTestScn=1
maxTestScn=30

cd $scenarios_dir

for i in `seq $minRate $incrRate $maxRate`; do
    testName=$testIput-`printf %03d $i`
    mkdir -p $testName
    cd $testName
    for j in `seq $minTestScn $maxTestScn`; do
        testId=`printf %02d $j`
        for k in node service ue; do
            ln -s ../${testIput}/${testIput}_${testId}_$k.csv ${testName}_${testId}_$k.csv 
        done
    done
    cd -
done

cd $cur_dir
