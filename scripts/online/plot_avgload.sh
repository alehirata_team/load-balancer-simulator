#!/bin/bash

if [ $# -lt 3 ]; then
    echo -e "Usage: $0 <confidence level> <output folder> <test folder 1> ... <test folder n>"
    exit 1
fi

curDir=`pwd`
cd `dirname $0`

if [ "$(uname)" == "Darwin" ]; then
    OS="mac"
	LS="ls"
else
    OS="linux"
	LS="ls --color=never"
fi


confidence_level=$1
output_dir=$2
shift 2
test_list=$*
STRATEGIES_LIST=`./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)"`
if [ "$OS" == "mac" ]; then
    NUM_STRATEGIES=`echo "$STRATEGIES_LIST" | sed $'s/ /\\\n/g' | wc -l | awk '{print $1}'`
    STRATEGIES_LIST=`echo $STRATEGIES_LIST | sed $'s/\\\n/ /g'`
else
    NUM_STRATEGIES=`echo "$STRATEGIES_LIST" | sed 's/ /\n/g' | wc -l`
    STRATEGIES_LIST=`echo $STRATEGIES_LIST | sed 's/\n/ /g' `
fi
datafile=$output_dir/avgload.dat
chartfile=$output_dir/avgload.png

#
# Calculate the means and confidence intervals to be plotted
#

./genPlotData_avgload.sh $confidence_level $datafile $test_list

if [ ! -f $datafile ]; then
    echo -e "ERROR: $datafile could not be generated!"
    cd $curDir
    exit 1
fi


#
# Mapping idx to strategy name to fill in x-axis values
#

idx=0
x_numList=""
for test_id in $test_list; do
    x_numList=$x_numList" ${test_id##*-}"
    idx=`expr $idx \+ 1`
done
num_tests=$idx
x_numList=`echo $x_numList | sed "s/^ //"`


#
# Plot as many columns as we have strategies
#

cols_str=""
col_idx=4
for i in `seq 2 $NUM_STRATEGIES`; do
    #col_str=$col_str" '' u 0:(\$$col_idx*100):(\$$(($col_idx+1))*100):xtic(1) ti \"$(echo $STRATEGIES_LIST | cut -d' ' -f$i)\","
    col_str=$col_str" '' u (\$1):(\$$col_idx):(\$$(($col_idx+1))) ti \"$(echo $STRATEGIES_LIST | cut -d' ' -f$i | sed 's/_/\\\\_/g')\" ls $i," 
    col_idx=$((col_idx+2))
done
col_str=`echo $col_str | sed 's/,$//'`


#
# Calling gnuplot with the script to plot the chart
#

firstStrategy=`echo "$STRATEGIES_LIST" | cut -d' ' -f1 | sed 's/_/\\\\\\\\_/g'`
#gnuplot << EOF
gnuplotInput=$output_dir/gnuplot.avgload.in 
cat > $gnuplotInput << EOF
set term png
set output '$chartfile'
set title "Average Load"
set xlabel "UE/min"
set ylabel "Load (%)"
set key outside
set boxwidth 0.9 absolute
set style fill solid 1.00 border -1
set datafile missing '-'
set style data errorlines
#set xtics border in scale 1,0.5 nomirror rotate by -45 offset character 0, 0, 0
set xtics border in scale 1,0.5
set offsets 0.25, 0.25, 10, 10
#set xrange [0:60]
#set style line 1 lt 1 lw 2 ps 0 pt 0 lc rgb "gold"
#set style line 2 lt 1 lw 2 ps 0 pt 0 lc rgb "red"
#set style line 3 lt 1 lw 2 ps 0 pt 0 lc rgb "green"
#set style line 4 lt 1 lw 2 ps 0 pt 0 lc rgb "blue"
#set style line 5 lt 1 lw 2 ps 0 pt 0 lc rgb "gray"
#set style line 6 lt 1 lw 2 ps 0 pt 0 lc rgb "cyan"
#set style line 7 lt 1 lw 2 ps 0 pt 0 lc rgb "yellow"
#set style line 8 lt 1 lw 2 ps 0 pt 0 lc rgb "purple"
set style line 1 lt 1 lw 1.5 pt 11 lc rgb "gold"
set style line 2 lt 1 lw 1.5 pt 5  lc rgb "red"
set style line 3 lt 1 lw 1.5 pt 1  lc rgb "green"
set style line 4 lt 1 lw 1.5 pt 9  lc rgb "blue"
set style line 5 lt 1 lw 1.5 pt 2  lc rgb "gray"
set style line 6 lt 1 lw 1.5 pt 7  lc rgb "cyan"
set style line 7 lt 1 lw 1.5 pt 7  lc rgb "yellow"
set style line 8 lt 1 lw 1.5 pt 1  lc rgb "purple"
#plot '$datafile' u 0:(\$2*100):(\$3*100):xtic(1) ti "$firstStrategy" ls 1, $col_str
#plot '$datafile' u (\$1/60):(\$2*100) ti "$firstStrategy" ls 1, $col_str
plot '$datafile' u (\$1):(\$2):(\$3) ti "$firstStrategy" ls 1, $col_str
EOF
gnuplot < $gnuplotInput

if [ ! -s $chartfile ]; then
    echo -e "ERROR: $chartfile could not be generated"
    cd $curDir
    exit 1
fi

echo "$chartfile created."

cd $curDir
exit 0

