#!/bin/bash

ROOT_FOLDER=../..
SCENARIOS_FOLDER=scenarios
BACKUP_FOLDER=backup
OUTPUT_FILE=summary.`date +%s`.tar.gz

curDir=`pwd`
cd `dirname $0`

cd $ROOT_FOLDER
find $SCENARIOS_FOLDER/ -name "*.png" -or -name "*.dat" | xargs tar czvf $BACKUP_FOLDER/$OUTPUT_FILE *.out
echo -e "\n'$BACKUP_FOLDER/$OUTPUT_FILE' created!"

cd $curDir
