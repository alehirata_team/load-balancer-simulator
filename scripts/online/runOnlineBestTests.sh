#!/bin/bash

curDir=`pwd`
cd `dirname $0`

OUTPUT=`echo $0 | sed 's/\.sh//g'`.out
SCRIPT="runStrategies.sh"
NUM_REPETITIONS=30
#NUM_REPETITIONS=5
TEST_FOLDER=../../scenarios
FILTER="[0-1][0-9][0-9]$"

if [ "$(uname)" == "Darwin" ]; then
	LS="ls"
else
	LS="ls --color=never"
fi

if [ -f "$OUTPUT" ]; then
    rm -f "$OUTPUT"
fi

echo "Running $0"
echo "Start Time: `date`"

ndashes=$(grep -o "-" <<<"$($LS $TEST_FOLDER | grep -v '\.properties' | grep simul | tail -1)" | wc -l)
regexp=".*$(for i in `seq 1 $ndashes`; do echo -n '-.*'; done)"

for i in `$LS $TEST_FOLDER | grep $FILTER | grep -v '\.properties' | grep simul | egrep "($regexp)" | sort -n  -t'-' -k $(expr $ndashes \+ 1)`; do
    testInput=$TEST_FOLDER/$i
    echo -e "\n\n================================================================================" >> $OUTPUT
    echo "$testInput" >> $OUTPUT
    echo -e "================================================================================\n" >> $OUTPUT
    echo "./$SCRIPT $testInput $NUM_REPETITIONS >> $OUTPUT 2>&1"
    ./$SCRIPT $testInput $NUM_REPETITIONS >> $OUTPUT 2>&1
done

echo "$SCRIPT: Done!" >> $OUTPUT
echo "End Time: `date`"
echo "$0: Done!"

cd $curDir
