#!/bin/bash
curDir=`pwd`
cd `dirname $0`/../../
grep -A100 LBOnlineStrategy src/loadBalancer/online/strategy/LBOnlineStrategy.java | egrep -v '(\{|\})' | sed 's/\/\/.*//g' | sed 's/,//' | sed 's/  //g' | grep -v "^$"
cd $curDir
