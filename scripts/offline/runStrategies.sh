#!/bin/bash

if [ $# -ne 2 ]; then
    echo -e "Usage: $0 <test path> <num repetitions> \n e.g. $0 config/test01\n"
    exit 1
fi

curDir=`pwd`
cd `dirname $0`

test_path=$1
num_repetitions=$2
test_id=`basename $test_path`
output_folder=$test_path
qos_factor=""
FLOAT_PRECISION=5

getCurQoSFactor(){
    qosOutput=$1
    if [ ! -f $qosOutput ]; then
        curQoS="0.0"
    else
        curQoS=`cat $qosOutput`
    fi
    echo "$curQoS"
}

updateQoSFactor(){
    qosOutput=$1
    testOutput=$2
    curQoS=`getCurQoSFactor $qosOutput`
    newQoS=`grep "Attended UE Percentage: " $testOutput | cut -d':' -f2 | cut -d' ' -f2`
    if [ "$(echo "$newQoS > $curQoS" | bc)" == "1" ]; then
        echo "$newQoS" > $qosOutput
    fi
}

function findLastIteration(){
    eventFile=$1
    echo `expr $(wc -l $eventFile | awk '{print $1}') - 1`
}

function printStats(){
    test_idx_path=$1
    testOutput=$2
    iter=$(findLastIteration ${test_idx_path}_evt.csv) 
    marker="{ITER_"`printf %06d $iter`"}"
    echo -e "### Statistics ###\n"
    grep "$marker" -A1000 $testOutput
}

#
# Main
#

for strategy in $(./listStrategies.sh | grep -v "$(./listBlockedStrategies.sh)"); do
    for i in `seq 1 $num_repetitions`; do
        idx=`printf %02d $i`
        echo "Strategy: $strategy"
        output=${output_folder}/${test_id}_${idx}_${strategy}.out
        qos_output=${output_folder}/${test_id}_${idx}_qos.out
        qos_factor=`getCurQoSFactor $qos_output`
        test_idx_path=${test_path}/${test_id}_${idx}
        echo "time ./simulator.sh $strategy $test_idx_path $qos_factor > $output 2>&1"
        time ./simulator.sh $strategy $test_idx_path $qos_factor > $output 2>&1
        echo ""
        printStats $test_idx_path $output
        echo -e "\n--------------------------------------------------------------------------------\n"
    done
done

cd $curDir
