#!/bin/bash
curDir=`pwd`
cd `dirname $0`/../../
grep -A100 LBStrategy src/loadBalancer/offline/strategy/LBStrategy.java | egrep -v '(\{|\})' | sed 's/\/\/.*//g' | sed 's/,//' | sed 's/  //g' | grep -v "^$"
cd $curDir
