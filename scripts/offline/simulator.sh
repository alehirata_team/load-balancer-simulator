#!/bin/bash

RELATIVE_ROOT_PATH=../..
JAVA_OPTS="-Djava.library.path=/usr/lib/jni/ -cp ${RELATIVE_ROOT_PATH}/libs/*:${RELATIVE_ROOT_PATH}/bin"
DEFAULT_PROPERTIES=${RELATIVE_ROOT_PATH}/config/default.properties 

curDir=`pwd`
cd `dirname $0`
java $JAVA_OPTS OnlineSimulator $DEFAULT_PROPERTIES SOLVE $*
cd $curDir
