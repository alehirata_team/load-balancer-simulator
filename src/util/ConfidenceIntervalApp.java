package util;
// https://gist.github.com/gcardone/5536578
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class ConfidenceIntervalApp {

    public static void main(String args[]) {
        if(args.length <= 2){
            System.err.println("Usage: ConfidenceIntervalApp <Confidence Level> <val 1> <val 2> ... <val n>\n" +
                    "Returns: mean  mean-ci mean+ci");
            System.exit(1);
        }

        // Build summary statistics of the dataset "data"
        SummaryStatistics stats = new SummaryStatistics();
        for (int i = 1; i < args.length; i++) {
            stats.addValue(Double.parseDouble(args[i]));
        }

        // Calculate 95% confidence interval
        double ci = calcMeanCI(stats, Double.parseDouble(args[0]));

        System.out.println(String.format("%f %f", stats.getMean(), ci));
    }

    private static double calcMeanCI(SummaryStatistics stats, double level) {
        try {
            // Create T Distribution with N-1 degrees of freedom
            TDistribution tDist = new TDistribution(stats.getN() - 1);
            // Calculate critical value
            double critVal = tDist.inverseCumulativeProbability(1.0 - (1 - level) / 2);
            // Calculate confidence interval
            return critVal * stats.getStandardDeviation() / Math.sqrt(stats.getN());
        } catch (MathIllegalArgumentException e) {
            return Double.NaN;
        }
    }

}