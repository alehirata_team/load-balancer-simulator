package util;

import hetnet.Service;
import hetnet.userEquipment.UserEquipmentFactory.UECSVCol;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

import loadBalancer.online.solver.OnlineEvent;
import loadBalancer.online.solver.OnlineEventType;
import au.com.bytecode.opencsv.CSVReader;


public class OnlineEventGenerator {

    private static class OnlineTestParams{
        public static final int TIMEFRAME_SIZE = 60 ;
        public double start;
        public double end;
        public int uePerMin;
        public HashMap<String, Double> callHoldMap;

        public OnlineTestParams(double start, double end, HashMap<String, Double> callHoldMap, int uePerMin) {
            this.start = start;
            this.end = end;
            this.callHoldMap = callHoldMap;
            this.uePerMin = uePerMin;
        }
    }

    public static void printPos(double[] pos){
        System.out.println("(" + pos[0] + ", " + pos[1] + ")");
    }

    public static HashMap<String, String> loadUeInfoFile(String ueInfoFile){
        HashMap<String, String> ueToServiceMap = new HashMap<String, String>();

        CSVReader reader;
        try {
            reader = new CSVReader(new FileReader(ueInfoFile), ',', '"', 1);

            String [] col;

            while ((col = reader.readNext()) != null) {
                String ueId = col[UECSVCol.Id.ordinal()];
                String serviceName = col[UECSVCol.ServiceName.ordinal()];
                ueToServiceMap.put(ueId, serviceName);
            }

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ueToServiceMap;
    }
    
    public static double getNextExpRandom(double lambda) {
        return Math.log(1-Math.random())/(-lambda);
    }

    public static double[] getRandomInterval(double start, double end, double size){
        double[] interval = new double[2];

        interval[0] = start + Math.random() * (end - start);
        //interval[1] = interval[0] + getNextExpRandom(1 / size) ;
        interval[1] = interval[0] + size;

        return interval;
    }

    public static SortedMap<Double, OnlineEvent> genOnlineEventList(ArrayList<String> ueList, HashMap<String, String> ueToService, OnlineTestParams params){
        SortedMap<Double, OnlineEvent> eventList = new TreeMap<Double, OnlineEvent>();
        
        double start_i = params.start;
        double end_i = start_i + OnlineTestParams.TIMEFRAME_SIZE;
        int ueCount = params.uePerMin;
        int totalUE = 0;

        for(String ueId : ueList){
            if (start_i >= params.end){
                System.out.println("WARNING: Exceeded number of UEs. " + (ueList.size() - totalUE) + " will not be added to this scenario");
                break;
            }
            totalUE++;
            double[] interval = getRandomInterval(start_i, end_i, params.callHoldMap.get(ueToService.get(ueId)));
            OnlineEvent inEvt = new OnlineEvent(interval[0], ueId, OnlineEventType.JOINING);
            OnlineEvent outEvt = new OnlineEvent(interval[1], ueId, OnlineEventType.LEAVING);
            eventList.put(inEvt.getTimestamp(), inEvt);
            eventList.put(outEvt.getTimestamp(), outEvt);
            
            ueCount--;
            if (ueCount == 0) {
                start_i += OnlineTestParams.TIMEFRAME_SIZE;
                end_i += OnlineTestParams.TIMEFRAME_SIZE;
                ueCount = params.uePerMin;
            }
        }

        return eventList;
    }
    
    public static void genOnlineTestInfoFile(String filename, OnlineTestParams params){
        try{
            PrintWriter writer = new PrintWriter(filename, "UTF-8");
            writer.println(String.format("Start time: %.3f", params.start));
            writer.println(String.format("End time: %.3f", params.end));
            writer.println(String.format("UEs per minute: %d", params.uePerMin));
            for (String service : params.callHoldMap.keySet()){
                writer.println(String.format("Call-hold (%s): %.3f", service, params.callHoldMap.get(service)));
            }
            writer.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }

    public static void genOnlineEventTest(int numTests, String testId, OnlineTestParams params){
        File outdir = new File("scenarios/test_" + testId);
        System.out.println("\n\nGenerating Online Events for '" + testId + "'...");
        String testNamePreffix = outdir.toString() + "/test_" + testId;
        genOnlineTestInfoFile(testNamePreffix + "_info.txt", params);
        for(int i=1; i<=numTests; i++){
            String testName = String.format("%s_%02d", testNamePreffix, i);
            System.out.println("\n\n==================================================\n" + 
                    testName + "\n==================================================");
            try {
                HashMap<String, String> ueToService = loadUeInfoFile(testName + "_ue.csv");
                
                ArrayList<String> ueList = new ArrayList<>();
                ueList.addAll(ueToService.keySet());
                Collections.shuffle(ueList);
                
                SortedMap<Double, OnlineEvent> eventList = genOnlineEventList(ueList, ueToService, params);
                OnlineEvent.genCSV(testName + "_evt.csv", eventList);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("\nFinished generating events '" + testId + "'...");
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        HashMap<String, Double> callHoldMap = new HashMap<String, Double>();
        callHoldMap.put(Service.Type.VoIP.name(), 100.0);
        callHoldMap.put(Service.Type.HDVideoCall.name(), 300.0);
        callHoldMap.put(Service.Type.FullHDVideoStreaming.name(), 600.0);
        
        for (int uePerMin = 10; uePerMin <= 120; uePerMin+=10) {
            OnlineTestParams params = new OnlineTestParams(0.0, 3600.0, callHoldMap, uePerMin);
            genOnlineEventTest(30, "simul-04-03-10000-" + String.format("%03d", uePerMin), params);
        }
        
        /*int uePerMin = 10;
        OnlineTestParams params = new OnlineTestParams(0.0, 3600.0, callHoldMap, uePerMin);
        genOnlineEventTest(30, "simul-04-03-10000-" + String.format("%03d", uePerMin), params);*/
        
        System.out.println("Done!");
    }

}
