package util;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class StandardDeviationApp {

    public static void main(String args[]) {
        if(args.length <= 2){
            System.err.println("Usage: StandardDeviationApp <val 1> <val 2> ... <val n>\n" +
                    "Returns: mean stddev");
            System.exit(1);
        }

        // Build summary statistics of the dataset "data"
        SummaryStatistics stats = new SummaryStatistics();
        for (int i = 0; i < args.length; i++) {
            stats.addValue(Double.parseDouble(args[i]));
        }
     
        System.out.println(String.format("%f %f", stats.getMean(), stats.getStandardDeviation()));
    }
}