package util;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Properties {

    private static Map<String,String> configs;
    private static boolean loaded = false;
    private static File configFile;

    public static String get(String property) {
        return configs.get(property);
    }

    public static boolean isLoaded() {
        return loaded;
    }

    public static File getConfigFile() {
        return configFile;
    }

    public static void loadConfigFile(File file) throws FileNotFoundException, IOException {

        // Read in the config file
        configFile = file;
        configs = new HashMap<String,String>();
        DataInputStream in = new DataInputStream(new FileInputStream(configFile));
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        Pattern patt = Pattern.compile("^\\s*([a-zA-Z0-9_]+)\\s*=\\s*(.*)\\s*$");
        String strLine;
        while ((strLine = br.readLine()) != null){
            Matcher match = patt.matcher(strLine);
            if(match.find()) {
                configs.put(match.group(1), match.group(2));
                System.out.println(match.group(1) + " = " + match.group(2));
            }
        }
        in.close();
        loaded = true;
    }
}
