package util;

import hetnet.Service;
import hetnet.node.ENodeB;
import hetnet.node.FemtoLPN;
import hetnet.node.NodeType;
import hetnet.node.PicoLPN;
import hetnet.node.RelayNode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import au.com.bytecode.opencsv.CSVWriter;


public class InputGenerator {

    public static double[] getRandomPos(double[] minPos, double[] maxPos){
        double[] newPos = new double[2];

        newPos[0] = minPos[0] + Math.random()*(Math.abs(maxPos[0]-minPos[0]));
        newPos[1] = minPos[1] + Math.random()*(Math.abs(maxPos[1]-minPos[1]));

        return newPos;
    }

    public static double[] getRandomPos(double[] center, double r){
        double[] newPos = new double[2];
        double[] minPos = new double[2];
        double[] maxPos = new double[2];
        minPos[0] = center[0] - r;
        minPos[1] = center[1] - r;
        maxPos[0] = center[0] + r;
        maxPos[1] = center[1] + r;

        do{
            newPos[0] = minPos[0] + Math.random()*(Math.abs(maxPos[0]-minPos[0]));
            newPos[1] = minPos[1] + Math.random()*(Math.abs(maxPos[1]-minPos[1]));
        }while(Math.pow(newPos[0] - center[0], 2) + Math.pow(newPos[1] - center[1], 2) >= r*r);

        //System.out.println("center = (" + center[0] + "," + center[1] + "), r = " + r + ", newPos = (" + newPos[0] + "," + newPos[1] + ")");

        return newPos;
    }

    public static void printPos(double[] pos){
        System.out.println("(" + pos[0] + ", " + pos[1] + ")");
    }

    public static ArrayList<double[]> getENodeBListPos(double radio, double intersectDist, double[] minPos, double[] maxPos){
        /**
         * http://www.wirelessbrasil.org/wirelessbr/colaboradores/marcio_rodrigues/tel_03.html
         * http://www.teleco.com.br/tutoriais/tutorialalambcel1/pagina_2.asp
         * A mathematical perspective of self-optimizing wireless networks
         * 
         *        _       _
         *    _ /   \ _ /   \
         *  /   \d_ /   \e_ /
         *  \ _ /   \ _ /   \
         *      \ _ /   \ _ /
         *      /   \c_ /
         *      \ _ /   \ _
         *    _ /   \ _ /   \
         *  /   \a_ /   \b_ /
         *  \ _ /   \ _ /   \
         *      \ _ /   \ _ /
         *
         *    c
         *   / \
         *  a---b
         *
         *  (xa, ya), (xb, yb), (xc =?, yc = ?)
         *
         *  d(a,b) = d(b,c) = d(a,c) = 3R
         *  xc = d(a,b)/2 = 3R/2
         *
         *  xc^2 + yc^2 = d(a,c)^2 = (3R)^2
         *  yc^2 = 9R^2 - (9/4)R^2 = (27/4)R^2
         *  yc = 1.5*sqrt(3)*R 
         */

        ArrayList<double[]> lpos = new ArrayList<double[]>();
        double newRadio = radio - intersectDist/2;

        boolean oddLine =  false;
        for(double y=minPos[1]; y<=maxPos[1]; y+=1.5*Math.sqrt(3)*newRadio){
            for(double x=minPos[0]; x<=maxPos[0]; x+=3*newRadio){
                if(x==minPos[0] && oddLine){
                    x += 1.5 * newRadio;
                }
                lpos.add(new double[]{x,y});
                //InputGenerator.printPos(lpos.get(lpos.size()-1));
            }
            oddLine = !oddLine;
        }

        return lpos;
    }

    /**
     * @param output
     * @param numNodes [ENODEB, RELAY_NODE, PICO, FEMTO]
     * @param minPos
     * @param maxPos
     */
    public static void genNode_CSV(String output, int[] numNodes, double[] minPos, double[] maxPos){
        //ArrayList<double[]> lENodeBPos = getENodeBListPos(2.1, 0.2, minPos, maxPos);
        ArrayList<double[]> lENodeBPos = getENodeBListPos(ENodeB.DEFAULT_COVER_RADIO, ENodeB.DEFAULT_INTERSECT_DIST, minPos, maxPos);
        CSVWriter writer;
        double[] pos = new double[2];

        try {
            writer = new CSVWriter(new FileWriter(output), ',');
            String[] header = {"Type",
                               "Id",
                               "PosX",
                               "PosY",
                               "CoverRadio",
                               "TxPower",
                               "RxPower"};
            writer.writeNext(header);

            int enbId = 0;

            for(double[] posENB : lENodeBPos){

                String l = NodeType.ENODEB + "," 
                        + String.format("ENB_%07d", enbId++) + "," 
                        + posENB[0] + ","
                        + posENB[1] + ","
                        + ENodeB.DEFAULT_COVER_RADIO + ","
                        + ENodeB.DEFAULT_TX_POWER + ","
                        + ENodeB.DEFAULT_RX_POWER;
                writer.writeNext(l.split(","));
            }

            int totalRandomNodes = numNodes[NodeType.RELAY_NODE.ordinal()];

            for(int i=0; i<totalRandomNodes; i++){
                pos = getRandomPos(minPos, maxPos);
                String l = NodeType.RELAY_NODE + "," 
                        + String.format("RN_%07d", i) + "," 
                        + pos[0] + ","
                        + pos[1] + ","
                        + RelayNode.DEFAULT_COVER_RADIO + ","
                        + RelayNode.DEFAULT_TX_POWER + ","
                        + RelayNode.DEFAULT_RX_POWER;
                writer.writeNext(l.split(","));
            }

            totalRandomNodes = numNodes[NodeType.PICO.ordinal()];

            for(int i=0; i<totalRandomNodes; i++){
                pos = getRandomPos(minPos, maxPos);
                String l = NodeType.PICO + "," 
                        + String.format("PICO_%07d", i) + "," 
                        + pos[0] + ","
                        + pos[1] + ","
                        + PicoLPN.DEFAULT_COVER_RADIO + ","
                        + PicoLPN.DEFAULT_TX_POWER + ","
                        + PicoLPN.DEFAULT_RX_POWER;
                writer.writeNext(l.split(","));
            }

            totalRandomNodes = numNodes[NodeType.FEMTO.ordinal()];

            for(int i=0; i<totalRandomNodes; i++){
                pos = getRandomPos(minPos, maxPos);
                String l = NodeType.FEMTO + "," 
                        + String.format("FEMTO_%07d", i) + "," 
                        + pos[0] + ","
                        + pos[1] + ","
                        + FemtoLPN.DEFAULT_COVER_RADIO + ","
                        + FemtoLPN.DEFAULT_TX_POWER + ","
                        + FemtoLPN.DEFAULT_RX_POWER;
                writer.writeNext(l.split(","));
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param output
     * @param numNodes [ENODEB, RELAY_NODE, PICO, FEMTO]
     * @param minPos
     * @param maxPos
     */
    public static ArrayList<double[]> genNode_NewCSV(String output, double[] minPos, double[] maxPos){
        ArrayList<double[]> lENodeBPos = getENodeBListPos(ENodeB.DEFAULT_COVER_RADIO, ENodeB.DEFAULT_INTERSECT_DIST, minPos, maxPos);
        CSVWriter writer;
        double[] pos = new double[2];

        try {
            writer = new CSVWriter(new FileWriter(output), ',');
            String[] header = {"Type",
                               "Id",
                               "PosX",
                               "PosY",
                               "CoverRadio",
                               "TxPower",
                               "RxPower"};
            writer.writeNext(header);

            int enbId = 0;

            for(double[] posENB : lENodeBPos){

                String l = NodeType.ENODEB + "," 
                        + String.format("ENB_%07d", enbId++) + "," 
                        + posENB[0] + ","
                        + posENB[1] + ","
                        + ENodeB.DEFAULT_COVER_RADIO + ","
                        + ENodeB.DEFAULT_TX_POWER + ","
                        + ENodeB.DEFAULT_RX_POWER;
                writer.writeNext(l.split(","));
            }

            /*
             *        _       _
             *    _ /   \ _ /   \
             *  /   \e_ /   \f_ /
             *  \ _ /   \#_ /   \ _
             *      \ _ /   \#_ /   \
             *      /   \c_ /   \d_ /
             *      \ _ /#  \ _ /   \
             *    _ /   \ _ /   \ _ /
             *  /   \a_ /   \b_ /
             *  \ _ /   \ _ /   \
             *      \ _ /   \ _ /
             *
             *    # = position of the Pico Cells
             *
             *    c
             *   / \
             *  a---b
             *
             *  (xa, ya), (xb, yb), (xc =?, yc = ?)
             *
             *  d(a,b) = d(b,c) = d(a,c) = 3R
             *  xc = d(a,b)/2 = 3R/2
             *
             *  xc^2 + yc^2 = d(a,c)^2 = (3R)^2
             *  yc^2 = 9R^2 - (9/4)R^2 = (27/4)R^2
             *  yc = 1.5*sqrt(3)*R
             */

            // Calculating Pico LPNs positions
            ArrayList<double[]> lPicoPos = new ArrayList<double[]>();
            double[] posB = lENodeBPos.get(1);
            double[] posC = lENodeBPos.get(2);
            double newRadio = ENodeB.DEFAULT_COVER_RADIO - ENodeB.DEFAULT_INTERSECT_DIST/2;
            lPicoPos.add(posC);
            pos[0] = posC[0];
            pos[1] = posC[1] + (2.0/3.0)*1.5*Math.sqrt(3)*newRadio;
            lPicoPos.add(pos.clone());
            pos[0] = posB[0];
            pos[1] -= (1.0/3.0)*1.5*Math.sqrt(3)*newRadio;
            lPicoPos.add(pos.clone());

            for(int i=0; i<3; i++){
                pos = lPicoPos.get(i);
                String l = NodeType.PICO + "," 
                        + String.format("PICO_%07d", i) + "," 
                        + pos[0] + ","
                        + pos[1] + ","
                        + PicoLPN.DEFAULT_COVER_RADIO + ","
                        + PicoLPN.DEFAULT_TX_POWER + ","
                        + PicoLPN.DEFAULT_RX_POWER;
                writer.writeNext(l.split(","));
            }

            writer.close();

            ArrayList<double[]> lCellPos = new ArrayList<double[]>();

            /*
             *        _       _
             *    _ /   \ _ /   \
             *  /   \4_ /   \5_ /
             *  \ _ /   \#_ /   \ _
             *      \ _ /   \#_ /   \
             *      /   \2_ /   \3_ /
             *      \ _ /#  \ _ /   \
             *    _ /   \ _ /   \ _ /
             *  /   \0_ /   \1_ /
             *  \ _ /   \ _ /   \
             *      \ _ /   \ _ /
             */

            HashMap<Integer, double[][]> lCenterCellPos = new HashMap<Integer, double[][]>();
            for(int i=0; i<lENodeBPos.size(); i++){
                lCenterCellPos.put(i, ENodeB.getCellPos(lENodeBPos.get(i), ENodeB.DEFAULT_COVER_RADIO));
            }


            // Adding the center macro cell
            lCellPos.add(lCenterCellPos.get(2)[1]);

            // Adding pico cells
            lCellPos.addAll(lPicoPos);

            // Adding the other macro cells
            lCellPos.add(lCenterCellPos.get(2)[0]);
            lCellPos.add(lCenterCellPos.get(2)[2]);
            lCellPos.add(lCenterCellPos.get(3)[0]);
            lCellPos.add(lCenterCellPos.get(5)[2]);
            lCellPos.add(lCenterCellPos.get(5)[0]);
            lCellPos.add(lCenterCellPos.get(4)[2]);

            return lCellPos;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void genService_CSV(String output){
        CSVWriter writer;
        try {
            writer = new CSVWriter(new FileWriter(output), ',');
            String[] header = {"Name","MinTxRate","MaxTxRate","MinRxRate","MaxRxRate","MinFrequency","MaxFrequency","Priority"};
            writer.writeNext(header);

            ArrayList<String> lServices = new ArrayList<String>();

            lServices.add(Service.Type.VoIP.name() + ",100,100,100,100,1800,1900,0");
            lServices.add(Service.Type.HDVideoCall.name() + ",1500,1500,1500,1500,1800,1900,0");
            lServices.add(Service.Type.FullHDVideoStreaming.name() + ",11500,11500,11500,11500,1800,1900,0");

            /*lServices.add(Service.Type.VoIP.name() + ",512,512,512,512,1800,1900,0");
            lServices.add(Service.Type.HDVideoCall.name() + ",512,512,512,512,1800,1900,0");
            lServices.add(Service.Type.FullHDVideoStreaming.name() + ",512,512,512,512,1800,1900,0");*/

            for(String service : lServices){
                writer.writeNext(service.split(","));
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static double getRadioListCell(int idx){
        if(idx>0 && idx<4){
            return PicoLPN.DEFAULT_COVER_RADIO;
        }
        return ENodeB.DEFAULT_COVER_RADIO;
    }

    private static boolean isInCoverAreaOfOtherCells(ArrayList<double[]> lCellPos, int idx, double[] pos){
        for(int i=0; i<lCellPos.size(); i++){
            if(i != idx){
                double r = getRadioListCell(idx);
                double[] center = lCellPos.get(i); 
                if(Math.pow(pos[0] - center[0], 2) + Math.pow(pos[1] - center[1], 2) <= r*r){
                    //System.out.println("ERROR: (" + pos[0] + "," + pos[1] + ") is in coverage area of idx=[" + i + "] center=(" + center[0] + "," + center[1] + "), r=" + r);
                    return true;
                }
            }
        }
        return false;
    }

    public static void genUE_NewCSV(String output, int baseId, int numUE, String UEType, ArrayList<double[]> lCellPos, int idx, String idFmt, boolean writeHeader){
        double[][] pos = new double[numUE][2];
        for(int i=0; i<numUE; i++){
            if(idx>0 && idx <4){ // Pico
                pos[i] = getRandomPos(lCellPos.get(idx), getRadioListCell(idx));
            }
            else{
                do{
                    pos[i] = getRandomPos(lCellPos.get(idx), getRadioListCell(idx));
                } while (isInCoverAreaOfOtherCells(lCellPos, idx, pos[i]));
            }
            //InputGenerator.printPos(pos[i]);
        }

        CSVWriter writer;
        boolean append = !writeHeader;
        try {
            writer = new CSVWriter(new FileWriter(output, append), ',');

            if(writeHeader){
                String[] header = {"Type",
                                   "Id",
                                   "PosX",
                                   "PosY",
                                   "ServiceName"};
                writer.writeNext(header);
            }

            for(int i=0; i<numUE; i++){
                // feed in your array (or convert your data to an array)
                String l = UEType + "," 
                        + String.format(idFmt, baseId + i) + "," 
                        + pos[i][0] + ","
                        + pos[i][1] + ","
                        + (Service.Type.values()[i%Service.Type.values().length]).name();

                writer.writeNext(l.split(","));
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void genUE_CSV(String output, int numUE, String UEType, double[] minPos, double[] maxPos, String idFmt){
        double[][] pos = new double[numUE][2];
        for(int i=0; i<numUE; i++){
            pos[i] = getRandomPos(minPos, maxPos);
            //InputGenerator.printPos(pos[i]);
        }

        CSVWriter writer;
        try {
            writer = new CSVWriter(new FileWriter(output), ',');
            String[] header = {"Type",
                               "Id",
                               "PosX",
                               "PosY",
                               "ServiceName"};

            writer.writeNext(header);
            for(int i=0; i<numUE; i++){
                // feed in your array (or convert your data to an array)
                String l = UEType + "," 
                        + String.format(idFmt, i) + "," 
                        + pos[i][0] + ","
                        + pos[i][1] + ","
                        + (Service.Type.values()[i%Service.Type.values().length]).name();

                writer.writeNext(l.split(","));
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void genUnitTest(int numTests, String testId, double maxPosX, double maxPosY, int numRN, int numPico, int numFemto, int numMobiles){
        double[] minPos = {0.0, 0.0};
        double[] maxPos = {maxPosX, maxPosY};
        File outdir = new File("config/test_" + testId);
        outdir.mkdir();
        String testNamePreffix = outdir.toString() + "/test_" + testId;
        for(int i=1; i<=numTests; i++){
            String testName = String.format("%s_%02d", testNamePreffix, i);
            InputGenerator.genUE_CSV(testName + "_ue.csv", numMobiles, "CELLPHONE", minPos, maxPos, "C_%07d");
            InputGenerator.genService_CSV(testName + "_service.csv");
            InputGenerator.genNode_CSV(testName + "_node.csv", new int[]{0, 0, numRN, numPico, numFemto}, minPos, maxPos);
        }
    }

    public static void genSimulTest(int numTests, String testId, double maxPosX, double maxPosY, int numPico, int numMobiles){
        double[] minPos = {0.0, 0.0};
        double[] maxPos = {maxPosX, maxPosY};
        File outdir = new File("scenarios/test_" + testId);
        System.out.println("\n\nGenerating '" + testId + "'...");
        outdir.mkdir();
        String testNamePreffix = outdir.toString() + "/test_" + testId;
        for(int i=1; i<=numTests; i++){
            String testName = String.format("%s_%02d", testNamePreffix, i);
            System.out.println("\n\n==================================================\n" + 
                    testName + "\n==================================================");
            InputGenerator.genService_CSV(testName + "_service.csv");
            ArrayList<double[]> lCellPos = InputGenerator.genNode_NewCSV(testName + "_node.csv", minPos, maxPos);

            int numMobileCentralCell = (int)Math.ceil(numMobiles*0.04);
            System.out.println("\n" + numMobileCentralCell + " Mobiles on Central Cell");
            InputGenerator.genUE_NewCSV(testName + "_ue.csv", 0, numMobileCentralCell, "CELLPHONE", lCellPos, 0, "C_%07d", true);

            int numMobilePicoCell = (int)Math.ceil(numMobiles*0.2);
            for(int j=1; j<=3; j++){
                System.out.println("\n" + numMobilePicoCell + " Mobile on Pico Cell #" + j);
                InputGenerator.genUE_NewCSV(testName + "_ue.csv", numMobileCentralCell + (j-1)*numMobilePicoCell, numMobilePicoCell, "CELLPHONE", lCellPos, j, "C_%07d", false);
            }

            int numMobileMacroCell = (int)Math.ceil(numMobiles*0.06);
            for(int j=0; j<6; j++){
                System.out.println("\n" + numMobileCentralCell + " Mobile on Macro Cell #" + j);
                InputGenerator.genUE_NewCSV(testName + "_ue.csv", numMobileCentralCell + 3*numMobilePicoCell + j*numMobileMacroCell, numMobileMacroCell, "CELLPHONE", lCellPos, 4+j, "C_%07d", false);
            }

        }
        System.out.println("\nFinished generating '" + testId + "'...");
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // testId: number of eNodeB, number of LPNs, number of UE
        /*genUnitTest(30, "smoke-01-03-05", 10, 10, 1, 1, 1, 5);
        genUnitTest(30, "01-50-20", 10, 10, 20, 15, 5, 20);
        genUnitTest(30, "01-150-500", 1000, 2000, 0, 150, 0, 500);
        genUnitTest(30, "01-50-1000", 2000.0, 2000.0, 0, 50, 0, 1000);
        genUnitTest(30, "01-100-1000", 2000.0, 2000.0, 0, 100, 0, 1000);
        genUnitTest(30, "01-100-2000", 2000.0, 2000.0, 0, 100, 0, 2000);
        genUnitTest(30, "01-100-3000", 2000.0, 2000.0, 0, 100, 0, 3000);
        genUnitTest(30, "01-100-4000", 2000.0, 2000.0, 0, 100, 0, 4000);
        genUnitTest(30, "01-100-5000", 2000.0, 2000.0, 0, 100, 0, 5000);
        genUnitTest(30, "01-100-10000", 2000.0, 2000.0, 0, 100, 0, 10000);
        genUnitTest(30, "cplex_01-10-100", 1000, 1000, 0, 10, 0, 100);
        genUnitTest(30, "cplex_01-10-200", 1000, 1000, 0, 10, 0, 200);
        genUnitTest(30, "cplex_01-10-300", 1000, 1000, 0, 10, 0, 300);
        genUnitTest(30, "cplex_01-10-400", 1000, 1000, 0, 10, 0, 400);
        genUnitTest(30, "cplex_02-10-400", 2000, 4000, 0, 10, 0, 400);
        genUnitTest(30, "cplex_03-10-400", 4000, 4000, 0, 10, 0, 400);
        genUnitTest(30, "cplex_03-10-750", 4000, 4000, 0, 10, 0, 750);
        genUnitTest(30, "cplex_03-15-1000", 4000, 4000, 0, 15, 0, 1000);
        genUnitTest(30, "cplex_03-20-2000", 4000, 4000, 0, 20, 0, 2000);
        genUnitTest(30, "cplex_03-20-5000", 4000, 4000, 0, 20, 0, 5000);
        genUnitTest(30, "cplex_03-20-10000", 4000, 4000, 0, 20, 0, 10000);
        genUnitTest(30, "cplex_03-20-20000", 4000, 4000, 0, 20, 0, 20000);*/
        /*genSimulTest(30, "simul-04-03-10", 5000, 6000, 3, 10);
        genSimulTest(30, "simul-04-03-50", 5000, 6000, 3, 50);

        for(int i=1; i<=9; i++) {
            genSimulTest(30, "simul-04-03-" + i + "00", 5000, 6000, 3, i*100);
        }

        genSimulTest(30, "simul-04-03-1000", 5000, 6000, 3, 1000);
        genSimulTest(30, "simul-04-03-1500", 5000, 6000, 3, 1500);
        genSimulTest(30, "simul-04-03-2000", 5000, 6000, 3, 2000);
        genSimulTest(30, "simul-04-03-2500", 5000, 6000, 3, 2500);
        */
        
//        for(int i=100; i<=1000; i+=100) {
//            genSimulTest(30, "simul-04-03-" + i, 5000, 6000, 3, i);
//        }
        
        genSimulTest(30, "simul-04-03-10000", 5000, 6000, 3, 10000);

        System.out.println("Done!");
    }

}
