import hetnet.AssociationHandler;
import hetnet.cell.CellFactory;
import hetnet.cell.CellType;
import hetnet.userEquipment.UserEquipmentFactory;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.HashSet;

import loadBalancer.offline.solver.CplexHelper;
import loadBalancer.offline.solver.LoadBalancer;
import loadBalancer.offline.strategy.LBStrategy;
import util.Properties;

/**
 * @author alexandre
 *
 */
public class Simulator {

    private enum ARGS {
        PROPERTIES_FILE,
        SIMUL_MODE,
        STRATEGY,
        INPUT_TEST,
        QOS_ATTENDED_USERS
    }

    private enum MODE {
        SOLVE,
        PARSE_SOL
    }

    private static boolean DEBUG_MODE = false;


    /**
     * @param frequency
     * @return estimation of QoS Factor based on predicted unattended UE per cell
     */
    /*private static double estimateQosFactor(double frequency) {
        int totalUnattendedUE = 0;
        for(Cell cell : CellFactory.getHashCell().values()) {
            totalUnattendedUE += cell.getLowerBoundUnattendedUE(frequency);
        }

        return (double)totalUnattendedUE / (double) UserEquipmentFactory.getHashUE().size();
    }*/

    /**
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) {
        if(args.length < 1) {
            System.out.println("\nUsage: Simulator <properties file> [simulator mode] [strategy] [input test] [rate of attended users - LP]\n");
            System.out.println("strategy:");
            for(LBStrategy lbstrategy : LBStrategy.values()) {
                System.out.println(lbstrategy.name());
            }
            System.out.println("\ne.g. 'java -Djava.library.path=/usr/lib/jni/ -cp ./libs/*:bin Simulator config/default.properties SOLVE" + LBStrategy.BEST_DOWNLINK.name() +" test01'\n");
            System.exit(1);
        }

        System.out.println("Simulator - " + args[ARGS.PROPERTIES_FILE.ordinal()]);

        try{
            File config_file = new File(args[ARGS.PROPERTIES_FILE.ordinal()]);
            if (config_file.isFile() == false || config_file.canRead() == false) {   
                throw new IOException("first parameter is not a readable file");
            }

            // Load internal configs
            Properties.loadConfigFile(config_file);

            // Setting simulator mode
            boolean parseSolution = false;

            if(args.length > ARGS.SIMUL_MODE.ordinal()) {
                parseSolution = args[ARGS.SIMUL_MODE.ordinal()].equals(MODE.PARSE_SOL.name());
            } else {
                parseSolution = Properties.get("defaultSimulMode").equals(MODE.PARSE_SOL.name()); ;
            }

            System.out.println("Mode: " + (parseSolution ? MODE.PARSE_SOL.name() : MODE.SOLVE.name()));

            // Setting strategy
            LBStrategy lbstrategy;
            String strategy = "";

            try {
                if(args.length > ARGS.STRATEGY.ordinal()) {
                    strategy = args[ARGS.STRATEGY.ordinal()];
                } else {
                    strategy = Properties.get("defaultStrategy");
                }

                lbstrategy = LBStrategy.valueOf(strategy);

            } catch(Exception e) {
                throw new IllegalArgumentException("There is no value with name '" + strategy + " in Enum " + LBStrategy.class.getName());
            }

            System.out.println("Strategy: " + strategy);

            // Setting input test
            String inputTest = "";

            if(args.length > ARGS.INPUT_TEST.ordinal()) {
                inputTest = args[ARGS.INPUT_TEST.ordinal()];
            } else {
                inputTest = Properties.get("defaultInputTest");
            }
            System.out.println("Input Test: " + inputTest + "\n");

            if(parseSolution && (!lbstrategy.name().startsWith("LP_") && !lbstrategy.name().startsWith("ILP_"))) {
                System.err.println("ERROR: Cannot parse solution for '" + strategy + "' on '" + inputTest + "'");
                System.exit(1);
            }

            // Initializing Simulator
            HashMap<String, HashSet<String>> assocMap;
            AssociationHandler.init(inputTest, DEBUG_MODE);
            //AssociationHandler.load_(); // Just for debug
            AssociationHandler.calcInitState();
            //LoadBalancer.runGLPKSample();

            // TODO: temporary frequency
            double frequency = UserEquipmentFactory.getHashUE().values().iterator().next().getService().getMinFrequency();
            double qosFactor = Double.valueOf(Properties.get("defaultQoSFactor"));
            if(args.length > ARGS.QOS_ATTENDED_USERS.ordinal()) {
                qosFactor = Double.valueOf(args[ARGS.QOS_ATTENDED_USERS.ordinal()]);
            }
            // Disabling this predicted value... It seems not to be working...
            // qosFactor = Math.max(qosFactor, estimateQosFactor(frequency));
            System.out.println("QoS Factor: " + qosFactor);

            if(parseSolution) {
                assocMap = CplexHelper.parseCPLEXSolution(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), lbstrategy, qosFactor, inputTest);
            } else {
                if(lbstrategy.name().startsWith("LP_") || lbstrategy.name().startsWith("ILP_")) {
                    //LoadBalancer.balanceGLPK(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), lbstrategy, qosFactor, inputTest);
                    assocMap = LoadBalancer.balanceLP(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), lbstrategy, qosFactor, inputTest);
                } else {
                    assocMap = LoadBalancer.balance(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), lbstrategy);
                }
            }

            if(assocMap != null) {
                System.out.println("\n\n### Association Map ###");
                AssociationHandler.printAssocMap(assocMap);
            }

            //            System.out.println("\n\n\nXML PARSER\n");
            //LoadBalancer.parseCPLEXSolution("config/test.out");
            /*
            System.out.println("\n\n### Result ###\n\n");
            CellFactory.printHashCell();
            UserEquipmentFactory.printHashUE();
             */
            System.out.println("\n\n### Statistics ###");
            int totalUnreachableUsers = UserEquipmentFactory.getTotalUnreachableUsers();
            int totalUnattendedUsers = UserEquipmentFactory.getTotalUnattendedUsers();
            int totalEmptyCells = CellFactory.getTotalEmptyCells();
            int totalZeroLoadCells = CellFactory.getTotalZeroLoadCells(frequency, null);
            NumberFormat defaultFormat = NumberFormat.getPercentInstance();
            defaultFormat.setMinimumFractionDigits(3);
            int totalUE = UserEquipmentFactory.getHashUE().size();
            double maxDLload = CellFactory.getMaxDownLinkLoadFactor(frequency, null);
            double minDLload = CellFactory.getMinDownLinkLoadFactor(frequency, null);
            System.out.println("QoS Factor: " + qosFactor);
            System.out.println("Total UE: " + totalUE);
            System.out.println("Total Unreachable UE: " + totalUnreachableUsers + " (" + 
                    defaultFormat.format((double)totalUnreachableUsers/(double)UserEquipmentFactory.getTotalUE()) + ")");
            System.out.println("Total Unattended Reachable UE: " + totalUnattendedUsers + " (" + 
                    defaultFormat.format((double)totalUnattendedUsers/(double)UserEquipmentFactory.getTotalUE()) + ")");
            System.out.println("Total Cells: " + CellFactory.getHashCell().size());
            System.out.println("Total Empty Cells (no reachable UEs): " + CellFactory.getTotalEmptyCells() + " (" + 
                    defaultFormat.format((double)totalEmptyCells/(double)CellFactory.getTotalCell()) + ")");
            System.out.println("Total Non-Empty and Zero-Load Cells: " + totalZeroLoadCells + " (" + 
                    defaultFormat.format((double)totalZeroLoadCells/(double)CellFactory.getTotalCell()) +")");
            System.out.format("Max Downlink Load Factor: %.3f\n", maxDLload);
            System.out.format("Min Downlink Load Factor: %.3f\n", minDLload);
            System.out.format("Load Balance Solution: %.3f\n", maxDLload - minDLload);
            System.out.println("Attended UE: " + CellFactory.getTotalAttendedUE(frequency, null));
            System.out.println("Attended UE Percentage: " + (double)CellFactory.getTotalAttendedUE(frequency, null)/(double)totalUE);
            System.out.println("Load Mean: " + defaultFormat.format(CellFactory.getLoadMean(frequency, null)));
            System.out.println("Load Median: " + defaultFormat.format(CellFactory.getLoadMedian(frequency, null)));
            System.out.println("Load Variance: " + CellFactory.getLoadVariance(frequency, null));
            System.out.println("Load Std Deviation: " + CellFactory.getLoadStdDeviation(frequency, null));

            for(CellType ctype : CellType.values()) {
                if(ctype != CellType.GENERIC){
                    int totalAttendedUE = CellFactory.getTotalAttendedUE(frequency, ctype);
                    double maxDLload_cell = CellFactory.getMaxDownLinkLoadFactor(frequency, ctype);
                    double minDLload_cell = CellFactory.getMinDownLinkLoadFactor(frequency, ctype);
                    System.out.println("Attended UE (" + ctype.name() +"): " + totalAttendedUE);
                    System.out.println("Attended UE Percentage (" + ctype.name() +"): " + (double)CellFactory.getTotalAttendedUE(frequency, ctype)/(double)totalUE);
                    System.out.println("Load Mean (" + ctype.name() +"): " + defaultFormat.format(CellFactory.getLoadMean(frequency, ctype)));
                    System.out.println("Load Median (" + ctype.name() +"): " + defaultFormat.format(CellFactory.getLoadMedian(frequency, ctype)));
                    System.out.println("Load Variance (" + ctype.name() +"): " + CellFactory.getLoadVariance(frequency, ctype));
                    System.out.println("Load Std Deviation (" + ctype.name() +"): " + CellFactory.getLoadStdDeviation(frequency, ctype));
                    System.out.format("Max Downlink Load Factor (%s): %.3f\n", ctype.name(), maxDLload_cell);
                    System.out.format("Min Downlink Load Factor (%s): %.3f\n", ctype.name(), minDLload_cell);
                    System.out.format("Load Balance Solution (%s): %.3f\n", ctype.name(), maxDLload_cell - minDLload_cell);
                }
            }

            System.out.println("\n\n");

            /* graphs:
             * LB: unsatisfied users
             * User association per tier * User Association for Load Balancing in Heterogeneous Cellular Networks
             */

            return;
        }
        catch(Exception e) {
            System.err.println("[ERROR] " + e.toString());
            System.exit(1);
        }

        System.exit(0);
    }

}
