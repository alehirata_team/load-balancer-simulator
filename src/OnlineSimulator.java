import hetnet.AssociationHandler;
import hetnet.cell.CellFactory;
import hetnet.userEquipment.UserEquipmentFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;

import loadBalancer.offline.strategy.LBStrategy;
import loadBalancer.online.solver.OnlineEvent;
import loadBalancer.online.solver.OnlineLoadBalancer;
import loadBalancer.online.solver.OnlineStatistics;
import loadBalancer.online.strategy.LBOnlineStrategy;
import util.Properties;

/**
 * @author alexandre
 *
 */
public class OnlineSimulator {

    private enum ARGS {
        PROPERTIES_FILE,
        SIMUL_MODE,
        STRATEGY,
        INPUT_TEST,
        QOS_ATTENDED_USERS
    }

    private enum MODE {
        SOLVE,
        PARSE_SOL
    }

    private static boolean DEBUG_MODE = false;

    /**
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) {
        if(args.length < 1) {
            System.out.println("\nUsage: OnlineSimulator <properties file> [simulator mode] [strategy] [input test] [rate of attended users - LP]\n");
            System.out.println("strategy:");
            for(LBStrategy lbstrategy : LBStrategy.values()) {
                System.out.println(lbstrategy.name());
            }
            System.out.println("\ne.g. 'java -Djava.library.path=/usr/lib/jni/ -cp ./libs/*:bin Simulator config/default.properties SOLVE " + LBStrategy.BEST_DOWNLINK.name() +" test01'\n");
            System.exit(1);
        }

        System.out.println("Simulator - " + args[ARGS.PROPERTIES_FILE.ordinal()]);

        try{
            File config_file = new File(args[ARGS.PROPERTIES_FILE.ordinal()]);
            if (config_file.isFile() == false || config_file.canRead() == false) {   
                throw new IOException("first parameter is not a readable file");
            }

            // Load internal configs
            Properties.loadConfigFile(config_file);

            // Setting simulator mode
            boolean parseSolution = false;

            if(args.length > ARGS.SIMUL_MODE.ordinal()) {
                parseSolution = args[ARGS.SIMUL_MODE.ordinal()].equals(MODE.PARSE_SOL.name());
            } else {
                parseSolution = Properties.get("defaultSimulMode").equals(MODE.PARSE_SOL.name()); ;
            }

            System.out.println("Mode: " + (parseSolution ? MODE.PARSE_SOL.name() : MODE.SOLVE.name()));

            // Setting strategy
            LBOnlineStrategy lbstrategy;
            String strategy = "";

            try {
                if(args.length > ARGS.STRATEGY.ordinal()) {
                    strategy = args[ARGS.STRATEGY.ordinal()];
                } else {
                    strategy = Properties.get("defaultStrategy");
                }

                lbstrategy = LBOnlineStrategy.valueOf(strategy);

            } catch(Exception e) {
                throw new IllegalArgumentException("There is no value with name '" + strategy + " in Enum " + LBStrategy.class.getName());
            }

            System.out.println("Strategy: " + strategy);

            // Setting input test
            String inputTest = "";

            if(args.length > ARGS.INPUT_TEST.ordinal()) {
                inputTest = args[ARGS.INPUT_TEST.ordinal()];
            } else {
                inputTest = Properties.get("defaultInputTest");
            }
            System.out.println("Input Test: " + inputTest + "\n");

            if(parseSolution && (!lbstrategy.name().startsWith("LP_") && !lbstrategy.name().startsWith("ILP_"))) {
                System.err.println("ERROR: Cannot parse solution for '" + strategy + "' on '" + inputTest + "'");
                System.exit(1);
            }

            // Initializing Simulator
            HashMap<String, HashSet<String>> assocMap;
            String infofile = inputTest.substring(0, inputTest.lastIndexOf('_')) +"_info.txt";
            OnlineStatistics.init(infofile);
            AssociationHandler.init(inputTest, DEBUG_MODE);
            //AssociationHandler.load_(); // Just for debug
            AssociationHandler.calcInitState();
            //LoadBalancer.runGLPKSample();

            // TODO: temporary frequency
            //double frequency = UserEquipmentFactory.getHashUE().values().iterator().next().getService().getMinFrequency();
            double qosFactor = Double.valueOf(Properties.get("defaultQoSFactor"));
            if(args.length > ARGS.QOS_ATTENDED_USERS.ordinal()) {
                qosFactor = Double.valueOf(args[ARGS.QOS_ATTENDED_USERS.ordinal()]);
            }
            // Disabling this predicted value... It seems not to be working...
            // qosFactor = Math.max(qosFactor, estimateQosFactor(frequency));
            System.out.println("QoS Factor: " + qosFactor);

            /*
            if(parseSolution) {
                assocMap = CplexHelper.parseCPLEXSolution(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), lbstrategy, qosFactor, inputTest);
            } else {
                if(lbstrategy.name().startsWith("LP_") || lbstrategy.name().startsWith("ILP_")) {
                    //LoadBalancer.balanceGLPK(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), lbstrategy, qosFactor, inputTest);
                    assocMap = LoadBalancer.balanceLP(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), lbstrategy, qosFactor, inputTest);
                } else {
                    assocMap = LoadBalancer.balance(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), lbstrategy);
                }
            }
             */

            SortedMap<Double, OnlineEvent> eventList = OnlineEvent.genEventListFromCSV(inputTest + "_evt.csv"); 
            assocMap = OnlineLoadBalancer.balance(UserEquipmentFactory.getHashUE(), CellFactory.getHashCell(), eventList, lbstrategy);

            if(DEBUG_MODE && assocMap != null) {
                System.out.println("\n\n### Association Map ###");
                AssociationHandler.printAssocMap(assocMap);
            }
            /* graphs:
             * LB: unsatisfied users
             * User association per tier * User Association for Load Balancing in Heterogeneous Cellular Networks
             */

            return;
        }
        catch(Exception e) {
            System.err.println("[ERROR] " + e.toString());
            System.exit(1);
        }

        System.exit(0);
    }

}
