package hetnet.userEquipment;

import hetnet.Service;
import hetnet.TransmissionLink;
import hetnet.cell.Cell;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * 
 */

/**
 * @author alexandre
 *
 */
public class UserEquipment {
    private String id;
    private double[] position;
    private double coverageRadio;
    private ArrayList<Cell> listReachableCells;
    //private ArrayList<Node> listReachableNodes;
    private Service service;
    private HashMap<String, TransmissionLink> mapLinkQuality; // key = cellId
    private double txPower;
    private double rxPower;
    private int priority;
    private Cell associatedCell;

    /**
     * 
     */
    UserEquipment() {
        this.listReachableCells = new ArrayList<Cell>();
        this.mapLinkQuality = new HashMap<String, TransmissionLink>();
        this.associatedCell = null;
    }

    /**
     * @return the position
     */
    public double[] getPosition() {
        return this.position;
    }

    /**
     * @param x
     * @param y
     */
    public void setPosition(double x, double y) {
        this.position = new double[2];
        this.position[0] = x;
        this.position[1] = y;
    }

    /**
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the coverageRadio
     */
    public double getCoverageRadio() {
        return this.coverageRadio;
    }

    /**
     * @param coverageRadio the coverageRadio to set
     */
    public void setCoverageRadio(double coverageRadio) {
        this.coverageRadio = coverageRadio;
    }

    /**
     * @return the listReachableCells
     */
    public ArrayList<Cell> getListReachableCells() {
        return this.listReachableCells;
    }

    /**
     * @return the listReachableNodes
     */
    /*public ArrayList<Node> getListReachableNodes() {
        return this.listReachableNodes;
    }*/


    /**
     * @return the service
     */
    public Service getService() {
        return this.service;
    }


    /**
     * @param service the service to set
     */
    public void setService(Service service) {
        this.service = service;
    }


    /**
     * @param cellId
     * @return
     */
    public TransmissionLink getTransmisisonLink(String cellId) {
        return this.mapLinkQuality.get(cellId);
    }


    /**
     * @param cellId
     * @param txLink
     */
    public void setTransmisisonLink(String cellId, TransmissionLink txLink) {
        this.mapLinkQuality.put(cellId,txLink);
    }


    /**
     * @return the txPower
     */
    public double getTxPower() {
        return this.txPower;
    }

    /**
     * @param txPower the txPower to set
     */
    public void setTxPower(double txPower) {
        this.txPower = txPower;
    }

    /**
     * @return the rxPower
     */
    public double getRxPower() {
        return this.rxPower;
    }

    /**
     * @param rxPower the rxPower to set
     */
    public void setRxPower(double rxPower) {
        this.rxPower = rxPower;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return this.priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the associatedCell
     */
    public Cell getAssociatedCell() {
        return associatedCell;
    }

    /**
     * @param associatedCell the associatedCell to set
     */
    public void setAssociatedCell(Cell associatedCell) {
        this.associatedCell = associatedCell;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();

        result.append(this.getClass().getName());
        result.append(" { ");
        result.append("id: " + this.id);
        result.append(", position: (" + this.position[0] + ", " + this.position[1] + ")");
        result.append(", txPower: " + this.txPower);
        result.append(", rxPower: " + this.rxPower);
        result.append(", service: " + this.service);
        result.append(" }");

        return result.toString();
    }

}
