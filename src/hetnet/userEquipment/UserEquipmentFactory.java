package hetnet.userEquipment;

import hetnet.Service;
import hetnet.cell.Cell;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import au.com.bytecode.opencsv.CSVReader;

/**
 * 
 */

/**
 * @author alexandre
 *
 */
public abstract class UserEquipmentFactory {

    private static int totalUE;
    private static HashMap<String, UserEquipment> hashUE;
    private static HashMap<String, Service> hashService;

    public static enum UECSVCol{
        Type,
        Id,
        PosX,
        PosY,
        ServiceName
    }


    /**
     * @throws IOException 
     * @throws NumberFormatException 
     * 
     */
    public static void initUEFactory(String ueFile, String serviceFile, boolean debugMode) throws NumberFormatException, IOException{
        totalUE = 0;
        hashUE = new HashMap<String,UserEquipment>();
        hashService = Service.createHashService(serviceFile, debugMode);

        CSVReader reader = new CSVReader(new FileReader(ueFile), ',', '"', 1);
        String [] col;

        while ((col = reader.readNext()) != null) {
            String id = col[UECSVCol.Id.ordinal()];
            double posX = Double.valueOf(col[UECSVCol.PosX.ordinal()]);
            double posY = Double.valueOf(col[UECSVCol.PosY.ordinal()]);
            String serviceName = col[UECSVCol.ServiceName.ordinal()];

            if(!hashService.containsKey(serviceName)){
                System.err.println("[ERROR] No service '" + serviceName + "' defined on '" + serviceFile + "'");
                System.exit(1);
            }

            UserEquipment ue = UserEquipmentFactory.createUE(id, posX, posY, hashService.get(serviceName));            
            
            if (debugMode) {
                // System.out.print("\n"); printCSVLine(col); // Debug
                System.out.println(ue); // Debug
            }
        }

        reader.close();
    }

    /**
     * @param col
     */
    /*private static void printCSVLine(String[] col){
        for(int i=0; i<UECSVCol.values().length; i++){
            System.out.println(UECSVCol.values()[i].toString() + ": " + col[i]);
        }
    }*/

    /**
     * @return the hashUE
     */
    public static HashMap<String, UserEquipment> getHashUE() {
        return hashUE;
    }

    /**
     * @return
     */
    public static int getTotalUE(){
        return totalUE;
    }

    /**
     * @return
     */
    public static UserEquipment createUE(String id, double posX, double posY, Service service){
        totalUE++;
        UserEquipment ue = new UserEquipment();
        ue.setId(id);
        ue.setPosition(posX, posY);
        ue.setService(service);
        hashUE.put(id, ue);

        return ue;
    }

    public static void printHashUE(){
        for(UserEquipment ue : hashUE.values()){
            ue.toString();
        }
    }

    public static int getTotalUnattendedUsers(){
        int count = 0;

        for(UserEquipment ue : hashUE.values()){
            if(ue.getListReachableCells().size() > 0){
                for(Cell c : ue.getListReachableCells()){
                    if(c.isAssociated(ue.getId())){
                        count--;
                        break;
                    }
                }
                count++;
            }
        }

        return count;
    }

    public static int getTotalUnreachableUsers(){
        int count = 0;

        for(UserEquipment ue : hashUE.values()){
            if(ue.getListReachableCells().size() == 0){
                count++;
            }
        }

        return count;
    }
}
