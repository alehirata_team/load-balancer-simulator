package hetnet;

import util.Properties;
import hetnet.cell.Cell;
import hetnet.cell.CellFactory;
import hetnet.node.Node;
import hetnet.userEquipment.UserEquipment;

/**
 * @author alexandre
 *
 */
public class TransmissionLink {

    private double frequency;
    private String ueId;
    private String cellId;
    private double ue2CellTxRate;
    private double cell2UETxRate;
    private double effUE2CellTxRate;
    private double effCell2UETxRate;
    private static double thermalNoise = 0; //TODO: Find out the real value
    /*    private static double backgroundNoisePower = 0; //TODO: Find out the real value 
    private static double ueInterferencePower = 0; //TODO: Find out the real value */

    /**
     * 
     */
    public TransmissionLink(UserEquipment ue, Cell cell, double frequency) {
        this.ueId = ue.getId();
        this.cellId = cell.getId();
        this.frequency = frequency;
        calcLinkQuality(ue, cell, frequency);
    }

    public void calcLinkQuality(UserEquipment ue, Cell cell, double frequency){
        this.ue2CellTxRate = ue.getService().getMaxTxRate();
        this.cell2UETxRate = ue.getService().getMaxRxRate();
        this.effCell2UETxRate = TransmissionLink.calcEffCell2UETxRate(ue, cell);
        this.effUE2CellTxRate = this.ue2CellTxRate; //TODO: Calculate real value
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();

        result.append(this.getClass().getName());
        result.append(" { ");
        result.append("ueId: " + this.ueId);
        result.append(", cellId: " + this.cellId);
        result.append(", ue2CellTxRate: " + this.ue2CellTxRate);
        result.append(", cell2UETxRate: " + this.cell2UETxRate);
        result.append(", effUE2CellTxRate: " + this.effUE2CellTxRate);
        result.append(", effCell2UETxRate: " + this.effCell2UETxRate);
        result.append(", Frequency: " + this.frequency + ")");
        result.append(" }");

        return result.toString();
    }

    /**
     * @return the frequency
     */
    public double getFrequency() {
        return this.frequency;
    }

    /**
     * @param frequency the frequency to set
     */
    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    /**
     * @return the ueId
     */
    public String getUeId() {
        return this.ueId;
    }

    /**
     * @param ueId the ueId to set
     */
    public void setUeId(String ueId) {
        this.ueId = ueId;
    }

    /**
     * @return the cellId
     */
    public String getCellId() {
        return this.cellId;
    }

    /**
     * @param cellId the cellId to set
     */
    public void setCellId(String cellId) {
        this.cellId = cellId;
    }

    /**
     * @return the ue2CellTxRate
     */
    public double getUe2CellTxRate() {
        return this.ue2CellTxRate;
    }

    /**
     * @param ue2CellTxRate the ue2CellTxRate to set
     */
    public void setUe2CellTxRate(double ue2CellTxRate) {
        this.ue2CellTxRate = ue2CellTxRate;
    }

    /**
     * @return the cell2UETxRate
     */
    public double getCell2UETxRate() {
        return this.cell2UETxRate;
    }

    /**
     * @param cell2ueTxRate the cell2UETxRate to set
     */
    public void setCell2UETxRate(double cell2ueTxRate) {
        this.cell2UETxRate = cell2ueTxRate;
    }

    /**
     * @return the effUE2CellTxRate
     */
    public double getEffUE2CellTxRate() {
        return this.effUE2CellTxRate;
    }

    /**
     * @param effUE2CellTxRate the effUE2CellTxRate to set
     */
    public void setEffUE2CellTxRate(double effUE2CellTxRate) {
        this.effUE2CellTxRate = effUE2CellTxRate;
    }

    /**
     * @return the effCell2UETxRate
     */
    public double getEffCell2UETxRate() {
        return this.effCell2UETxRate;
    }

    /**
     * @param effCell2UETxRate the effCell2UETxRate to set
     */
    public void setEffCell2UETxRate(double effCell2UETxRate) {
        this.effCell2UETxRate = effCell2UETxRate;
    }

    /**
     * @param pos1
     * @param pos2
     * @return distance between pos1 and pos2 in kilometers
     */
    private static double getDistance(double[] pos1, double[] pos2){
        return Math.sqrt(Math.pow(pos1[0] - pos2[0], 2) + Math.pow(pos1[1] - pos2[1], 2));
    }

    /**
     * @param uePos
     * @param nodePos
     * @return pathloss
     */
    private static double getPathloss(double[] uePos, double[] nodePos){
        // Values taken from: "A mathematical perspective of self-optimizing wireless networks"
        double lA = 148.1; // Typical value in dB
        double lB = 37.6; // Typical value in dB
        double Lpl = lA + lB + Math.log10(TransmissionLink.getDistance(uePos, nodePos)/1000); // /1000 = /km

        return Lpl;
    }

    private static double getVectorNorm(double[] v){
        return Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2));
    }

    private static double getAzimuthBeamPattern(double[] uePos, double[] nodePos){
        // Values taken from: "A mathematical perspective of self-optimizing wireless networks"
        double B0 = 20; // Backward attenuation [dB]
        double dPhi = 70; // Azimuth beam width
        double phi = 0; // {0, 120, -120} azimuth orientation [degrees]
        double angle = Math.acos((uePos[0]*nodePos[0]+uePos[1]*nodePos[1])/
                (TransmissionLink.getVectorNorm(uePos)*TransmissionLink.getVectorNorm(nodePos)));
        double B = - Math.max(B0, 12*Math.sqrt((Math.toDegrees(angle) - phi)/dPhi)); 

        return B;
    }

    private static double getElevationBeamPattern(double[] uePos, double[] nodePos, double nodeHeight){
        // Values taken from: "A mathematical perspective of self-optimizing wireless networks"
        double B0 = 20; // Backward attenuation [dB]
        double dTheta = 10; // Elevation width
        double theta = 0; // Downlit
        double angle = Math.atan(nodeHeight/TransmissionLink.getDistance(uePos, nodePos));
        double B = - Math.max(B0, 12*Math.sqrt((Math.toDegrees(angle) - theta)/dTheta)); 

        return B;
    }

    private static double get3DBeamPattern(double[] uePos, double[] nodePos, double nodeHeight){
        return TransmissionLink.getAzimuthBeamPattern(uePos, nodePos) + 
                TransmissionLink.getElevationBeamPattern(uePos, nodePos, nodeHeight);
    }

    private static double getShadowing(double[] uePos, double[] nodePos){
        // TODO: determine how to calculate it
        return 0;
    }

    private static double getOverallAttenuation(double[] uePos, double[] nodePos, double nodeHeight){
        return TransmissionLink.getPathloss(uePos, nodePos) + 
                TransmissionLink.get3DBeamPattern(uePos, nodePos, nodeHeight) +
                TransmissionLink.getShadowing(uePos, nodePos);
    }

    private static double getLinearOverallAttenuation(double[] uePos, double[] nodePos, double nodeHeight){
        return Math.pow(10, TransmissionLink.getOverallAttenuation(uePos, nodePos, nodeHeight)/10);
    }

    /**
     * X(u) connection function
     * @param ue
     * @return
     */
    private static String getConnectCell(UserEquipment ue){
        String cellId = "";
        double argMax = 0;
        double curArg = 0;

        for(Cell c : ue.getListReachableCells()){
            curArg = c.getTxPower() * TransmissionLink.getLinearOverallAttenuation(ue.getPosition(), c.getPosition(), c.getNodeHeight());

            if(argMax < curArg){
                cellId = c.getId();
                argMax = curArg;
            }
        }

        return cellId;
    }

    /**
     * SINR = P / (I + N)  [dB]
     * P = TX(s) / PL(s,r) (This represents the received power).
     * I = sum(i != s) TXi / PL(i,r) (This represents the interference power of other simultaneous transmissions)
     * N = (Background) noise power (Typically fixed)
     * TX(s) = Transmit power of the sender s (In a uniform power network, this is assumed to the same for all stations)
     * PL(s,r) = Path loss between the sender s and receiver r (taking into account the euclidean distance between them)
     * @param ue
     * @param node
     * @return
     */
    /*public static double getSINR(UserEquipment ue, Cell cell){*/
    /*double I = node.getListReachableUE().size()*(TransmissionLink.ueInterferencePower-1);
        double P = node.getTxPower()/TransmissionLink.getPathloss(ue.getPosition(), node.getPosition());
        double N = TransmissionLink.backgroundNoisePower;

        return P / (I + N);*/
    /*
        double SINRu = 0;
        Node Xu = NodeFactory.getHashNode().get(TransmissionLink.getConnectCell(ue));
        double extInterferencePower = 0;
        double effXu2UETxRate = TransmissionLink.calcEffNode2UETxRate(ue, Xu);

        for(Cell c : ue.getListReachableCells()){
            extInterferencePower += TransmissionLink.calcEffCell2UETxRate(ue, c);
        }

        extInterferencePower -= effXu2UETxRate;
        SINRu =  effXu2UETxRate / TransmissionLink.thermalNoise + extInterferencePower; 

        return SINRu;
    }*/

    /**
     * Calculate the achievable data rate via Shannon formula
     * @param bandwidth in MHz
     * @param sinr
     * @return achievable data rate in kbps
     */
    public static double getAchievableDataRate(double bandwidth, double sinr){
        return bandwidth * Math.log1p(sinr) / Math.log(2);
    }

    public static double getSINR(UserEquipment ue, Cell cell){
        /*double I = node.getListReachableUE().size()*(TransmissionLink.ueInterferencePower-1);
        double P = node.getTxPower()/TransmissionLink.getPathloss(ue.getPosition(), node.getPosition());
        double N = TransmissionLink.backgroundNoisePower;

        return P / (I + N);*/

        double SINRu = 0;
        Cell Xu = CellFactory.getHashCell().get(TransmissionLink.getConnectCell(ue));
        double extInterferencePower = 0;
        double effXu2UETxRate = TransmissionLink.calcEffCell2UETxRate(ue, Xu);

        for(Cell c : ue.getListReachableCells()){
            extInterferencePower += TransmissionLink.calcEffCell2UETxRate(ue, c);
        }

        extInterferencePower -= effXu2UETxRate;
        SINRu =  effXu2UETxRate / TransmissionLink.thermalNoise + extInterferencePower; 

        return SINRu;
    }

    public static double calcEffNode2UETxRate(UserEquipment ue, Node node){
        return node.getTxPower() * TransmissionLink.getLinearOverallAttenuation(ue.getPosition(), node.getPosition(), node.getHeight());
    }

    // TODO: Verify the math here
    public static double calcEffCell2UETxRate(UserEquipment ue, Cell cell){
        return cell.getTxPower() * TransmissionLink.getLinearOverallAttenuation(ue.getPosition(), cell.getPosition(), cell.getNodeHeight());
    }

    public static int getRequiredMinNumPRBs(UserEquipment ue, Cell cell){
        // Shannon-Hartley theorem
        double effDownlinkRx = Math.min(ue.getService().getMinRxRate(), 
                TransmissionLink.getAchievableDataRate(cell.getNode().getBandwidth(), TransmissionLink.getSINR(ue, cell)));
        int nSubFrames = Integer.valueOf(Properties.get("defaultTestInterval"));

        return Node.getNeededPRB(effDownlinkRx, nSubFrames);
    }
}

