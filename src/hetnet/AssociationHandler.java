package hetnet;

import hetnet.cell.Cell;
import hetnet.cell.CellFactory;
import hetnet.node.Node;
import hetnet.node.NodeFactory;
import hetnet.node.NodeType;
import hetnet.userEquipment.UserEquipment;
import hetnet.userEquipment.UserEquipmentFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import au.com.bytecode.opencsv.CSVReader;

/**
 * @author alexandre
 *
 */
public abstract class AssociationHandler {

    /**
     * Initialize the Abstract Factories
     * @throws IOException 
     */
    public static void init(String inputPrefix, boolean debugMode) throws IOException{
        String nodeFile = inputPrefix + "_node.csv";
        String ueFile = inputPrefix + "_ue.csv";
        String serviceFile = inputPrefix + "_service.csv";
        CellFactory.initCellFactory();
        NodeFactory.initNodeFactory(nodeFile, debugMode);
        UserEquipmentFactory.initUEFactory(ueFile, serviceFile, debugMode);
    }

    /**
     * DEBUG: Load pre-defined values for Nodes and UEs
     */
    public static void load_(){
        int MAX_UE = 5;
        int MAX_ENodeB = 1;
        int MAX_PicoLPN = 2;

        // eNodeB
        for(int i=1; i<=MAX_ENodeB; i++){
            String nodeId = "M" + i;
            HashMap<Double, Double> totalCapacity = new HashMap<Double, Double>();
            totalCapacity.put(1800.0, 100.0);
            Node node = NodeFactory.createNode(NodeType.ENODEB, nodeId, 10 + i*10, 10 + i*10, 10 + i, 10.0, totalCapacity);
            NodeFactory.getHashNode().put(nodeId, node);
        }

        // Pico Node
        for(int i=1; i<=MAX_PicoLPN; i++){
            String nodeId = "P" + i;
            HashMap<Double, Double> totalCapacity = new HashMap<Double, Double>();
            totalCapacity.put(1800.0, 10.0);
            Node node = NodeFactory.createNode(NodeType.PICO, nodeId, 15 + i*10, 15 + i*10, 2 + i, 10.0, totalCapacity);
            NodeFactory.getHashNode().put(nodeId, node);
        }

        // Setting Service
        Service s1 = new Service("s1", 1, 1, 1, 1, 1, 1);

        // Setting UE
        for(int i=1; i<=MAX_UE; i++){
            String ueId = "UE" + i;
            UserEquipment ue = UserEquipmentFactory.createUE(ueId, 15 + i*10, 15 + i*10, s1);
            UserEquipmentFactory.getHashUE().put(ueId, ue);
        }
    }

    public static void load(String filename) throws IOException{
        CSVReader reader = new CSVReader(new FileReader(filename), ',', '\'', 1);
        String [] col;

        while ((col = reader.readNext()) != null) {
            System.out.println(col[0] + col[1]);
        }

        reader.close();
    }

    /**
     * Calculate the initial state of the system
     */
    public static void calcInitState(){
        // Fill the list of reachableUE, reachableNodes and mapLinkQuality
        for(UserEquipment ue : UserEquipmentFactory.getHashUE().values()){
            for(Cell cell : CellFactory.getHashCell().values()){
                if(cell.isInCoverageArea(ue.getPosition())){
                    TransmissionLink txLink = new TransmissionLink(ue, cell, ue.getService().getMinFrequency());
                    //System.out.println(txLink);
                    ue.setTransmisisonLink(cell.getId(), txLink);
                    ue.getListReachableCells().add(cell);
                    cell.setTransmisisonLink(ue.getId(), txLink);
                    cell.getListReachableUE().add(ue);
                }
            }
        }
    }

    public static void associateUE(HashMap<String,HashSet<String>> assocMap){
        HashMap<String, Cell> hCells = CellFactory.getHashCell();
        for(String cellId : assocMap.keySet()){
            for(String ueId : assocMap.get(cellId)){
                hCells.get(cellId).associateUE(ueId);
            }
        }
    }
    
    public static void printAssocMap(HashMap<String,HashSet<String>> assocMap){
        for(String cellId : assocMap.keySet()){
            System.out.print(cellId + " = { ");
            for(String ueId : assocMap.get(cellId)){
                System.out.print(ueId + " ");
            }
            System.out.print("}\n");
        }
    }
}