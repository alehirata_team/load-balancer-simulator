package hetnet.cell;


import hetnet.node.Node;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.descriptive.moment.Variance;
import org.apache.commons.math3.stat.descriptive.rank.Median;

/**
 * @author alexandre
 *
 */
public abstract class CellFactory {

    private static int totalCell;
    private static int totalMacroCell;
    private static int totalPicoCell;
    private static int totalFemtoCell;
    private static HashMap<String,Cell> hashCell;
    //private static final double MIN_PERC_UE_PER_BS = 0.1;

    public static enum CellCSVCol{
        Type,
        Id,
        PosX,
        PosY,
        CoverRadio,
        TxPower,
        RxPower
    }

    /**
     * @throws IOException 
     * 
     */
    public static void initCellFactory() throws IOException{
        totalCell = 0;
        totalMacroCell = 0;
        totalPicoCell = 0;
        totalFemtoCell = 0;
        hashCell = new HashMap<String,Cell>();
    }

    /**
     * @return the hashCell
     */
    public static HashMap<String, Cell> getHashCell() {
        return hashCell;
    }

    /**
     * @return
     */
    public static int getTotalCell(){
        return totalCell;
    }

    /**
     * @return
     */
    public static int getTotalMacroCell(){
        return totalMacroCell;
    }

    /**
     * @return
     */
    public static int getTotalPicoCell(){
        return totalPicoCell;
    }

    /**
     * @return
     */
    public static int getTotalFemtoCell(){
        return totalFemtoCell;
    }

    public static Cell createCell(CellType type, String id, double posX, double posY, double coverRadio, double txPower, double rxPower, Node node){
        Cell cell = createCell(type);
        cell.setId(id);
        cell.setPosition(posX, posY);
        cell.setCoverageRadio(coverRadio);
        cell.setTxPower(txPower);
        cell.setRxPower(rxPower);
        cell.setNode(node);
        cell.setNodeHeight(node.getHeight());
        hashCell.put(id, cell);

        return cell;
    }

    /**
     * @param type
     * @return
     */
    public static Cell createCell(CellType type){
        totalCell++;

        switch(type){
            case MACRO:
                totalMacroCell++;
                break;

            case PICO:
                totalPicoCell++;
                break;

            case FEMTO:
                totalFemtoCell++;
                break;

            case GENERIC:
            default:
                break;
        }

        return new Cell(type);
    }

    public static void printHashCell(){
        for(Cell c : hashCell.values()){
            System.out.println(c);
        }
    }

    public static double getMinLoad(double frequency, CellType ctype){
        double minLoad = 0.0;
        for(Cell c : hashCell.values()){
            if(ctype == null || c.getType() == ctype){
                minLoad = Math.min(minLoad, (1.0 - c.getAvailDLCapacity(frequency) / c.getTotalDLCapacity(frequency)));
            }
        }

        return minLoad;
    }

    public static double getMaxLoad(double frequency, CellType ctype){
        double maxLoad = 0.0;
        for(Cell c : hashCell.values()){
            if(ctype == null || c.getType() == ctype){
                maxLoad = Math.max(maxLoad, (1.0 - c.getAvailDLCapacity(frequency) / c.getTotalDLCapacity(frequency)));
            }
        }

        return maxLoad;
    }

    public static int getTotalEmptyCells(){
        int count = 0;

        for(Cell c : hashCell.values()){
            if(c.getListReachableUE().size() == 0){
                count++;
            }
        }

        return count;
    }

    public static int getTotalZeroLoadCells(double frequency, CellType ctype){
        int count = 0;

        for(Cell c : hashCell.values()){
            if(ctype == null || c.getType() == ctype){
                if(c.getListReachableUE().size() > 0 && c.getAvailDLCapacity(frequency) == c.getTotalDLCapacity(frequency)){
                    count++;
                }
            }
        }

        return count;
    }

    public static double getMaxDownLinkLoadFactor(double frequency, CellType ctype){
        double maxLoad = 0.0;
        for(Cell c : hashCell.values())
            if(ctype == null || c.getType() == ctype){
                if(c.getListReachableUE().size() > 0){
                    maxLoad = Math.max(maxLoad, c.getDownLinkLoadFactor(frequency));
                    // Ignore cells that shall not be considered in the simulation
                    /*double dlFactor = c.getDownLinkLoadFactor(frequency);
                    if(!(dlFactor == 1.0 && c.getListReachableUE().size() <= UserEquipmentFactory.getTotalUE() * MIN_PERC_UE_PER_BS)){
                        maxLoad = Math.max(maxLoad, dlFactor);
                    }*/
                    /*else{
                        System.out.println("*** IGNORING: DL load factor=1.0 for Cell=" + c.getId() + ", reachableListSize=" + c.getListReachableUE().size());
                    }*/
                }
            }

        return maxLoad;
    }
    
    public static double getMinDownLinkLoadFactor(double frequency, CellType ctype){
        double minLoad = 100.0;
        for(Cell c : hashCell.values())
            if(ctype == null || c.getType() == ctype){
                if(c.getListReachableUE().size() > 0){
                    minLoad = Math.min(minLoad, c.getDownLinkLoadFactor(frequency));
                }
            }

        return minLoad;
    }

    public static int getTotalAttendedUE(double frequency, CellType ctype){
        int total = 0;
        for(Cell c : hashCell.values()){
            if((ctype == null || c.getType() == ctype) && c.getAssocUE() != null){
                total += c.getAssocUE().get(frequency) != null ? c.getAssocUE().get(frequency).size() : 0;
            }
        }

        return total;
    }

    public static double getLoadMean(double frequency, CellType ctype){
        double total = 0.0;
        int n = 0;
        for(Cell c : hashCell.values()){
            if(ctype == null || c.getType() == ctype){
                if(c.getListReachableUE().size() > 0){
                    total += 1.0 - c.getAvailDLCapacity(frequency) / c.getTotalDLCapacity(frequency);
                    n++;
                }
            }
        }
        
        return (double)(total/n);
    }

    public static double getLoadMedian(double frequency, CellType ctype){
        List<Double> lLoad = new ArrayList<Double>();
        for(Cell c : hashCell.values()){
            if(ctype == null || c.getType() == ctype){
                if(c.getListReachableUE().size() > 0){ 
                    lLoad.add(1.0 - c.getAvailDLCapacity(frequency) / c.getTotalDLCapacity(frequency));
                }
            }
        }

        Median m = new Median();
        double[] aLoad = new double[lLoad.size()];
        int i = 0;
        for(double l : lLoad){
            aLoad[i] = l;
        }

        return m.evaluate(aLoad);
    }

    public static double getLoadVariance(double frequency, CellType ctype){
        List<Double> lLoad = new ArrayList<Double>();
        for(Cell c : hashCell.values()){
            if(ctype == null || c.getType() == ctype){
                if(c.getListReachableUE().size() > 0){ 
                    lLoad.add(1.0 - c.getAvailDLCapacity(frequency) / c.getTotalDLCapacity(frequency));
                }
            }
        }

        Variance v = new Variance();
        double[] aLoad = new double[lLoad.size()];
        int i = 0;
        for(double l : lLoad){
            aLoad[i] = l;
        }

        return v.evaluate(aLoad);
    }

    public static double getLoadStdDeviation(double frequency, CellType ctype){
        List<Double> lLoad = new ArrayList<Double>();
        for(Cell c : hashCell.values()){
            if(ctype == null || c.getType() == ctype){
                if(c.getListReachableUE().size() > 0){ 
                    lLoad.add(1.0 - c.getAvailDLCapacity(frequency) / c.getTotalDLCapacity(frequency));
                }
            }
        }

        StandardDeviation v = new StandardDeviation();
        double[] aLoad = new double[lLoad.size()];
        int i = 0;
        for(double l : lLoad){
            aLoad[i] = l;
        }

        return v.evaluate(aLoad);
    }
}
