/**
 * 
 */
package hetnet.cell;

/**
 * @author alexandre
 *
 */
public enum CellType {
    GENERIC,
    MACRO,
    PICO,
    FEMTO
}
