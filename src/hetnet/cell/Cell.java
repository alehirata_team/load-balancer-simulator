package hetnet.cell;

import hetnet.Service;
import hetnet.TransmissionLink;
import hetnet.node.ENodeB;
import hetnet.node.Node;
import hetnet.userEquipment.UserEquipment;
import hetnet.userEquipment.UserEquipmentFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

import util.Properties;

/**
 * @author alexandre
 */
public class Cell implements Comparable<Cell> {

    private String id;
    private CellType type;
    private double[] position;
    private double coverageRadio;
    private ArrayList<UserEquipment> listReachableUE;
    private HashMap<String, TransmissionLink> mapLinkQuality;
    private HashMap<Double, ArrayList<String>> assocUE;
    private ArrayList<UserEquipment> ueBlackList;
    private double txPower;
    private double rxPower;
    private double bandwidth;
    private HashMap<Double, Double> availDLCapacityMap; // Capacity in PRBs
    private HashMap<Double, Double> totalDLCapacityMap;
    private HashMap<Double, Double> availULCapacityMap; // Capacity in PRBs
    private HashMap<Double, Double> totalULCapacityMap;
    private HashMap<Double, RangeExpansionInfo> rangeExpansionInfoMap;
    private Node node;


    /*
     * 
In Release 8 the only supported bandwidths are 1.4 MHz, 3 MHz, 5 MHz, 10 MHz, 15, MHz, 20 MHz. The number of "Usable" Resource Blocks allowed in each of the bands is the following:

1.4 MHz  - Usable PRBs = 6
3  MHz  - Usable PRBs = 15
5  MHz  - Usable PRBs = 25
10  MHz  - Usable PRBs = 50
15  MHz  - Usable PRBs = 75
20  MHz  - Usable PRBs = 100

All the remaining SubCarriers are reserved for Guard SubCarriers and cannot by used for any transmissions either in the Uplink or the Downlink. Hope that helps ...

Vishal

http://lteuniversity.com/ask_the_expert/f/69/t/2592.aspx


LTE: 15bps/Hz => 20MHz = 300Mbps = 100 PRBs => 1 PRB = 3Mbps // REV

     */

    /**
     * height in meters
     */
    private double nodeHeight;
    private int priority;

    /**
     * 
     */
    Cell(CellType type) {
        this.type = type;
        this.listReachableUE = new ArrayList<UserEquipment>();
        this.mapLinkQuality = new HashMap<String, TransmissionLink>();
        this.assocUE = new HashMap<Double, ArrayList<String>>();
        this.ueBlackList = new ArrayList<UserEquipment>();
        this.availDLCapacityMap = new HashMap<Double, Double>();
        this.totalDLCapacityMap = new HashMap<Double, Double>();
        this.availULCapacityMap = new HashMap<Double, Double>();
        this.totalULCapacityMap = new HashMap<Double, Double>();
        this.rangeExpansionInfoMap = new HashMap<Double, RangeExpansionInfo>();
        int nSubFrames = Integer.valueOf(Properties.get("defaultTestInterval"));

        // TODO: Temporary default value
        this.setAvailDLCapacity(1800.0, Node.getNumPRB(ENodeB.DEFAULT_BANDWIDTH, nSubFrames));
        this.setTotalDLCapacity(1800.0, Node.getNumPRB(ENodeB.DEFAULT_BANDWIDTH, nSubFrames));
        this.setAvailULCapacity(1800.0, Node.getNumPRB(ENodeB.DEFAULT_BANDWIDTH, nSubFrames));
        this.setTotalULCapacity(1800.0, Node.getNumPRB(ENodeB.DEFAULT_BANDWIDTH, nSubFrames));
    }

    public boolean associateUE(String ueId){
        UserEquipment ue = UserEquipmentFactory.getHashUE().get(ueId);
        Service service = ue.getService();
        double frequency = service.getMinFrequency();

        if(!this.assocUE.containsKey(frequency)){
            this.assocUE.put(frequency, new ArrayList<String>());
        }

        int nSubFrames = Integer.valueOf(Properties.get("defaultTestInterval"));
        double neededDLCapacity = (double)Node.getNeededPRB(this.mapLinkQuality.get(ueId).getCell2UETxRate(), nSubFrames);
        double neededULCapacity = (double)Node.getNeededPRB(this.mapLinkQuality.get(ueId).getUe2CellTxRate(), nSubFrames);
        if(this.getAvailDLCapacity(frequency) < neededDLCapacity || this.getAvailULCapacity(frequency) < neededULCapacity){
            return false;
        }

        this.assocUE.get(frequency).add(ueId);
        this.decrAvailDLCapacity(frequency, neededDLCapacity);
        this.decrAvailULCapacity(frequency, neededULCapacity);
        ue.setAssociatedCell(this);

        return true;
    }

    public boolean disassociateUE(String ueId){
        UserEquipment ue = UserEquipmentFactory.getHashUE().get(ueId);
        Service service = ue.getService();
        double frequency = service.getMinFrequency();

        if(!this.assocUE.containsKey(frequency) || !this.assocUE.get(frequency).contains(ueId)){
            return true;
        }

        this.assocUE.get(frequency).remove(ueId);

        int nSubFrames = Integer.valueOf(Properties.get("defaultTestInterval"));
        double neededDLCapacity = (double)Node.getNeededPRB(this.mapLinkQuality.get(ueId).getCell2UETxRate(), nSubFrames);
        double neededULCapacity = (double)Node.getNeededPRB(this.mapLinkQuality.get(ueId).getUe2CellTxRate(), nSubFrames);
        this.incrAvailDLCapacity(frequency, neededDLCapacity);
        this.incrAvailULCapacity(frequency, neededULCapacity);
        ue.setAssociatedCell(null);

        return true;
    }

    public boolean isAssociated(String ueId){
        UserEquipment ue = UserEquipmentFactory.getHashUE().get(ueId);
        Service service = ue.getService();
        double frequency = service.getMinFrequency();

        if(!this.assocUE.containsKey(frequency)){
            return false;
        }

        return this.assocUE.get(frequency).contains(ueId);
    }

    public boolean isInCoverageArea(double pos[]){
        return (Math.pow(pos[0] - this.position[0], 2) + Math.pow(pos[1] - this.position[1], 2) <= Math.pow(this.coverageRadio, 2));
    }

    /**
     * @return the assocUE
     */
    public HashMap<Double, ArrayList<String>> getAssocUE() {
        return this.assocUE;
    }

    /**
     * @param assocUE the assocUE to set
     */
    public void setAssocUE(HashMap<Double, ArrayList<String>> assocUE) {
        this.assocUE = assocUE;
    }

    /**
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public CellType getType() {
        return this.type;
    }

    /**
     * @return the position
     */
    public double[] getPosition() {
        return this.position;
    }

    /**
     * @param x
     * @param y
     */
    public void setPosition(double x, double y) {
        this.position = new double[2];
        this.position[0] = x;
        this.position[1] = y;
    }

    /**
     * @return the coverageRadio
     */
    public double getCoverageRadio() {
        return this.coverageRadio;
    }

    /**
     * @param coverageRadio the coverageRadio to set
     */
    public void setCoverageRadio(double coverageRadio) {
        this.coverageRadio = coverageRadio;
    }

    /**
     * @return the listReachableUE
     */
    public ArrayList<UserEquipment> getListReachableUE() {
        return this.listReachableUE;
    }

    /**
     * @return the mapLinkQuality
     */
    public HashMap<String, TransmissionLink> getMapLinkQuality() {
        return this.mapLinkQuality;
    }

    /**
     * @param ueId
     * @param txLink
     */
    public void setTransmisisonLink(String ueId, TransmissionLink txLink) {
        this.mapLinkQuality.put(ueId,txLink);
    }

    /**
     * @return the ueBlackList
     */
    public ArrayList<UserEquipment> getUEBlackList() {
        return this.ueBlackList;
    }

    /**
     * @return the txPower
     */
    public double getTxPower() {
        return this.txPower;
    }

    /**
     * @param txPower the txPower to set
     */
    public void setTxPower(double txPower) {
        this.txPower = txPower;
    }

    /**
     * @return the rxPower
     */
    public double getRxPower() {
        return this.rxPower;
    }

    /**
     * @param rxPower the rxPower to set
     */
    public void setRxPower(double rxPower) {
        this.rxPower = rxPower;
    }

    /**
     * @return the downlink capacity
     */
    public double getAvailDLCapacity(Double frequency) {
        return (this.availDLCapacityMap.get(frequency) != null) ? this.availDLCapacityMap.get(frequency) : 0.0;
    }

    /**
     * @param frequency of the service
     * @param capacity the available downlink capacity to set
     */
    public void setAvailDLCapacity(Double frequency, Double capacity) {
        if(this.availDLCapacityMap.containsKey(frequency)){
            this.availDLCapacityMap.remove(frequency);
        }

        this.availDLCapacityMap.put(frequency, capacity);
    }

    /**
     * @param frequency of the service
     * @param capacity the value to be decremented from downlink capacity
     */
    public void decrAvailDLCapacity(Double frequency, Double capacity) {
        double curCapacity = 0.0;
        if(this.availDLCapacityMap.containsKey(frequency)){
            curCapacity = this.availDLCapacityMap.remove(frequency);
        }

        this.availDLCapacityMap.put(frequency, curCapacity - capacity);
    }

    /**
     * @param frequency of the service
     * @param capacity the value to be incremented to downlink capacity
     */
    public void incrAvailDLCapacity(Double frequency, Double capacity) {
        double curCapacity = 0.0;
        if(this.availDLCapacityMap.containsKey(frequency)){
            curCapacity = this.availDLCapacityMap.remove(frequency);
        }

        this.availDLCapacityMap.put(frequency, curCapacity + capacity);
    }

    /**
     * @return the downlink capacity in PRBs
     */
    public double getTotalDLCapacity(Double frequency) {
        return (this.totalDLCapacityMap.get(frequency) != null) ? this.totalDLCapacityMap.get(frequency) : 0.0;
    }

    /**
     * @param capacity the downlink capacity to set
     */
    public void setTotalDLCapacity(Double frequency, Double capacity) {
        if(this.totalDLCapacityMap.containsKey(frequency)){
            this.totalDLCapacityMap.remove(frequency);
        }

        this.totalDLCapacityMap.put(frequency, capacity);
    }

    /**
     * @param frequency
     * @return
     */
    public double getDownLinkLoadFactor(Double frequency) {
        return (this.totalDLCapacityMap.get(frequency) != null) ? this.availDLCapacityMap.get(frequency)/this.totalDLCapacityMap.get(frequency) : 0.0;
    }

    /**
     * @return the uplink capacity
     */
    public double getAvailULCapacity(Double frequency) {
        return (this.availULCapacityMap.get(frequency) != null) ? this.availULCapacityMap.get(frequency) : 0.0;
    }

    /**
     * @param capacity the uplink capacity to set
     */
    public void setAvailULCapacity(Double frequency, Double capacity) {
        if(this.availULCapacityMap.containsKey(frequency)){
            this.availULCapacityMap.remove(frequency);
        }

        this.availULCapacityMap.put(frequency, capacity);
    }

    /**
     * @param capacity the uplink capacity to set
     */
    public void decrAvailULCapacity(Double frequency, Double capacity) {
        double curCapacity = 0.0;
        if(this.availULCapacityMap.containsKey(frequency)){
            curCapacity = this.availULCapacityMap.remove(frequency);
        }

        this.availULCapacityMap.put(frequency, curCapacity - capacity);
    }

    /**
     * @param frequency of the service
     * @param capacity the value to be incremented to uplink capacity
     */
    public void incrAvailULCapacity(Double frequency, Double capacity) {
        double curCapacity = 0.0;
        if(this.availULCapacityMap.containsKey(frequency)){
            curCapacity = this.availULCapacityMap.remove(frequency);
        }

        this.availULCapacityMap.put(frequency, curCapacity + capacity);
    }

    /**
     * @return the uplink capacity in PRBs
     */
    public double getTotalULCapacity(Double frequency) {
        return (this.totalULCapacityMap.get(frequency) != null) ? this.totalULCapacityMap.get(frequency) : 0.0;
    }

    /**
     * @param capacity the downlink capacity to set
     */
    public void setTotalULCapacity(Double frequency, Double capacity) {
        if(this.totalULCapacityMap.containsKey(frequency)){
            this.totalULCapacityMap.remove(frequency);
        }

        this.totalULCapacityMap.put(frequency, capacity);
    }

    /**
     * @param frequency
     * @return
     */
    public double getUpLinkLoadFactor(Double frequency) {
        return (this.totalULCapacityMap.get(frequency) != null) ? this.availULCapacityMap.get(frequency)/this.totalULCapacityMap.get(frequency) : 0.0;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return this.priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @param type the type to set
     */
    public void setType(CellType type) {
        this.type = type;
    }

    /**
     * @return the height
     */
    public double getNodeHeight() {
        return this.nodeHeight;
    }

    /**
     * @param nodeHeight the height to set
     */
    public void setNodeHeight(double nodeHeight) {
        this.nodeHeight = nodeHeight;
    }

    /**
     * @return the bandwidth
     */
    public double getBandwidth() {
        return this.bandwidth;
    }

    /**
     * @param bandwidth the bandwidth to set
     */
    public void setBandwidth(double bandwidth) {
        this.bandwidth = bandwidth;
    }

    /**
     * @return the node
     */
    public Node getNode() {
        return this.node;
    }

    /**
     * @param node the node to set
     */
    public void setNode(Node node) {
        this.node = node;
    }

    /**
     * @param frequency
     * @return predicted lower bound of unattended UE
     */
    public int getLowerBoundUnattendedUE(double frequency){
        // Key = load, Value = Number of UE the require this load
        SortedMap<Double, Integer> loadUEMap = new TreeMap<>(); 
        for(UserEquipment ue : this.listReachableUE){
            if(ue.getService().getMinFrequency() == frequency){
                double load = (double)TransmissionLink.getRequiredMinNumPRBs(ue, this);
                if(loadUEMap.containsKey(load)){
                    loadUEMap.put(load, loadUEMap.get(load)+1);
                }
                else{
                    loadUEMap.put(load, 1);
                }
            }
        }

        // Allocate the resources to the UEs that demand less load in order to
        // maximize the number of attended UEs.
        double availCapacity = this.getAvailDLCapacity(frequency);
        ArrayList<Double> lremLoad = new ArrayList<>();
        for(double load : loadUEMap.keySet()){
            int numUE = loadUEMap.get(load);
            int possibleNumUE = (int) Math.floor(availCapacity / load);
            if(possibleNumUE > 0){
                if(numUE <= possibleNumUE){
                    availCapacity -= numUE*load;
                    lremLoad.add(load);
                }
                else{
                    availCapacity -= possibleNumUE*load;
                    loadUEMap.put(load, numUE-possibleNumUE);
                    break;
                }
            }
        }

        for(double load : lremLoad){
            loadUEMap.remove(load);
        }

        // Add up the amount of UEs that may not be attended
        int totalUnattendedUE = 0;
        for(int numUE : loadUEMap.values()){
            totalUnattendedUE += numUE;
        }

        return totalUnattendedUE;
    }

    public void addRangeExpansionInfo(double frequency, RangeExpansionInfo info){
        this.rangeExpansionInfoMap.put(frequency, info);
    }

    public RangeExpansionInfo getRangeExpansionInfo(double frequency){
        return this.rangeExpansionInfoMap.get(frequency);
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();

        result.append(this.getClass().getName());
        result.append(" { ");
        result.append("id: " + this.id);
        result.append(", type: " + this.type.name());
        result.append(", position: (" + this.position[0] + ", " + this.position[1] + ")");
        result.append(", coverRadio: " + this.coverageRadio + ", ");
        result.append(", txPower: " + this.txPower);
        result.append(", rxPower: " + this.rxPower);
        result.append(", availDLCapacity: " + this.availDLCapacityMap);
        result.append(", totalDLCapacity: " + this.totalDLCapacityMap);
        result.append(", availULCapacity: " + this.availULCapacityMap);
        result.append(", totalULCapacity: " + this.totalULCapacityMap);
        result.append(", rangeExpansionInfoMap: " + this.rangeExpansionInfoMap);
        result.append(", assocUE: " + this.assocUE);
        result.append(" }");

        return result.toString();
    }

    @Override
    public int compareTo(Cell o) {
        return this.id.compareTo(o.getId());
    }
}
