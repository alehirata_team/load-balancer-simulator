package hetnet.cell;

public class RangeExpansionInfo {
    // User Association for Load Balancing in Heterogeneous Cellular Networks (Ye et al - 2013)
    private double lagrangeMultiplier;
    private double effectiveLoad;

    /**
     * @return the lagrangeMultiplier
     */
    public double getLagrangeMultiplier() {
        return lagrangeMultiplier;
    }

    /**
     * @param lagrangeMultiplier the lagrangeMultiplier to set
     */
    public void setLagrangeMultiplier(double lagrangeMultiplier) {
        this.lagrangeMultiplier = lagrangeMultiplier;
    }

    /**
     * @return the effectiveLoad
     */
    public double getEffectiveLoad() {
        return effectiveLoad;
    }

    /**
     * @param effectiveLoad the effectiveLoad to set
     */
    public void setEffectiveLoad(double effectiveLoad) {
        this.effectiveLoad = effectiveLoad;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();

        result.append(this.getClass().getName());
        result.append(" { ");
        result.append("lagrangeMultiplier: " + this.lagrangeMultiplier);
        result.append(", effectiveLoad: " + this.effectiveLoad);
        result.append(" }");

        return result.toString();
    }
}
