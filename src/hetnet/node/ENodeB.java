/**
 * 
 */
package hetnet.node;

import hetnet.cell.Cell;
import hetnet.cell.CellFactory;
import hetnet.cell.CellType;

import java.util.ArrayList;

import util.Properties;

/**
 * @author alexandre
 *
 */
public class ENodeB extends Node {

    public static final double DEFAULT_COVER_RADIO = 1000; // http://www.visionmobile.com/blog/2007/12/do-we-really-need-femto-cells/  // http://en.wikipedia.org/wiki/Femtocell
    public static final double DEFAULT_INTERSECT_DIST = 100;
    public static final double DEFAULT_TX_POWER = 49.0;
    public static final double DEFAULT_RX_POWER = 49.0;
    public static final double DEFAULT_BANDWIDTH = 20; // MHz
    public ArrayList<Cell> ListCell;

    /**
     * 
     */
    ENodeB(NodeType type) {
        super(type);
        this.setBandwidth(ENodeB.DEFAULT_BANDWIDTH);

        // TODO: Temporary default value
        int nSubFrames = Integer.valueOf(Properties.get("defaultTestInterval"));
        this.setTotalCapacity(1800.0, Node.getNumPRB(ENodeB.DEFAULT_BANDWIDTH, nSubFrames));

        this.ListCell = new ArrayList<Cell>();
    }

    /**
     * @return the listCell
     */
    public ArrayList<Cell> getListCell() {
        return this.ListCell;
    }

    /**
     * @param listCell the listCell to set
     */
    public void addCell(Cell cell) {
        this.ListCell.add(cell);
    }
    
    /**
     * @param center
     * @param r
     * @return Array with 3 center positions of the cells 
     */
    public static double[][] getCellPos(double[] center, double r){
        /**
         * http://www.wirelessbrasil.org/wirelessbr/colaboradores/marcio_rodrigues/tel_03.html
         *        _ B
         *    _ /   \
         * A/   \O_ /
         *  \ _ /   \
         *      \ _ /
         *          C
         *
         * A: (x - (Ox - r))^2 + (y - Oy)^2 = r^2 
         * B: (x - (Ox + r/2))^2 + (y - (Oy + 0.5*r*sqrt(3)))^2 = r^2
         * C: (x - (Ox + r/2))^2 + (y - (Oy - 0.5*r*sqrt(3)))^2 = r^2
         */          
        
        double[][] cellPos = new double[3][2];
        double x_delta = center[0] + r/2;
        double y_delta = center[1] + 0.5*r*Math.sqrt(3);
        
        // A
        cellPos[0][0] = center[0] - r;
        cellPos[0][1] = center[1];
        
        // B
        cellPos[1][0] = center[0] + x_delta;
        cellPos[1][1] = center[1] + y_delta;
        
        // C
        cellPos[2][0] = center[0] + x_delta;
        cellPos[2][1] = center[1] - y_delta;
        
        return cellPos;
    }
    
    public void createCells(boolean debugMode){
        /**
         * http://www.wirelessbrasil.org/wirelessbr/colaboradores/marcio_rodrigues/tel_03.html
         *        _ B
         *    _ /   \
         * A/   \O_ /
         *  \ _ /   \
         *      \ _ /
         *          C
         */

        double[][] cellPos = getCellPos(this.getPosition(), this.getCoverageRadio());
        
        // A
        this.addCell(CellFactory.createCell(
                CellType.MACRO, 
                String.format("C%07d_%s", CellFactory.getTotalCell(), this.getId()),
                cellPos[0][0],
                cellPos[0][1],
                this.getCoverageRadio(),
                this.getTxPower(),
                this.getRxPower(),
                this));

        // B
        this.addCell(CellFactory.createCell(
                CellType.MACRO, 
                String.format("C%07d_%s", CellFactory.getTotalCell(), this.getId()),
                cellPos[1][0],
                cellPos[1][1],
                this.getCoverageRadio(),
                this.getTxPower(),
                this.getRxPower(),
                this));

        // C
        this.addCell(CellFactory.createCell(
                CellType.MACRO, 
                String.format("C%07d_%s", CellFactory.getTotalCell(), this.getId()),
                cellPos[2][0],
                cellPos[2][1],
                this.getCoverageRadio(),
                this.getTxPower(),
                this.getRxPower(),
                this));

        // Debug - printing the cells
        if (debugMode) {
            for(Cell c : this.ListCell){
                System.out.println(c);
            }
        }
    }

    public boolean isInCoverageArea(double pos[]){
        for(Cell c : this.ListCell){
            if(c.isInCoverageArea(pos))
                return true;
        }

        return false;
    }
}
