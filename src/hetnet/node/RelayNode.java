/**
 * 
 */
package hetnet.node;

import hetnet.cell.Cell;
import hetnet.cell.CellFactory;
import hetnet.cell.CellType;


/**
 * @author alexandre
 *
 */
public class RelayNode extends Node {

    public static final double DEFAULT_COVER_RADIO = 300;
    public static final double DEFAULT_TX_POWER = 1000;
    public static final double DEFAULT_RX_POWER = 1000;
    public static final double DEFAULT_BANDWIDTH = 10;
    public Cell cell; 

    /**
     * 
     */
    RelayNode(NodeType type) {
        super(type);
        this.setBandwidth(RelayNode.DEFAULT_BANDWIDTH);
    }

    /**
     * @return the cell
     */
    public Cell getCell() {
        return this.cell;
    }

    /**
     * @param cell the cell to set
     */
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public void createCells(boolean debugMode){
        this.cell = CellFactory.createCell(
                CellType.PICO,
                String.format("C%07d_%s", CellFactory.getTotalCell(), this.getId()),
                this.getPosition()[0],
                this.getPosition()[1],
                this.getCoverageRadio(),
                this.getTxPower(),
                this.getRxPower(),
                this);

        // Debug - printing cell
        if (debugMode) {
            System.out.println(this.cell);
        }
    }
}
