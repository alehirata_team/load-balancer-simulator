/**
 * 
 */
package hetnet.node;

/**
 * @author alexandre
 *
 */
public enum NodeType {
    GENERIC,
    ENODEB,
    RELAY_NODE,
    PICO,
    FEMTO
}
