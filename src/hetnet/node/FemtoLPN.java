/**
 * 
 */
package hetnet.node;

import util.Properties;
import hetnet.cell.Cell;
import hetnet.cell.CellFactory;
import hetnet.cell.CellType;


/**
 * @author alexandre
 *
 */
public class FemtoLPN extends Node {
// AKA Home eNode B (HeNB)
    public static final double DEFAULT_COVER_RADIO = 10; // Dimitris Mavrakis (2007-12-01). "Do we really need femto cells?". VisionMobile. Retrieved 2012-07-26.
    public static final double DEFAULT_TX_POWER = 15.0;
    public static final double DEFAULT_RX_POWER = 15.0;
    public static final double DEFAULT_BANDWIDTH = 1.4; // 6 PRBs = 18Mbps
    public Cell cell; 

    /**
     * 
     */
    FemtoLPN(NodeType type) {
        super(type);
        this.setBandwidth(FemtoLPN.DEFAULT_BANDWIDTH);

        // TODO: Temporary default value
        int nSubFrames = Integer.valueOf(Properties.get("defaultTestInterval"));
        this.setTotalCapacity(1800.0, Node.getNumPRB(FemtoLPN.DEFAULT_BANDWIDTH, nSubFrames));
    }

    /**
     * @return the cell
     */
    public Cell getCell() {
        return this.cell;
    }

    /**
     * @param cell the cell to set
     */
    public void setCell(Cell cell) {
        this.cell = cell;
    }
    
    public void createCells(boolean debugMode){
        this.cell = CellFactory.createCell(
                CellType.FEMTO,
                String.format("C%07d_%s", CellFactory.getTotalCell(), this.getId()),
                this.getPosition()[0],
                this.getPosition()[1],
                this.getCoverageRadio(),
                this.getTxPower(),
                this.getRxPower(),
                this);

        // Debug - printing cell
        if (debugMode) {
            System.out.println(this.cell);
        }
    }

}
