/**
 * 
 */
package hetnet.node;

import hetnet.cell.Cell;
import hetnet.cell.CellFactory;
import hetnet.cell.CellType;
import util.Properties;


/**
 * @author alexandre
 *
 */
public class PicoLPN extends Node {

    public static final double DEFAULT_COVER_RADIO = 100; // http://www.visionmobile.com/blog/2007/12/do-we-really-need-femto-cells/
    public static final double DEFAULT_TX_POWER = 30.0;
    public static final double DEFAULT_RX_POWER = 30.0;
    public static final double DEFAULT_BANDWIDTH = 5;
    public Cell cell; 

    /**
     * 
     */
    PicoLPN(NodeType type) {
        super(type);
        this.setBandwidth(PicoLPN.DEFAULT_BANDWIDTH);

        // TODO: Temporary default value
        int nSubFrames = Integer.valueOf(Properties.get("defaultTestInterval"));
        this.setTotalCapacity(1800.0, Node.getNumPRB(PicoLPN.DEFAULT_BANDWIDTH, nSubFrames));
    }

    /**
     * @return the cell
     */
    public Cell getCell() {
        return this.cell;
    }

    /**
     * @param cell the cell to set
     */
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public void createCells(boolean debugMode){
        this.cell = CellFactory.createCell(
                CellType.PICO,
                String.format("C%07d_%s", CellFactory.getTotalCell(), this.getId()),
                this.getPosition()[0],
                this.getPosition()[1],
                this.getCoverageRadio(),
                this.getTxPower(),
                this.getRxPower(),
                this);

        // Debug - printing cell
        if (debugMode) {
            System.out.println(this.cell);
        }
    }
}
