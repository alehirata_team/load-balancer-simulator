package hetnet.node;

import hetnet.Service;
import hetnet.TransmissionLink;
import hetnet.userEquipment.UserEquipment;
import hetnet.userEquipment.UserEquipmentFactory;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author alexandre
 */
public class Node {

    private String id;
    private NodeType type;
    private double[] position;
    private double coverageRadio;
    private ArrayList<UserEquipment> listReachableUE;
    private HashMap<String, TransmissionLink> mapLinkQuality;
    private HashMap<Double, ArrayList<String>> assocUE;
    private ArrayList<UserEquipment> ueBlackList;
    private double txPower;
    private double rxPower;
    private HashMap<Double, Double> availCapacityMap; // PRBs
    private HashMap<Double, Double> totalCapacityMap;
    private double bandwidth; // MHz
    public static final double DEFAULT_SPECTRAL_EFFICIENCY = 15; // 15bps/Hz

    /**
     * height in meters
     */
    private double height;
    private int priority;

    /**
     * 
     */
    Node(NodeType type) {
        this.type = type;
        this.listReachableUE = new ArrayList<UserEquipment>();
        this.mapLinkQuality = new HashMap<String, TransmissionLink>();
        this.assocUE = new HashMap<Double, ArrayList<String>>();
        this.ueBlackList = new ArrayList<UserEquipment>();
        this.availCapacityMap = new HashMap<Double, Double>();
        this.totalCapacityMap = new HashMap<Double, Double>();
    }

    public boolean associateUE(String ueId){
        UserEquipment ue = UserEquipmentFactory.getHashUE().get(ueId);
        Service service = ue.getService();
        double frequency = service.getMinFrequency();

        if(!this.assocUE.containsKey(frequency)){
            this.assocUE.put(frequency, new ArrayList<String>());
        }

        this.assocUE.get(frequency).add(ueId);
        decrAvailCapacity(frequency, this.mapLinkQuality.get(ueId).getEffCell2UETxRate());

        return true;
    }

    public boolean isInCoverageArea(double pos[]){
        return (Math.pow(pos[0] - this.position[0], 2) + Math.pow(pos[1] - this.position[1], 2) <= Math.pow(this.coverageRadio, 2));
    }

    /**
     * @return the assocUE
     */
    public HashMap<Double, ArrayList<String>> getAssocUE() {
        return this.assocUE;
    }

    /**
     * @param assocUE the assocUE to set
     */
    public void setAssocUE(HashMap<Double, ArrayList<String>> assocUE) {
        this.assocUE = assocUE;
    }

    /**
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public NodeType getType() {
        return this.type;
    }

    /**
     * @return the position
     */
    public double[] getPosition() {
        return this.position;
    }

    /**
     * @param x
     * @param y
     */
    public void setPosition(double x, double y) {
        this.position = new double[2];
        this.position[0] = x;
        this.position[1] = y;
    }

    /**
     * @return the coverageRadio
     */
    public double getCoverageRadio() {
        return this.coverageRadio;
    }

    /**
     * @param coverageRadio the coverageRadio to set
     */
    public void setCoverageRadio(double coverageRadio) {
        this.coverageRadio = coverageRadio;
    }

    /**
     * @return the listReachableUE
     */
    public ArrayList<UserEquipment> getListReachableUE() {
        return this.listReachableUE;
    }

    /**
     * @return the mapLinkQuality
     */
    public HashMap<String, TransmissionLink> getMapLinkQuality() {
        return this.mapLinkQuality;
    }

    /**
     * @param ueId
     * @param txLink
     */
    public void setTransmisisonLink(String ueId, TransmissionLink txLink) {
        this.mapLinkQuality.put(ueId,txLink);
    }

    /**
     * @return the ueBlackList
     */
    public ArrayList<UserEquipment> getUEBlackList() {
        return this.ueBlackList;
    }

    /**
     * @return the txPower
     */
    public double getTxPower() {
        return this.txPower;
    }

    /**
     * @param txPower the txPower to set
     */
    public void setTxPower(double txPower) {
        this.txPower = txPower;
    }

    /**
     * @return the rxPower
     */
    public double getRxPower() {
        return this.rxPower;
    }

    /**
     * @param rxPower the rxPower to set
     */
    public void setRxPower(double rxPower) {
        this.rxPower = rxPower;
    }

    /**
     * @return the capacity
     */
    public double getAvailCapacity(Double frequency) {
        return this.availCapacityMap.get(frequency);
    }

    /**
     * @param capacity the capacity to set
     */
    public void setAvailCapacity(Double frequency, Double capacity) {
        if(this.availCapacityMap.containsKey(frequency)){
            this.availCapacityMap.remove(frequency);
        }

        this.availCapacityMap.put(frequency, capacity);
    }

    /**
     * @param capacity the capacity to set
     */
    public void decrAvailCapacity(Double frequency, Double capacity) {
        double curCapacity = 0.0;
        if(this.availCapacityMap.containsKey(frequency)){
            curCapacity = this.availCapacityMap.remove(frequency);
        }

        this.availCapacityMap.put(frequency, curCapacity - capacity);
    }

    /**
     * @return the capacity
     */
    public double getTotalCapacity(Double frequency) {
        return this.totalCapacityMap.get(frequency);
    }

    /**
     * @param capacity the capacity to set
     */
    public void setTotalCapacity(Double frequency, Double capacity) {
        if(this.totalCapacityMap.containsKey(frequency)){
            this.totalCapacityMap.remove(frequency);
        }

        this.totalCapacityMap.put(frequency, capacity);
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return this.priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @param type the type to set
     */
    public void setType(NodeType type) {
        this.type = type;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * @return the bandwidth in MHz
     */
    public double getBandwidth() {
        return this.bandwidth;
    }

    /**
     * @param bandwidth the bandwidth in MHz to set
     */
    public void setBandwidth(double bandwidth) {
        this.bandwidth = bandwidth;
    }

    public static double getNumPRB(double bandwidth, int nsubframes){
        /* 
    In Release 8 the only supported bandwidths are 1.4 MHz, 3 MHz, 5 MHz, 10 MHz, 15, MHz, 20 MHz. The number of "Usable" Resource Blocks allowed in each of the bands is the following:

    1.4 MHz  - Usable PRBs = 6
    3  MHz  - Usable PRBs = 15
    5  MHz  - Usable PRBs = 25
    10  MHz  - Usable PRBs = 50
    15  MHz  - Usable PRBs = 75
    20  MHz  - Usable PRBs = 100

    All the remaining SubCarriers are reserved for Guard SubCarriers and cannot by used for any transmissions either in the Uplink or the Downlink. Hope that helps ...

    Vishal

    http://lteuniversity.com/ask_the_expert/f/69/t/2592.aspx
         */
        double numPRB = 0.0;

        if(bandwidth == 1.4){
            numPRB = 6;
        } else if(bandwidth == 3){
            numPRB = 15;
        } else if(bandwidth == 5){
            numPRB = 25;
        } else if(bandwidth == 10){
            numPRB = 50;
        } else if(bandwidth == 15){
            numPRB = 75;
        } else if(bandwidth == 20){
            numPRB = 100;
        }

        return numPRB*nsubframes;
    }

    /**
     * @param txRate in kbps
     * @return
     */
    public static int getNeededPRB(double txRate, int nSubFrames){
        /*
        LTE: 15bps/Hz => 20MHz = 300Mbps = 100 PRBs => 1 PRB = 3Mbps
        1 frame = 10 ms => 1 s = 100 frames 

        1 PRB = 3 Mb/s ~= 30 kb/10ms => 1 PRB ~= 30 kb/frame ~= 1kb/subframe 

        dataRate[kb] = txRate[kbps]/1000ms = neededDataRate[kb/nSubFrame]/nSubFrames
        neededDataRate = resp = nSubFrames * txRate / 1000
        */

        return (int) Math.ceil(nSubFrames * txRate / 1000);
    }

    /**
     * @param bandwidth in MHz
     * @param specEff bps/Hz
     * @param nSubFrames number of 1ms intervals
     * @return number of PRBs available in this timeFrame
     */
  /*  public static double calcCapacity(double bandwidth, double specEff, int nSubFrames){
        /*
        LTE: 15bps/Hz => 20MHz = 300Mbps = 100 PRBs => 1 PRB = 3Mbps
        1 frame = 10 ms => 1 s = 100 frames 

        1 PRB = 3 Mb/s ~= 30 kb/10ms => 1 PRB ~= 30 kb/frame 

        (20 MHz * 15 Mbps/MHz / 100 PRB = 3 Mb/s ) * 10ms / 1000
        */
      /*  return (bandwidth * specEff / Node.getNumPRB(bandwidth, nSubFrames)) * nSubFrames/1000;
    }*/

    /**
     * This must be implemented by its childs
     */
    public void createCells(boolean debugMode){
        System.err.println("ERROR: createCells called by cellId: " + this.id);
        return;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();

        result.append(this.getClass().getName());
        result.append(" { ");
        result.append("id: " + this.id);
        result.append(", type: " + this.type.name());
        result.append(", position: (" + this.position[0] + ", " + this.position[1] + ")");
        result.append(", coverRadio: " + this.coverageRadio + ", ");
        result.append(", txPower: " + this.txPower);
        result.append(", rxPower: " + this.rxPower);
        result.append(", availCapacity: " + this.availCapacityMap);
        result.append(", totalCapacity: " + this.totalCapacityMap);
        result.append(", assocUE: " + this.assocUE);
        result.append(" }");

        return result.toString();
    }
}