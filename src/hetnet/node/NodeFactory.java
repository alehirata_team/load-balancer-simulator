package hetnet.node;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import au.com.bytecode.opencsv.CSVReader;

/**
 * @author alexandre
 *
 */
public abstract class NodeFactory {

    private static int totalNode;
    private static int totalENodeB;
    private static int totalRelayNode;
    private static int totalPicoLPN;
    private static int totalFemtoLPN;
    private static HashMap<String,Node> hashNode;

    public static enum NodeCSVCol{
        Type,
        Id,
        PosX,
        PosY,
        CoverRadio,
        TxPower,
        RxPower,
        Bandwidth
    }

    /**
     * @throws IOException 
     * 
     */
    public static void initNodeFactory(String nodeFile, boolean debugMode) throws IOException{
        totalNode = 0;
        totalENodeB = 0;
        totalPicoLPN = 0;
        totalFemtoLPN = 0;
        hashNode = new HashMap<String,Node>();

        CSVReader reader = new CSVReader(new FileReader(nodeFile), ',', '"', 1);
        String [] col;

        while ((col = reader.readNext()) != null) {
            NodeType type = NodeType.valueOf(col[NodeCSVCol.Type.ordinal()]);
            String id = col[NodeCSVCol.Id.ordinal()];
            double posX = Double.valueOf(col[NodeCSVCol.PosX.ordinal()]);
            double posY = Double.valueOf(col[NodeCSVCol.PosY.ordinal()]);
            double coverRadio = Double.valueOf(col[NodeCSVCol.CoverRadio.ordinal()]);
            double txPower = Double.valueOf(col[NodeCSVCol.TxPower.ordinal()]);
            double rxPower = Double.valueOf(col[NodeCSVCol.RxPower.ordinal()]);
            HashMap<Double, Double> totalCapacity = new HashMap<Double, Double>();

            Node node = NodeFactory.createNode(type, id, posX, posY, coverRadio, -1, totalCapacity);
            node.setTxPower(txPower);
            node.setRxPower(rxPower);
            node.createCells(debugMode);
            
            if (debugMode) {
                // System.out.print("\n"); printCSVLine(col); // Debug
                System.out.println(node); // Debug
            }
        }

        reader.close();
    }

    /**
     * @param col
     */
    /*private static void printCSVLine(String[] col){
        for(int i=0; i<NodeCSVCol.values().length; i++){
            System.out.println(NodeCSVCol.values()[i].toString() + ": " + col[i]);
        }
    }*/

    /**
     * @return the hashNode
     */
    public static HashMap<String, Node> getHashNode() {
        return hashNode;
    }

    /**
     * @return
     */
    public static int getTotalNode(){
        return totalNode;
    }

    /**
     * @return
     */
    public static int getTotalENodeB(){
        return totalENodeB;
    }

    /**
     * @return the totalRelayNode
     */
    public static int getTotalRelayNode() {
        return totalRelayNode;
    }

    /**
     * @return
     */
    public static int getTotalPicoLPN(){
        return totalPicoLPN;
    }

    /**
     * @return
     */
    public static int getTotalFemtoLPN(){
        return totalFemtoLPN;
    }

    public static Node createNode(NodeType type, String id, double posX, double posY, double coverRadio, double bandwidth, HashMap<Double, Double> totalCapacity){
        Node node = createNode(type);
        node.setId(id);
        node.setPosition(posX, posY);
        node.setCoverageRadio(coverRadio);

        if(bandwidth > 0){
            node.setBandwidth(bandwidth);
        }

        hashNode.put(id, node);

        return node;
    }

    /**
     * @param type
     * @return
     */
    private static Node createNode(NodeType type){
        Node node;
        totalNode++;

        switch(type){
            case ENODEB:
                totalENodeB++;
                node = new ENodeB(type);
                break;

            case RELAY_NODE:
                totalRelayNode++;
                node = new RelayNode(type);
                break;

            case PICO:
                totalPicoLPN++;
                node = new PicoLPN(type);
                break;

            case FEMTO:
                totalFemtoLPN++;
                node = new FemtoLPN(type);
                break;

            case GENERIC:
            default:
                node = new Node(type);
                break;
        }

        return node;
    }

    public static void printHashNode(){
        for(Node n : hashNode.values()){
            n.toString();
        }
    }

    public static double getMinLoad(double frequency){
        double minLoad = 0.0;
        for(Node n : hashNode.values())
            minLoad = Math.min(minLoad, (1.0 - n.getAvailCapacity(frequency) / n.getTotalCapacity(frequency)));

        return minLoad;
    }

    public static double getMaxLoad(double frequency){
        double maxLoad = 0.0;
        for(Node n : hashNode.values())
            maxLoad = Math.max(maxLoad, (1.0 - n.getAvailCapacity(frequency) / n.getTotalCapacity(frequency)));

        return maxLoad;
    }

    public static double getAvgLoad(double frequency){
        double totalLoad = 0.0;
        for(Node n : hashNode.values())
            totalLoad += (1.0 - n.getAvailCapacity(frequency) / n.getTotalCapacity(frequency));

        return totalLoad / hashNode.values().size();
    }
}
