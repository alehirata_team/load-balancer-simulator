package hetnet;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import au.com.bytecode.opencsv.CSVReader;
/**
 * 
 */

/**
 * @author alexandre
 *
 */
public class Service {

    private String name;
    private Type type;
    private double minTxRate;
    private double maxTxRate;
    private double minRxRate;
    private double maxRxRate;
    private double minFrequency;
    private double maxFrequency;
    private int priority;

    public static enum Type{
        VoIP,
        HDVideoCall,
        FullHDVideoStreaming
    }

    public static enum ServiceCSVCol{
        Name,
        MinTxRate,
        MaxTxRate,
        MinRxRate,
        MaxRxRate,
        MinFrequency,
        MaxFrequency,
        Priority
    }

    /**
     * 
     */
    public Service(
            String name,
            double minTxRate,
            double maxTxRate,
            double minRxRate,
            double maxRxRate,
            double minFrequency,
            double maxFrequency) {
        this.name = name;
        if (name.equals(Type.HDVideoCall.name())){
            this.type = Type.HDVideoCall;
        } else if (name.equals(Type.FullHDVideoStreaming.name())){
            this.type = Type.FullHDVideoStreaming;
        } else {
            this.type = Type.VoIP;
        }
        this.minTxRate = minTxRate;
        this.maxTxRate = maxTxRate;
        this.minRxRate = minRxRate;
        this.maxRxRate = maxRxRate;
        this.minFrequency = minFrequency;
        this.maxFrequency = maxFrequency;
    }


    public static HashMap<String, Service> createHashService(String serviceFile, boolean debugMode) throws NumberFormatException, IOException{
        HashMap<String, Service> hashService = new HashMap<String, Service>();

        CSVReader reader = new CSVReader(new FileReader(serviceFile), ',', '"', 1);
        String [] col;

        while ((col = reader.readNext()) != null) {
            String name = col[ServiceCSVCol.Name.ordinal()];
            double minTxRate = Double.valueOf(col[ServiceCSVCol.MinTxRate.ordinal()]);
            double maxTxRate = Double.valueOf(col[ServiceCSVCol.MaxTxRate.ordinal()]);
            double minRxRate = Double.valueOf(col[ServiceCSVCol.MinRxRate.ordinal()]);
            double maxRxRate = Double.valueOf(col[ServiceCSVCol.MaxRxRate.ordinal()]);
            double minFrequency = Double.valueOf(col[ServiceCSVCol.MinFrequency.ordinal()]);
            double maxFrequency = Double.valueOf(col[ServiceCSVCol.MaxFrequency.ordinal()]);

            Service s = new Service(name, minTxRate, maxTxRate, minRxRate, maxRxRate, minFrequency, maxFrequency);

            hashService.put(name, s);

            if (debugMode) {
                // System.out.print("\n"); printCSVLine(col); // Debug
                System.out.println(s);// Debug
            }
        }

        reader.close();

        return hashService;
    }

    /**
     * @param col
     */
    /*private static void printCSVLine(String[] col){
        for(int i=0; i<ServiceCSVCol.values().length; i++){
            System.out.println(ServiceCSVCol.values()[i].toString() + ": " + col[i]);
        }
    }*/

    /**
     * @return the name
     */
    public Type getType() {
        return this.type;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the minTxRate
     */
    public double getMinTxRate() {
        return this.minTxRate;
    }

    /**
     * @param minTxRate the minTxRate to set
     */
    public void setMinTxRate(double minTxRate) {
        this.minTxRate = minTxRate;
    }

    /**
     * @return the maxTxRate
     */
    public double getMaxTxRate() {
        return this.maxTxRate;
    }

    /**
     * @param maxTxRate the maxTxRate to set
     */
    public void setMaxTxRate(double maxTxRate) {
        this.maxTxRate = maxTxRate;
    }

    /**
     * @return the minRxRate
     */
    public double getMinRxRate() {
        return this.minRxRate;
    }

    /**
     * @param minRxRate the minRxRate to set
     */
    public void setMinRxRate(double minRxRate) {
        this.minRxRate = minRxRate;
    }

    /**
     * @return the maxRxRate
     */
    public double getMaxRxRate() {
        return this.maxRxRate;
    }

    /**
     * @param maxRxRate the maxRxRate to set
     */
    public void setMaxRxRate(double maxRxRate) {
        this.maxRxRate = maxRxRate;
    }

    /**
     * @return the minFrequency
     */
    public double getMinFrequency() {
        return this.minFrequency;
    }

    /**
     * @param minFrequency the minFrequencyRate to set
     */
    public void setMinFrequency(double minFrequency) {
        this.minFrequency = minFrequency;
    }

    /**
     * @return the maxFrequency
     */
    public double getMaxFrequency() {
        return this.maxFrequency;
    }

    /**
     * @param maxFrequency the maxFrequencyRate to set
     */
    public void setMaxFrequency(double maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return this.priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();

        result.append(this.getClass().getName());
        result.append(" { ");
        result.append("name: " + this.name);
        result.append(", Tx: (min = " + this.minTxRate + ", max = " + this.maxTxRate + ")");
        result.append(", Rx: (min = " + this.minRxRate + ", max = " + this.maxRxRate + ")");
        result.append(", Frequency: (min = " + this.minFrequency + ", max = " + this.maxFrequency + ")");
        result.append(" }");

        return result.toString();
    }

}
