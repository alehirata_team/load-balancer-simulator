package hetnet;


public class AssociationNode implements Comparable<AssociationNode> {
    private String ueName;
    private String cellName;
    private double val;
    
    public AssociationNode(String ueName, String cellName, double val) {
        this.ueName = ueName;
        this.cellName = cellName;
        this.val = val;
    }

    @Override
    public int compareTo(AssociationNode o) {
        int ret = 0;
        if (this.val != o.val) {
            ret = (this.val > o.val) ? 1 : -1;
        }
        return ret;
    }

    /**
     * @return the ueName
     */
    public String getUeName() {
        return ueName;
    }

    /**
     * @param ueName the ueName to set
     */
    public void setUeName(String ueName) {
        this.ueName = ueName;
    }

    /**
     * @return the cellName
     */
    public String getCellName() {
        return cellName;
    }

    /**
     * @param cellName the cellName to set
     */
    public void setCellName(String cellName) {
        this.cellName = cellName;
    }

    /**
     * @return the val
     */
    public double getVal() {
        return val;
    }

    /**
     * @param val the val to set
     */
    public void setVal(double val) {
        this.val = val;
    }    
}
