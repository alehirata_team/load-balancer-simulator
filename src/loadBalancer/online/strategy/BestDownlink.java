package loadBalancer.online.strategy;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;

import loadBalancer.online.solver.OnlineEvent;
import loadBalancer.online.solver.OnlineEventType;

public class BestDownlink extends OnlineStrategy{

    public boolean processJoiningEvent(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell,
            OnlineEvent evt, HashMap<String,HashSet<String>> assocMap){
        if(evt.getType() != OnlineEventType.JOINING){
            return false;
        }

        UserEquipment ue = hashUE.get(evt.getUeId());
        if(ue.getListReachableCells().size() > 0){
            // Sorting cells by DownLink in descending order
            SortedMap<Double, Cell> dlMap = new TreeMap<>(Collections.reverseOrder());
            for(Cell cell : ue.getListReachableCells()){
                dlMap.put(ue.getTransmisisonLink(cell.getId()).getEffCell2UETxRate(), cell);
            }

            String assocCellId = null;
            for(double dl : dlMap.keySet()){
                if(dlMap.get(dl).associateUE(ue.getId())){
                    assocCellId = dlMap.get(dl).getId(); 
                    break;
                }
            }

            // Create an association between Cell and UE
            if(assocCellId != null){
                if(!assocMap.containsKey(assocCellId)){
                    HashSet<String> s = new HashSet<String>();
                    s.add(ue.getId());
                    assocMap.put(assocCellId, s);
                } else {
                    assocMap.get(assocCellId).add(ue.getId());
                }
                return true;
            }
        }

        return false;
    }
}
