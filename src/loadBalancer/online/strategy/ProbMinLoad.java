package loadBalancer.online.strategy;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.HashMap;
import java.util.HashSet;

import loadBalancer.online.solver.OnlineEvent;
import loadBalancer.online.solver.OnlineEventType;

public class ProbMinLoad extends OnlineStrategy{

    public boolean processJoiningEvent(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell,
            OnlineEvent evt, HashMap<String,HashSet<String>> assocMap){
        if(evt.getType() != OnlineEventType.JOINING){
            return false;
        }

        UserEquipment ue = hashUE.get(evt.getUeId());
        if(ue.getListReachableCells().size() > 0){
            // Associate UE Minimum Load Cell Probabilistically
            Cell c = associateMinLoadCell(ue);

            // Fill in the association map
            if(c != null){
                if(!assocMap.containsKey(c.getId())){
                    HashSet<String> s = new HashSet<String>();
                    s.add(ue.getId());
                    assocMap.put(c.getId(), s);
                } else {
                    assocMap.get(c.getId()).add(ue.getId());
                }
                return true;
            }
            else{
                System.out.println("WARNING: Could not associate UserEquipment: " + ue);
            }
        }

        return false;
    }

    private Cell associateMinLoadCell(UserEquipment ue){
        HashMap<Cell, Double> cellList = new HashMap<Cell, Double>();
        Cell cell = null;
        double totalPercLoad = 0.0;

        for(Cell c : ue.getListReachableCells()){
            double percLoad = getPercLoad(c, ue);
            cellList.put(c, percLoad);
            totalPercLoad += percLoad;
        }

        while(!cellList.isEmpty()){
            cell = chooseMinLoadCell(cellList, totalPercLoad);
            if(cell != null){
                totalPercLoad -= cellList.get(cell);
                cellList.remove(cell);
                if(cell.associateUE(ue.getId())){
                    System.out.println("DEBUG: MinLoadCell - ue(" + ue.getId() + ")" + " cell(" + (cell != null ? cell.getId() : null) + ")");
                    return cell;    
                }
            }
            else{
                System.err.println("ERROR: associateMinLoadCell - cell is NULL");
            }
        }

        return null;
    }

    private double getPercLoad(Cell c, UserEquipment ue){
        return c.getAvailDLCapacity(ue.getService().getMinFrequency()) / c.getTotalDLCapacity(ue.getService().getMinFrequency());
    }

    private Cell chooseMinLoadCell(HashMap<Cell, Double> cellList, double totalPercLoad){
        double chosen = Math.random() * totalPercLoad;
        double range = 0.0;
        Cell cell = null;

        for(Cell c : cellList.keySet()){
            double step = cellList.get(c);
            range += step;
            if(chosen <= range){
                cell = c;
                break;
            }
        }

        if(cellList.size() > 0 && cell == null){
            System.err.println("ERROR: Failure on choseMinLoadCell!");
            System.exit(0);
        }

        return cell;
    }
}
