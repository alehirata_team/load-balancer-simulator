package loadBalancer.online.strategy;

import hetnet.TransmissionLink;
import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;

import loadBalancer.online.solver.OnlineEvent;
import loadBalancer.online.solver.OnlineEventType;
import util.Properties;

/**
 * SINR Biasing (Range Expansion)
 * [2] Q. Ye, B. Rong, Y. Chen, M. Al-Shalash, C. Caramanis, and J. Andrews,
 * "User Association for Load Balancing in Heterogeneous Cellular Networks,"
 * Wireless Communications, IEEE Transactions on, vol. 12, no. 6, pp. 2706-2716,
 * June 2013.
 */
public class RangeExpansion extends OnlineStrategy{

    private double macroCellBias;
    private double picoCellBias;
    private double femtoCellBias;

    public RangeExpansion(){
        super();
        this.macroCellBias = Double.valueOf(Properties.get("sinrBiasMacroCell"));
        this.picoCellBias = Double.valueOf(Properties.get("sinrBiasPicoCell"));
        this.femtoCellBias = Double.valueOf(Properties.get("sinrBiasFemtoCell"));
        System.out.println("RANGE EXPANSION (macro=" + this.macroCellBias + ", pico=" + this.picoCellBias + ", femto=" + this.femtoCellBias + ")");
    }

    public boolean processJoiningEvent(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell,
            OnlineEvent evt, HashMap<String,HashSet<String>> assocMap){
        if(evt.getType() != OnlineEventType.JOINING){
            return false;
        }

        UserEquipment ue = hashUE.get(evt.getUeId());
        if(ue.getListReachableCells().size() > 0){
            // Sorting cells by SINR in descending order
            SortedMap<Double, Cell> sinrMap = new TreeMap<>(Collections.reverseOrder());
            for(Cell cell : ue.getListReachableCells()){
                sinrMap.put(getSINRBias(ue, cell), cell);
            }

            String assocCellId = null;
            for(double sinr : sinrMap.keySet()){
                Cell cell = sinrMap.get(sinr);
                if(cell.associateUE(ue.getId())){
                    assocCellId = cell.getId(); 
                    break;
                }
            }

            // Create an association between Cell and UE
            if(assocCellId != null){
                if(!assocMap.containsKey(assocCellId)){
                    HashSet<String> s = new HashSet<String>();
                    s.add(ue.getId());
                    assocMap.put(assocCellId, s);
                } else {
                    assocMap.get(assocCellId).add(ue.getId());
                }
                return true;
            }
        }

        return false;
    }

    // User algorithm
    private double getSINRBias(UserEquipment ue, Cell cell){
        double bias = 0.0;

        switch(cell.getType()){
        case MACRO:
            bias = this.macroCellBias;
            break;
        case PICO:
            bias = this.picoCellBias;
            break;
        case FEMTO:
            bias = this.femtoCellBias;
            break;
        default:
        }

        return bias*TransmissionLink.getSINR(ue, cell);
    }
}
