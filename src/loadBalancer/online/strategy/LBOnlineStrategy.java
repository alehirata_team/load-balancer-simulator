/**
 * 
 */
package loadBalancer.online.strategy;

/**
 * @author alexandre
 *
 */

/**
 * Leave Linear Programming Strategies as the last ones in the enum because they
 * need the results from the other strategies to perform correctly.
 */
public enum LBOnlineStrategy {
    BEST_DOWNLINK,
//    BEST_POWER,
    BEST_SINR,
    PICO_CELL_FIRST,
    PROB_MIN_LOAD,
    RANGE_EXTENSION,
//    LP_GREEDY,
//    ILP_SIMPLE_MODEL,
//    LP_DL_AND_UL,
//    LP_RES_ALLOC
}
