package loadBalancer.online.strategy;

import hetnet.TransmissionLink;
import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;

import loadBalancer.online.solver.OnlineEvent;
import loadBalancer.online.solver.OnlineEventType;

public class BestSINR extends OnlineStrategy{
    public boolean processJoiningEvent(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell,
            OnlineEvent evt, HashMap<String,HashSet<String>> assocMap){
        if(evt.getType() != OnlineEventType.JOINING){
            return false;
        }

        UserEquipment ue = hashUE.get(evt.getUeId());
        if(ue.getListReachableCells().size() > 0){
            // Sorting cells by SINR in ascending order
            SortedMap<Double, Cell> sinrMap = new TreeMap<>();
            for(Cell cell : ue.getListReachableCells()){
                sinrMap.put(TransmissionLink.getSINR(ue, cell), cell);
            }

            String assocCellId = null;
            for(double sinr : sinrMap.keySet()){
                if(sinrMap.get(sinr).associateUE(ue.getId())){
                    assocCellId = sinrMap.get(sinr).getId(); 
                    break;
                }
            }

            // Create an association between Cell and UE
            if(assocCellId != null){
                if(!assocMap.containsKey(assocCellId)){
                    HashSet<String> s = new HashSet<String>();
                    s.add(ue.getId());
                    assocMap.put(assocCellId, s);
                } else {
                    assocMap.get(assocCellId).add(ue.getId());
                }
                return true;
            }
        }

        return false;
    }
}
