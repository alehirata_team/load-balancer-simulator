package loadBalancer.online.strategy;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;

import loadBalancer.online.solver.OnlineEvent;
import loadBalancer.online.solver.OnlineEventType;
import loadBalancer.online.solver.OnlineStatistics;

public abstract class OnlineStrategy {
    
    private int iteration;
    
    private double SAMPLE_TIMEFRAME_SECONDS = 30.0;

    public OnlineStrategy() {
        iteration = 1;
    }
    
    public int getIteration(){
        return iteration;
    }
    
    public void incrementIteration(){
        iteration++;
    }

    public HashMap<String,HashSet<String>> solve(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell, SortedMap<Double, OnlineEvent> eventList) {
        HashMap<String,HashSet<String>> assocMap = new HashMap<String,HashSet<String>>();
        for(Double t : eventList.keySet()){
            OnlineEvent evt = eventList.get(t);
            while(evt.getTimestamp() > iteration * SAMPLE_TIMEFRAME_SECONDS){
                OnlineStatistics.printIterationStats(getIteration(), iteration * SAMPLE_TIMEFRAME_SECONDS);
                incrementIteration();
            }
            processEvent(hashUE, hashCell, evt, assocMap);
        }
        
        // Print the statics after the last event was processed
        OnlineStatistics.printIterationStats(getIteration(), iteration * SAMPLE_TIMEFRAME_SECONDS);
        incrementIteration();

        return assocMap;
    }

    public boolean processEvent(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell,
            OnlineEvent evt, HashMap<String,HashSet<String>> assocMap){
        System.out.println("\n---------------------------------------------------\n\nProcessing: " + evt);
        switch(evt.getType()){
        case JOINING:
            if(processJoiningEvent(hashUE, hashCell, evt, assocMap)){
                // Statistics for successfull association
                UserEquipment ue = hashUE.get(evt.getUeId());
                Cell cell = ue.getAssociatedCell();
                OnlineStatistics.joinUE(cell.getType(), ue.getService().getType());
            }
            else{
                  UserEquipment ue = hashUE.get(evt.getUeId());
                  if(ue == null)
                  {
                      System.err.println("ERROR: UE " + evt.getUeId() + " not found in UE hashmap.");
                      break;
                  }
                  if(ue.getListReachableCells().size() == 0){
                      System.out.println("Unreachable UE: " + evt.getUeId());
                      OnlineStatistics.incrementTotalUnreachableUEs();
                  }
                  else{
                      System.out.println("Not attended UE: " + evt.getUeId());
                      OnlineStatistics.incrementTotalNotAttendedReachableUEs(ue.getService().getType());
                  }
            }
            break;
        case LEAVING:
            if(!processLeavingEvent(hashUE, hashCell, evt, assocMap)){
                System.out.println("WARNING: UE ( " + evt.getUeId() + ") is not being attended by any cell");
            }
            break;
        default:
            System.err.println("ERROR: Unsupported event: " + evt.toString());
        }
        
        System.err.println("");
        return false;
        
    }
    
    public abstract boolean processJoiningEvent(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell,
            OnlineEvent evt, HashMap<String,HashSet<String>> assocMap);
    
    public boolean processLeavingEvent(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell,
            OnlineEvent evt, HashMap<String,HashSet<String>> assocMap){
        if(evt.getType() != OnlineEventType.LEAVING){
            return false;
        }
        
        try{
            UserEquipment ue = hashUE.get(evt.getUeId());
            Cell cell = ue.getAssociatedCell();
            if (cell != null)         {
                cell.disassociateUE(ue.getId());
                OnlineStatistics.leaveUE(cell.getType(), ue.getService().getType());
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    
        return false;
    }
    
}
