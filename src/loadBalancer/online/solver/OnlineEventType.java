package loadBalancer.online.solver;

public enum OnlineEventType {
    JOINING,
    LEAVING
}
