package loadBalancer.online.solver;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class OnlineEvent {

    public static enum EVTCSVCol{
        Timestamp,
        UE,
        EventType
    }

    private double timestamp;

    private String ueId;

    private OnlineEventType type;

    /**
     * @return the timestamp
     */
    public double getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the ueId
     */
    public String getUeId() {
        return ueId;
    }

    /**
     * @param ueId the ueId to set
     */
    public void setUeId(String ueId) {
        this.ueId = ueId;
    }

    /**
     * @return the type
     */
    public OnlineEventType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(OnlineEventType type) {
        this.type = type;
    }

    public OnlineEvent(double timestamp, String ueId, OnlineEventType type) {
        this.timestamp = timestamp;
        this.ueId = ueId;
        this.type = type;
    }

    public static void genCSV(String output, SortedMap<Double, OnlineEvent> eventList){
        CSVWriter writer;
        try {
            writer = new CSVWriter(new FileWriter(output), ',');
            String[] header = {EVTCSVCol.Timestamp.name(),
                    EVTCSVCol.UE.name(),
                    EVTCSVCol.EventType.name()};

            writer.writeNext(header);
            for(double t : eventList.keySet()){
                // feed in your array (or convert your data to an array)
                OnlineEvent e = eventList.get(t);
                String l = e.getTimestamp() + "," 
                        + e.getUeId() + "," 
                        + e.getType().name();

                writer.writeNext(l.split(","));
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SortedMap<Double, OnlineEvent> genEventListFromCSV(String eventFile){
        SortedMap<Double, OnlineEvent> eventList = new TreeMap<>();

        CSVReader reader;
        try {
            reader = new CSVReader(new FileReader(eventFile), ',', '"', 1);

            String [] col;

            while ((col = reader.readNext()) != null) {
                OnlineEvent e = new OnlineEvent(
                        Double.valueOf(col[EVTCSVCol.Timestamp.ordinal()]),
                        col[EVTCSVCol.UE.ordinal()],
                        OnlineEventType.valueOf(col[EVTCSVCol.EventType.ordinal()]));
                eventList.put(e.getTimestamp(), e);
            }

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return eventList;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder("OnlineEvent: [");
        sb.append(this.timestamp).append(", ");
        sb.append(this.type).append(", ");
        sb.append(this.ueId);
        sb.append("]");
        return sb.toString();
    }
}
