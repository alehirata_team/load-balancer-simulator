package loadBalancer.online.solver;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;

import loadBalancer.offline.strategy.LBStrategy;
import loadBalancer.offline.strategy.LPStrategy;
import loadBalancer.online.strategy.BestDownlink;
import loadBalancer.online.strategy.BestSINR;
import loadBalancer.online.strategy.LBOnlineStrategy;
import loadBalancer.online.strategy.OnlineStrategy;
import loadBalancer.online.strategy.PicoCellFirst;
import loadBalancer.online.strategy.ProbMinLoad;
import loadBalancer.online.strategy.RangeExpansion;
//www.devmedia.com.br/manipulando-arquivos-xml-em-java/3245#ixzz2u9oKkkfP
import loadBalancer.offline.strategy.ILPSimpleModel;
import loadBalancer.offline.strategy.LPGreedy;

/**
 * @author alexandre
 */
public abstract class OnlineLoadBalancer {

    public static HashMap<String,HashSet<String>> balance(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell, 
            SortedMap<Double, OnlineEvent> eventList, LBOnlineStrategy strategy){
        HashMap<String,HashSet<String>> association = null;
        OnlineStrategy st = null;

        switch(strategy){
        case BEST_DOWNLINK:
            st = new BestDownlink();
            break;

        case BEST_SINR:
            st = new BestSINR();
            break;

        case PICO_CELL_FIRST:
            st = new PicoCellFirst();
            break;

        case PROB_MIN_LOAD:
            st = new ProbMinLoad();
            break;

        case RANGE_EXTENSION:
            st = new RangeExpansion();
            break;

        default:
            return null;
        }

        association = st.solve(hashUE, hashCell, eventList);

        return association;
    }

    /**
     * @param hashUE
     * @param hashCell
     * @param strategy
     * @param qosFactor
     * @param inputTest
     * @return
     */
    public static HashMap<String,HashSet<String>> balanceLP(HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, LBStrategy strategy, double qosFactor, String inputTest){
        LPStrategy st = null;

        switch(strategy){
        case ILP_SIMPLE_MODEL:
            st = new ILPSimpleModel();
            break;

        case LP_GREEDY:
            st = new LPGreedy();
            break;

        case BEST_DOWNLINK:
        case BEST_SINR:
        case PICO_CELL_FIRST:
            /*case LP_DL_AND_UL:
            case LP_RES_ALLOC:*/
        default:
            return null;
        }

        return st.solveLP(hashUE, hashCell, strategy, qosFactor, inputTest);
    }
}