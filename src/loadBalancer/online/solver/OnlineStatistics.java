package loadBalancer.online.solver;

import hetnet.Service;
import hetnet.cell.CellFactory;
import hetnet.cell.CellType;
import hetnet.userEquipment.UserEquipmentFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class OnlineStatistics {

    private static AtomicInteger totalUneacheableUEs;
    private static AtomicInteger totalUEs;
    private static AtomicInteger maxSimultUEs;
    private static HashMap<Service.Type, AtomicInteger> totalNotAttendedReacheableUEsPerService;
    private static HashMap<CellType, AtomicInteger> totalUEsPerCell;
    private static HashMap<Service.Type, AtomicInteger> totalUEsPerService;
    private static HashMap<CellType, AtomicInteger> simultUEsPerCell;
    private static HashMap<CellType, AtomicInteger> maxSimultUEsPerCell;
    private static HashMap<Service.Type, AtomicInteger> simultUEsPerService;
    private static HashMap<Service.Type, AtomicInteger> maxSimultUEsPerService;
    private static int uePerMin; 

    private static void loadInfoFile(String filename) throws IOException{
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(filename));
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.contains("UEs per minute")){
                    String valStr = line.replaceAll("[^\\d]", "");
                    uePerMin = Integer.valueOf(valStr);
                }
            }
        } catch (IOException x) {
            System.err.println(x);
        } finally {
            if (reader != null) {
                reader.close();    
            }
        }
    }

    public static void init(String infofile) throws IOException {
        totalUneacheableUEs = new AtomicInteger(0);
        totalUEs = new AtomicInteger(0);
        maxSimultUEs = new AtomicInteger(0); 
        totalNotAttendedReacheableUEsPerService = new HashMap<>();
        totalUEsPerCell = new HashMap<>();
        totalUEsPerService = new HashMap<>();
        simultUEsPerCell = new HashMap<>();
        maxSimultUEsPerCell = new HashMap<>();
        simultUEsPerService = new HashMap<>();
        maxSimultUEsPerService = new HashMap<>();

        for (CellType cellType : CellType.values())
        {
            totalUEsPerCell.put(cellType, new AtomicInteger(0));
            simultUEsPerCell.put(cellType, new AtomicInteger(0));
            maxSimultUEsPerCell.put(cellType, new AtomicInteger(0));
        }

        for (Service.Type serviceType : Service.Type.values())
        {
            totalNotAttendedReacheableUEsPerService.put(serviceType, new AtomicInteger(0));
            totalUEsPerService.put(serviceType, new AtomicInteger(0));
            simultUEsPerService.put(serviceType, new AtomicInteger(0));
            maxSimultUEsPerService.put(serviceType, new AtomicInteger(0));
        }

        loadInfoFile(infofile);
    }

    public static void joinUE(CellType cellType, Service.Type serviceType) {
        int curSimultUEsPerCell = simultUEsPerCell.get(cellType).incrementAndGet();
        maxSimultUEsPerCell.get(cellType).compareAndSet(curSimultUEsPerCell - 1, curSimultUEsPerCell);
        int curSimultUEsPerService = simultUEsPerService.get(serviceType).incrementAndGet();
        maxSimultUEsPerService.get(serviceType).compareAndSet(curSimultUEsPerService - 1, curSimultUEsPerService);
        int curSimultUEs = getTotalSimultAttendedUEs();
        maxSimultUEs.compareAndSet(curSimultUEs - 1, curSimultUEs);
        totalUEsPerCell.get(cellType).incrementAndGet();
        totalUEsPerService.get(serviceType).incrementAndGet();
        totalUEs.incrementAndGet();
    }

    public static void leaveUE(CellType cellType, Service.Type serviceType) {
        simultUEsPerCell.get(cellType).decrementAndGet();
        simultUEsPerService.get(serviceType).decrementAndGet();
    }

    public static void incrementTotalNotAttendedReachableUEs(Service.Type serviceType) {
        totalNotAttendedReacheableUEsPerService.get(serviceType).incrementAndGet();
        totalUEs.incrementAndGet();
    }

    public static int getNotAttendedUEs(Service.Type serviceType) {
        return totalNotAttendedReacheableUEsPerService.get(serviceType).get();
    }

    public static void incrementTotalUnreachableUEs() {
        totalUneacheableUEs.incrementAndGet();
        totalUEs.incrementAndGet();
    }

    public static int getTotalUnreachableUEs() {
        return totalUneacheableUEs.get();
    }

    public static int getTotalNotAttendedUEs() {
        int count = 0;
        for (AtomicInteger i : totalNotAttendedReacheableUEsPerService.values()){
            count += i.get();
        }
        return count;
    }

    public static int getTotalAttendedUEs(Service.Type serviceType) {
        return totalUEsPerService.get(serviceType).get();
    }

    public static int getTotalAttendedUEs(CellType cellType) {
        return totalUEsPerCell.get(cellType).get();
    }

    public static int getTotalAttendedUEs() {
        int count = 0;
        for (AtomicInteger i : totalUEsPerCell.values()){
            count += i.get();
        }
        return count;
    }

    public static int getSimultAttendedUEs(Service.Type serviceType) {
        return simultUEsPerService.get(serviceType).get();
    }

    public static int getSimultAttendedUEs(CellType cellType) {
        return simultUEsPerCell.get(cellType).get();
    }

    public static int getTotalSimultAttendedUEs() {
        int count = 0;
        for (AtomicInteger i : simultUEsPerCell.values()){
            count += i.get();
        }
        return count;
    }

    public static void printIterationStats(int iter, double timestamp) {
        int val = 0;
        String iterStr = String.format("\n{ITER_%06d} ", iter);
        StringBuilder sb = new StringBuilder("### Statistics ###\n");
        double frequency = UserEquipmentFactory.getHashUE().values().iterator().next().getService().getMinFrequency();
        int totalEmptyCells = CellFactory.getTotalEmptyCells();
        int totalZeroLoadCells = CellFactory.getTotalZeroLoadCells(frequency, null);
        NumberFormat percFormat = NumberFormat.getPercentInstance();
        percFormat.setMinimumFractionDigits(3);

        // Timestamp
        sb.append(iterStr).append(String.format("Timestamp: %.3f", timestamp));

        // UE per minute
        sb.append(iterStr).append(String.format("UEs per minute: %d", uePerMin));

        // UEs Statistics
        int totalUE = totalUEs.get();
        sb.append(iterStr).append("Total UEs: ").append(totalUE);
        int simultAttendedUsers = 0;
        val = 0;
        for (Service.Type serviceType : simultUEsPerService.keySet()){
            val = simultUEsPerService.get(serviceType).get();
            sb.append(iterStr).append("Simult Attended UEs (").append(serviceType.name()).append("): ").append(val);
            val = maxSimultUEsPerService.get(serviceType).get();
            sb.append(iterStr).append("Max Simult Attended UEs (").append(serviceType.name()).append("): ").append(val);
        }
        val = 0;
        for (CellType cellType : simultUEsPerCell.keySet()){
            val = simultUEsPerCell.get(cellType).get();
            simultAttendedUsers += val;
            sb.append(iterStr).append("Simult Attended UEs (").append(cellType.name()).append("): ").append(val);
            val = maxSimultUEsPerCell.get(cellType).get();
            sb.append(iterStr).append("Max Simult Attended UEs (").append(cellType.name()).append("): ").append(val);
        }
        sb.append(iterStr).append("Simult Attended UEs: ").append(simultAttendedUsers);
        sb.append(iterStr).append("Max Simult Attended UEs: ").append(maxSimultUEs.get());

        int totalAttendedUsers = 0;
        val = 0;
        for (Service.Type serviceType : totalUEsPerService.keySet()){
            val = totalUEsPerService.get(serviceType).get();
            sb.append(iterStr).append("Total Attended UEs (").append(serviceType.name()).append("): ").append(val);
        }
        val = 0;
        for (CellType cellType : totalUEsPerCell.keySet()){
            val = totalUEsPerCell.get(cellType).get();
            sb.append(iterStr).append("Total Attended UEs (").append(cellType.name()).append("): ").append(val);
            totalAttendedUsers += val;
        }
        sb.append(iterStr).append("Total Attended UEs: ").append(totalAttendedUsers);
        sb.append(iterStr).append("Total Attended UE Percentage: ").append((double)totalAttendedUsers/(double)totalUE);

        int totalUnattendedUsers = 0;
        val = 0;
        for (Service.Type serviceType : totalNotAttendedReacheableUEsPerService.keySet()){
            val = totalNotAttendedReacheableUEsPerService.get(serviceType).get();
            sb.append(iterStr).append("Unattended UEs (").append(serviceType.name()).append("): ").append(val);
            totalUnattendedUsers += val;
        }
        sb.append(iterStr).append("Total Unattended Reachable UEs: ").append(totalUnattendedUsers)
        .append(" (").append(percFormat.format((double)totalUnattendedUsers/(double)totalUE)).append(")");

        int totalUnreachableUsers = getTotalUnreachableUEs();
        sb.append(iterStr).append("Total Unreachable UEs: ").append(totalUnreachableUsers)
        .append(" (").append(percFormat.format((double)totalUnreachableUsers/(double)totalUE)).append(")");


        // Cells Statistics
        double maxDLload = CellFactory.getMaxDownLinkLoadFactor(frequency, null);
        double minDLload = CellFactory.getMinDownLinkLoadFactor(frequency, null);
        //System.out.println("QoS Factor: " + qosFactor);

        sb.append(iterStr).append("Total Cells: " + CellFactory.getHashCell().size());
        sb.append(iterStr).append("Total Empty Cells (no reachable UEs): ").append(CellFactory.getTotalEmptyCells())
        .append(" (").append(percFormat.format((double)totalEmptyCells/(double)CellFactory.getTotalCell())).append(")");
        sb.append(iterStr).append("Total Non-Empty and Zero-Load Cells: ").append(totalZeroLoadCells)
        .append(" (").append(percFormat.format((double)totalZeroLoadCells/(double)CellFactory.getTotalCell())).append(")");

        // Load Balance Statistics
        sb.append(iterStr).append(String.format("Max Downlink Load Factor: %.3f", maxDLload));
        sb.append(iterStr).append(String.format("Min Downlink Load Factor: %.3f", minDLload));
        sb.append(iterStr).append(String.format("Load Balance Solution: %.3f", maxDLload - minDLload));
        sb.append(iterStr).append("Load Mean: ").append(percFormat.format(CellFactory.getLoadMean(frequency, null)));
        sb.append(iterStr).append("Load Median: ").append(percFormat.format(CellFactory.getLoadMedian(frequency, null)));
        sb.append(iterStr).append("Load Variance: ").append(CellFactory.getLoadVariance(frequency, null));
        sb.append(iterStr).append("Load Std Deviation: ").append(CellFactory.getLoadStdDeviation(frequency, null));

        for(CellType ctype : CellType.values()) {
            if(ctype != CellType.GENERIC){
                double maxDLload_cell = CellFactory.getMaxDownLinkLoadFactor(frequency, ctype);
                double minDLload_cell = CellFactory.getMinDownLinkLoadFactor(frequency, ctype);
                sb.append(iterStr).append("Load Mean (").append(ctype.name()).append("): ").append(percFormat.format(CellFactory.getLoadMean(frequency, ctype)));
                sb.append(iterStr).append("Load Median (").append(ctype.name()).append("): ").append(percFormat.format(CellFactory.getLoadMedian(frequency, ctype)));
                sb.append(iterStr).append("Load Variance (").append(ctype.name()).append("): ").append(CellFactory.getLoadVariance(frequency, ctype));
                sb.append(iterStr).append("Load Std Deviation (").append(ctype.name()).append("): ").append(CellFactory.getLoadStdDeviation(frequency, ctype));
                sb.append(iterStr).append(String.format("Max Downlink Load Factor (%s): %.3f", ctype.name(), maxDLload_cell));
                sb.append(iterStr).append(String.format("Min Downlink Load Factor (%s): %.3f", ctype.name(), minDLload_cell));
                sb.append(iterStr).append(String.format("Load Balance Solution (%s): %.3f", ctype.name(), maxDLload_cell - minDLload_cell));
            }
        }
        sb.append("\n\n");
        System.out.println(sb.toString());
    }
}

