package loadBalancer.offline.solver;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.HashMap;
import java.util.HashSet;

import loadBalancer.offline.strategy.BestDownlink;
import loadBalancer.offline.strategy.BestSINR;
import loadBalancer.offline.strategy.ILPSimpleModel;
import loadBalancer.offline.strategy.LBStrategy;
import loadBalancer.offline.strategy.LPGreedy;
import loadBalancer.offline.strategy.LPStrategy;
import loadBalancer.offline.strategy.PicoCellFirst;
import loadBalancer.offline.strategy.ProbMinLoad;
import loadBalancer.offline.strategy.RangeExpansion;
import loadBalancer.offline.strategy.RateBias;
import loadBalancer.offline.strategy.Strategy;

/**
 * @author alexandre
 */
public abstract class LoadBalancer {

    public static HashMap<String,HashSet<String>> balance(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell, LBStrategy strategy){
        HashMap<String,HashSet<String>> association = null;
        Strategy st = null;

        switch(strategy){
        case BEST_DOWNLINK:
            st = new BestDownlink();
            break;

        case BEST_SINR:
            st = new BestSINR();
            break;

        case PICO_CELL_FIRST:
            st = new PicoCellFirst();
            break;

        case RANGE_EXPANSION:
            st = new RangeExpansion();
            break;

        case RATE_BIAS:
            st = new RateBias();
            break;

        case PROB_MIN_LOAD:
            st = new ProbMinLoad();
            break;

        case ILP_SIMPLE_MODEL:
        case LP_GREEDY:
            /*case BEST_POWER:
            case RANGE_EXTENSION:*/
        default:
            return null;
        }

        association = st.solve(hashUE, hashCell);

        return association;
    }

    /**
     * @param hashUE
     * @param hashCell
     * @param strategy
     * @param qosFactor
     * @param inputTest
     * @return
     */
    public static HashMap<String,HashSet<String>> balanceLP(HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, LBStrategy strategy, double qosFactor, String inputTest){
        LPStrategy st = null;

        switch(strategy){
        case ILP_SIMPLE_MODEL:
            st = new ILPSimpleModel();
            break;

        case LP_GREEDY:
            st = new LPGreedy();
            break;

        case BEST_DOWNLINK:
        case BEST_SINR:
        case PICO_CELL_FIRST:
            /*case LP_DL_AND_UL:
            case LP_RES_ALLOC:*/
        default:
            return null;
        }

        return st.solveLP(hashUE, hashCell, strategy, qosFactor, inputTest);
    }
}