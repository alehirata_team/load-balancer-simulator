package loadBalancer.offline.solver;

import hetnet.TransmissionLink;
import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;

import loadBalancer.offline.strategy.LBStrategy;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.GlpkException;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.SWIGTYPE_p_int;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;

public class GlpkHelper {
    /**
     * @param glpkModel
     * @param output
     * @param solution
     */
    public static void callGLPK(String glpkModel, String output, String solution){
        System.out.println("\nCalling GLPK\n");
        Process process;
        try {
            //  glpsol --cpxlp model.lp -o glpk.out --write glpk.sol
            process = new ProcessBuilder("glpsol","--cpxlp", glpkModel, "-o", output, "--write", solution).start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getGlpkConstraint(String rowName, HashMap<String, Double> row){
        String output = rowName + ": ";
        for(String valName : row.keySet()){
            double coef = row.get(valName);
            output += ((coef > 0.0) ? "+ " : "- ") + Math.abs(coef) + " " + valName + " ";
        }
        return output;
    }


    /**
     * @param glpkModel
     * @param solution
     * @return
     */
    public static HashMap<String,HashSet<String>> parseGLPKSolution(HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, String glpkModel, String solution){
        HashMap<String,HashSet<String>> association = null;
        // TODO: Implement it

        return association;
    }

    /**
     * Sample taken from libglpk-java example
     */
    public static void runGLPKSample() {
        glp_prob lp;
        glp_smcp parm;
        SWIGTYPE_p_int ind;
        SWIGTYPE_p_double val;
        int ret;

        try {
            System.out.println( "GLPK ver." + GLPK.glp_version());
            System.out.println("\n\nRunning GLPK Sample\n");
            System.out.println(
                    "Minimize z = (x1-x2) /2 + (1-(x1-x2)) = -.5 * x1 + .5 * x2 + 1\n\n" +
                            "subject to\n" +
                            "0.0 <= x1 - x2 <= 0.2\n" +
                            "0.0 <= x1 <= 0.5\n" +
                    "0.0 <= x2 <= 0.5\n");

            // Create problem
            lp = GLPK.glp_create_prob();
            System.out.println("Problem created");
            GLPK.glp_set_prob_name(lp, "myProblem");

            // Define columns
            GLPK.glp_add_cols(lp, 2);
            GLPK.glp_set_col_name(lp, 1, "x1");
            GLPK.glp_set_col_kind(lp, 1, GLPKConstants.GLP_CV);
            GLPK.glp_set_col_bnds(lp, 1, GLPKConstants.GLP_DB, 0, .5);
            GLPK.glp_set_col_name(lp, 2, "x2");
            GLPK.glp_set_col_kind(lp, 2, GLPKConstants.GLP_CV);
            GLPK.glp_set_col_bnds(lp, 2, GLPKConstants.GLP_DB, 0, .5);

            // Create constraints
            GLPK.glp_add_rows(lp, 1);

            GLPK.glp_set_row_name(lp, 1, "c1");
            GLPK.glp_set_row_bnds(lp, 1, GLPKConstants.GLP_DB, 0, 0.2);
            ind = GLPK.new_intArray(3);
            GLPK.intArray_setitem(ind, 1, 1);
            GLPK.intArray_setitem(ind, 2, 2);
            val = GLPK.new_doubleArray(3);
            GLPK.doubleArray_setitem(val, 1, 1.);
            GLPK.doubleArray_setitem(val, 2, -1.);
            GLPK.glp_set_mat_row(lp, 1, 2, ind, val);

            // Define objective
            GLPK.glp_set_obj_name(lp, "z");
            GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MIN);
            GLPK.glp_set_obj_coef(lp, 0, 1.);
            GLPK.glp_set_obj_coef(lp, 1, -.5);
            GLPK.glp_set_obj_coef(lp, 2, .5);

            // Solve model
            parm = new glp_smcp();
            GLPK.glp_init_smcp(parm);
            ret = GLPK.glp_simplex(lp, parm);

            // Retrieve solution
            if (ret == 0) {
                write_lp_solution(lp);
            } else {
                System.out.println("The problem could not be solved");
            }

            // Free memory
            GLPK.glp_delete_prob(lp);
        } catch (GlpkException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * write simplex solution
     * @param lp problem
     */
    static void write_lp_solution(glp_prob lp) {
        int i;
        int n;
        String name;
        double val;

        name = GLPK.glp_get_obj_name(lp);
        val = GLPK.glp_get_obj_val(lp);
        System.out.print(name);
        System.out.print(" = ");
        System.out.println(val);
        n = GLPK.glp_get_num_cols(lp);
        for (i = 1; i <= n; i++) {
            name = GLPK.glp_get_col_name(lp, i);
            val = GLPK.glp_get_col_prim(lp, i);
            System.out.print(name);
            System.out.print(" = ");
            System.out.println(val);
        }
    }

    /**
     * LP model without timeframe allocation 
     */
    public static void solveILPModel(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell, double minLoadPercentage) {
        glp_prob lp;
        glp_smcp parm;
        SWIGTYPE_p_int ind;
        SWIGTYPE_p_double val;
        int ret;
        int totalNewConstraints = 0;
        int totalUe_Cell = 0;
        int totalCell_Ue = 0;
        String rowName = "";

        System.out.println( "GLPK ver." + GLPK.glp_version());
        System.out.println("\n\nRunning GLPK - LP model without timeframe allocation\n");
        System.out.println("Minimize z = Lmax - Lmin\n\n" +
                "subject to\n" +
                "0.0 <= cb*Lmax - sum(m in Mb, lmb*xmb), for all b in B\n" +
                "0.0 <= sum_{m in Mb}(lmb*xmb) - cb*Lmin, for all b in B\n" +
                "0.0 <= sum_{b in B}(xmb) <= 1, for all m in Mb\n" +
                "0.0 <= sum_{m in Mb}(lmb*xmb) <= cb, for all b in B\n" +
                "alpha*|Mb| <= sum_{b in B}(sum_{m in Mb}(xmb)), for all b in B\n" +
                "xmb in {0,1}\n");


        //////////////////////////////////////////////////////////////////
        // Create problem                                               //
        //////////////////////////////////////////////////////////////////

        lp = GLPK.glp_create_prob();
        System.out.println("Problem created");
        GLPK.glp_set_prob_name(lp, "lbProblem");


        //////////////////////////////////////////////////////////////////
        // Define columns                                               //
        //////////////////////////////////////////////////////////////////

        int totalCols = 2; // Lmin and Lmax

        for(Cell c : hashCell.values()){
            totalCols += c.getListReachableUE().size();
        }

        GLPK.glp_add_cols(lp, totalCols);

        final int LmaxCol = 1;
        String LmaxColName = "Lmax";
        System.out.println("DEBUG: colName(" + LmaxCol + ") = " + LmaxColName);
        GLPK.glp_set_col_name(lp, LmaxCol, LmaxColName);
        GLPK.glp_set_col_kind(lp, LmaxCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LmaxCol, GLPKConstants.GLP_DB, 0, 1.0);

        final int LminCol = 2;
        String LminColName = "Lmin";
        System.out.println("DEBUG: colName(" + LminCol + ") = " + LminColName);
        GLPK.glp_set_col_name(lp, LminCol, LminColName);
        GLPK.glp_set_col_kind(lp, LminCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LminCol, GLPKConstants.GLP_DB, 0, 1.0);

        HashMap<String, Integer> colMap = new HashMap<String, Integer>();
        int colId = 3;

        for(Cell c : hashCell.values()){
            for(UserEquipment ue : c.getListReachableUE()){
                String colName = "x_" + c.getId() + "_" + ue.getId();
                System.out.println("DEBUG: colName(" + colId + ") = " + colName);
                GLPK.glp_set_col_name(lp, colId, colName);
                GLPK.glp_set_col_kind(lp, colId, GLPKConstants.GLP_BV);
                colMap.put(c.getId() + " " + ue.getId(), colId);
                colId++;
            }
        }


        int maxCol = colId - 1;
        System.out.println("DEBUG: max colId = " + maxCol);

        // Getting the frequency
        UserEquipment ms = (UserEquipment)hashUE.values().toArray()[0];
        double frequency = ms.getService().getMinFrequency();


        //////////////////////////////////////////////////////////////////
        // Create constraints                                           //
        //////////////////////////////////////////////////////////////////

        int rowIdx = 1;
        int dbg_counter = 0;

        for(Cell c : hashCell.values()){
            if(c.getTotalDLCapacity(frequency) > 0 && c.getListReachableUE().size() != 0)
                totalUe_Cell++;
        }

        for(UserEquipment ue : hashUE.values()){
            if(ue.getListReachableCells().size() != 0){
                totalCell_Ue++;
            }
        }

        System.err.println("DEBUG: totalUe_Cell = " + totalUe_Cell + "\nDEBUG: totalCell_Ue = " + totalCell_Ue);

        System.err.print("\n\n");
        for(int i=1; i<=colId-1; i++)
            System.err.format("   %5.1f", (double)i);
        System.err.print("\n");

        //////////////////////////////////////////////////////////////////
        // 0.0 <= cb*Lmax - sum(m in Mb, lmb*xmb), for all b in B       //
        //////////////////////////////////////////////////////////////////
        {
            //System.err.println("\n\nDEBUG: C1: 0.0 <= cb*Lmax - sum(m in Mb, lmb*xmb), for all b in B");
            dbg_counter = 0;
            HashMap<String, HashMap<Integer, Double>> constraintsMap = new HashMap<String, HashMap<Integer, Double>>();

            //GLPK.glp_add_rows(lp, totalNewConstraints);

            for(Cell c : hashCell.values()){
                if(c.getTotalDLCapacity(frequency) > 0 && c.getListReachableUE().size() > 0){
                    HashMap<Integer, Double> row = new HashMap<Integer, Double>();
                    rowName = "Lmax_" + c.getId();

                    for(UserEquipment ue : c.getListReachableUE()){
                        // TODO: review lmb calculations
                        double lmb = ue.getTransmisisonLink(c.getId()).getCell2UETxRate();
                        if(lmb != 0){
                            row.put(colMap.get(c.getId() + " " + ue.getId()), -lmb);
                            //System.err.println("DEBUG: " + rowName + " - colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()) + "\t lmb = " + lmb);
                        }
                    }

                    if(row.size() > 0){
                        row.put(LmaxCol, c.getTotalDLCapacity(frequency));
                        constraintsMap.put(rowName, row);
                    }
                }
            }

            if(!constraintsMap.isEmpty()){
                GLPK.glp_add_rows(lp, constraintsMap.size());
                for(String rName : constraintsMap.keySet()){
                    dbg_counter++;
                    HashMap<Integer, Double> row = constraintsMap.get(rName);
                    //printMatrixRow(row, maxCol);
                    int len = row.size();
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    int j=1;
                    for(int idx : row.keySet()){
                        GLPK.intArray_setitem(ind, j, idx);
                        GLPK.doubleArray_setitem(val, j, row.get(idx));
                        System.err.println("DEBUG: " + rName + " - j = " + j + "; ind = " + idx + "; val = " + row.get(idx) + "; len = " + len + "; rowIdx = " + rowIdx);
                        j++;
                    }
                    GLPK.glp_set_row_name(lp, rowIdx, rName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);
                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
            //System.err.println("DEBUG: C1 = Adding " + dbg_counter + " new constraint(s).");

            //constraintsMap.clear();

        }

        //////////////////////////////////////////////////////////////////
        // 0.0 <= sum_{m in Mb}(lmb*xmb) - cb*Lmin, for all b in B      //
        //////////////////////////////////////////////////////////////////
        {
            //System.err.println("\n\nDEBUG: C2: 0.0 <= sum_{m in Mb}(lmb*xmb) - cb*Lmin, for all b in B");
            dbg_counter = 0;
            HashMap<String, HashMap<Integer, Double>> constraintsMap = new HashMap<String, HashMap<Integer, Double>>();

            for(Cell c : hashCell.values()){
                if(c.getListReachableUE().size() > 0){
                    HashMap<Integer, Double> row = new HashMap<Integer, Double>();
                    rowName = "Lmin_" + c.getId();

                    for(UserEquipment ue : c.getListReachableUE()){
                        // TODO: review lmb calculations
                        double lmb = ue.getTransmisisonLink(c.getId()).getCell2UETxRate();
                        if(lmb != 0){
                            row.put(colMap.get(c.getId() + " " + ue.getId()), lmb);
                            //System.err.println("DEBUG: " + rowName + " - colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()) + "\t lmb = " + lmb);
                        }
                    }

                    if(row.size() > 0){
                        row.put(LminCol, -c.getTotalDLCapacity(frequency));
                        constraintsMap.put(rowName, row);
                    }
                }
            }

            if(!constraintsMap.isEmpty()){
                GLPK.glp_add_rows(lp, constraintsMap.size());
                for(String rName : constraintsMap.keySet()){
                    dbg_counter++;
                    HashMap<Integer, Double> row = constraintsMap.get(rName);
                    printMatrixRow(row, maxCol);
                    int len = row.size();
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    int j=1;
                    for(int idx : row.keySet()){
                        GLPK.intArray_setitem(ind, j, idx);
                        GLPK.doubleArray_setitem(val, j, row.get(idx));
                        System.err.println("DEBUG: " + rName + " - j = " + j + "; ind = " + idx + "; val = " + row.get(idx));
                        j++;
                    }
                    GLPK.glp_set_row_name(lp, rowIdx, rName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);
                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
            //System.err.println("DEBUG: C2 = Adding " + dbg_counter + " new constraint(s).");
            constraintsMap.clear();
        }

        //////////////////////////////////////////////////////////////////
        // 0.0 <= sum_{b in B}(xmb) <= 1, for all m in Mb               //
        //////////////////////////////////////////////////////////////////
        {
            dbg_counter = 0;
            HashMap<String, HashMap<Integer, Double>> constraintsMap = new HashMap<String, HashMap<Integer, Double>>();
            //System.err.println("\n\nDEBUG: C3 = Adding " + totalNewConstraints + " new row(s).");
            for(UserEquipment ue : hashUE.values()){
                if(ue.getListReachableCells().size() > 0){
                    HashMap<Integer, Double> row = new HashMap<Integer, Double>();
                    rowName = "assoc_" + ue.getId();

                    for(Cell c : ue.getListReachableCells()){
                        row.put(colMap.get(c.getId() + " " + ue.getId()), 1.0);
                    }

                    if(row.size() > 0){
                        constraintsMap.put(rowName, row);
                    }
                }
            }

            if(!constraintsMap.isEmpty()){
                GLPK.glp_add_rows(lp, constraintsMap.size());
                for(String rName : constraintsMap.keySet()){
                    dbg_counter++;
                    HashMap<Integer, Double> row = constraintsMap.get(rName);
                    printMatrixRow(row, maxCol);
                    int len = row.size();
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    int j=1;
                    for(int idx : row.keySet()){
                        GLPK.intArray_setitem(ind, j, idx);
                        GLPK.doubleArray_setitem(val, j, row.get(idx));
                        System.err.println("DEBUG: " + rName + " - j = " + j + "; ind = " + idx + "; val = " + row.get(idx));
                        j++;
                    }
                    GLPK.glp_set_row_name(lp, rowIdx, rName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_DB, 0, 1.0);
                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        //System.err.println("DEBUG: C3 = Adding " + dbg_counter + " new constraint(s).");


        //////////////////////////////////////////////////////////////////
        // 0.0 <= sum_{m in Mb}(lmb*xmb) <= cb, for all b in B          //
        //////////////////////////////////////////////////////////////////
        {
            //System.err.println("\n\nDEBUG: C4: 0.0 <= sum_{m in Mb}(lmb*xmb) <= cb, for all b in B");
            dbg_counter = 0;
            HashMap<String, HashMap<Integer, Double>> constraintsMap = new HashMap<String, HashMap<Integer, Double>>();
            HashMap<String, Double> boundMap = new HashMap<String, Double>();

            for(Cell c : hashCell.values()){
                if(c.getListReachableUE().size() > 0){
                    HashMap<Integer, Double> row = new HashMap<Integer, Double>();
                    rowName = "Capacity_" + c.getId();

                    for(UserEquipment ue : c.getListReachableUE()){
                        // TODO: review lmb calculations
                        double lmb = ue.getTransmisisonLink(c.getId()).getCell2UETxRate();
                        if(lmb != 0){
                            row.put(colMap.get(c.getId() + " " + ue.getId()), lmb);
                            //System.err.println("DEBUG: " + rowName + " - colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()) + "\t lmb = " + lmb);
                        }
                    }

                    if(row.size() > 0){
                        boundMap.put(rowName, c.getTotalDLCapacity(frequency));
                        constraintsMap.put(rowName, row);
                    }
                }
            }

            if(!constraintsMap.isEmpty()){
                GLPK.glp_add_rows(lp, constraintsMap.size());
                for(String rName : constraintsMap.keySet()){
                    dbg_counter++;
                    HashMap<Integer, Double> row = constraintsMap.get(rName);
                    printMatrixRow(row, maxCol);
                    int len = row.size();
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    int j=1;
                    for(int idx : row.keySet()){
                        GLPK.intArray_setitem(ind, j, idx);
                        GLPK.doubleArray_setitem(val, j, row.get(idx));
                        System.err.println("DEBUG: " + rName + " - j = " + j + "; ind = " + idx + "; val = " + row.get(idx));
                        j++;
                    }
                    GLPK.glp_set_row_name(lp, rowIdx, rName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_DB, 0.0, boundMap.get(rName));
                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
            //System.err.println("DEBUG: C4 = Adding " + dbg_counter + " new constraint(s).");
            constraintsMap.clear();
        }

        //System.err.println("DEBUG: C4 = Adding " + dbg_counter + " new constraint(s).");


        //////////////////////////////////////////////////////////////////
        // alpha*|Mb| <= sum_{b in B}(sum_{m in Mb}(xmb))               //
        //////////////////////////////////////////////////////////////////

        double alpha = minLoadPercentage;
        totalNewConstraints = 1;
        dbg_counter = 0;
        System.err.println("\n\nDEBUG: C5 = Adding " + totalNewConstraints + " new row(s).");
        GLPK.glp_add_rows(lp, totalNewConstraints);

        ind = GLPK.new_intArray(totalUe_Cell);
        val = GLPK.new_doubleArray(totalUe_Cell);
        rowName = "QoS";
        GLPK.glp_set_row_name(lp, rowIdx, rowName);
        GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, alpha*totalUe_Cell, Double.MAX_VALUE);

        for(Cell c : hashCell.values()){
            int len = c.getListReachableUE().size();
            int j=1;
            if(c.getTotalDLCapacity(frequency) > 0 && len > 1){
                for(UserEquipment ue : c.getListReachableUE()){
                    GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                    GLPK.doubleArray_setitem(val, j, 1.0);
                    j++;
                }
            }
        }
        GLPK.glp_set_mat_row(lp, rowIdx, totalUe_Cell, ind, val);
        dbg_counter++;
        rowIdx++;
        System.err.println("DEBUG: C5 = Adding " + dbg_counter + " new constraint(s).\n\n");


        //////////////////////////////////////////////////////////////////
        // Define objective                                             //
        //////////////////////////////////////////////////////////////////

        GLPK.glp_set_obj_name(lp, "z");
        GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MIN);
        GLPK.glp_set_obj_coef(lp, LmaxCol, 1);
        GLPK.glp_set_obj_coef(lp, LminCol, -1);


        //////////////////////////////////////////////////////////////////
        // Solve model                                                  //
        //////////////////////////////////////////////////////////////////

        parm = new glp_smcp();
        GLPK.glp_init_smcp(parm);
        ret = GLPK.glp_simplex(lp, parm);


        //////////////////////////////////////////////////////////////////
        // Retrieve solution                                            //
        //////////////////////////////////////////////////////////////////

        if (ret == 0) {
            write_lp_solution(lp);
        } else {
            System.out.println("The problem could not be solved");
        }

        // Free memory
        GLPK.glp_delete_prob(lp);
    }



    /**
     * LP model without timeframe allocation 
     */
    public static void solveILPModelDownAndUplink(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell, double minLoadPercentage) {
        glp_prob lp;
        glp_smcp parm;
        SWIGTYPE_p_int ind;
        SWIGTYPE_p_double val;
        int ret;
        int totalNewConstraints = 0;
        int totalUe_Cell = 0;
        int totalCell_Ue = 0;
        String rowName = "";


        System.out.println("\n\nRunning GLPK - LP model without timeframe allocation considering downlink and uplink\n");
        System.out.println("Minimize z = Lmax - Lmin\n\n" +
                "subject to\n" +
                "0.0 <= Lmax - Lmax(d)\n" + 
                "0.0 <= Lmax - Lmax(u)\n" + 
                "0.0 <= Lmin(d) - Lmin\n" + 
                "0.0 <= Lmin(u) - Lmin\n" + 
                "0.0 <= cb(d)*Lmax(d) - sum(m in Mb, lmb(d)*xmb), for all b in B\n" +
                "0.0 <= cb(u)*Lmax(u) - sum(m in Mb, lmb(u)*xmb), for all b in B\n" +
                "0.0 <= sum_{m in Mb}(lmb(d)*xmb) - cb(d)*Lmin(d), for all b in B\n" +
                "0.0 <= sum_{m in Mb}(lmb(u)*xmb) - cb(u)*Lmin(u), for all b in B\n" +
                "0.0 <= sum_{m in Mb}(lmb(d)*xmb) <= cb(d), for all b in B\n" +
                "0.0 <= sum_{m in Mb}(lmb(u)*xmb) <= cb(u), for all b in B\n" +
                "0.0 <= sum_{b in B}(xmb) <= 1, for all b in B\n" +
                "alpha*|Mb| <= sum_{b in B}(sum_{m in Mb}(xmb)), for all b in B\n" +
                "xmb in {0,1}\n");

        // Create problem
        lp = GLPK.glp_create_prob();
        System.out.println("Problem created");
        GLPK.glp_set_prob_name(lp, "lbProblem");

        // Define columns
        int totalCols = 6; // Lmin and Lmax

        for(Cell c : hashCell.values()){
            totalCols += c.getListReachableUE().size();
        }

        GLPK.glp_add_cols(lp, totalCols);

        final int LmaxCol = 1;
        String LmaxColName = "Lmax";
        System.out.println("DEBUG: colName(" + LmaxCol + ") = " + LmaxColName);
        GLPK.glp_set_col_name(lp, LmaxCol, LmaxColName);
        GLPK.glp_set_col_kind(lp, LmaxCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LmaxCol, GLPKConstants.GLP_DB, 0, 1.0);

        final int LminCol = 2;
        String LminColName = "Lmin";
        System.out.println("DEBUG: colName(" + LminCol + ") = " + LminColName);
        GLPK.glp_set_col_name(lp, LminCol, LminColName);
        GLPK.glp_set_col_kind(lp, LminCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LminCol, GLPKConstants.GLP_DB, 0, 1.0);

        final int LmaxDLCol = 3;
        String LmaxDLColName = "LmaxDL";
        System.out.println("DEBUG: colName(" + LmaxDLCol + ") = " + LmaxDLColName);
        GLPK.glp_set_col_name(lp, LmaxDLCol, LmaxDLColName);
        GLPK.glp_set_col_kind(lp, LmaxDLCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LmaxDLCol, GLPKConstants.GLP_DB, 0, 1.0);

        final int LminDLCol = 4;
        String LminDLColName = "LminDL";
        System.out.println("DEBUG: colName(" + LminDLCol + ") = " + LminDLColName);
        GLPK.glp_set_col_name(lp, LminDLCol, LminDLColName);
        GLPK.glp_set_col_kind(lp, LminDLCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LminDLCol, GLPKConstants.GLP_DB, 0, 1.0);

        final int LmaxULCol = 5;
        String LmaxULColName = "LmaxUL";
        System.out.println("DEBUG: colName(" + LmaxULCol + ") = " + LmaxULColName);
        GLPK.glp_set_col_name(lp, LmaxULCol, LmaxULColName);
        GLPK.glp_set_col_kind(lp, LmaxULCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LmaxULCol, GLPKConstants.GLP_DB, 0, 1.0);

        final int LminULCol = 6;
        String LminULColName = "LminUL";
        System.out.println("DEBUG: colName(" + LminULCol + ") = " + LminULColName);
        GLPK.glp_set_col_name(lp, LminULCol, LminULColName);
        GLPK.glp_set_col_kind(lp, LminULCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LminULCol, GLPKConstants.GLP_DB, 0, 1.0);

        HashMap<String, Integer> colMap = new HashMap<String, Integer>();
        int colId = 7;

        for(Cell c : hashCell.values()){
            for(UserEquipment ue : c.getListReachableUE()){
                String colName = "x_" + c.getId() + "_" + ue.getId();
                System.out.println("DEBUG: colName(" + colId + ") = " + colName);
                GLPK.glp_set_col_name(lp, colId, colName);
                GLPK.glp_set_col_kind(lp, colId, GLPKConstants.GLP_BV);
                colMap.put(c.getId() + " " + ue.getId(), colId);
                colId++;
            }
        }

        System.out.println("DEBUG: max colId = " + (colId - 1));

        // Getting the frequency
        UserEquipment ms = (UserEquipment)hashUE.values().toArray()[0];
        double frequency = ms.getService().getMinFrequency();

        // Create constraints
        int rowIdx = 1;
        int dbg_counter = 0;

        for(Cell c : hashCell.values()){
            if(c.getTotalDLCapacity(frequency) > 0 && c.getListReachableUE().size() != 0)
                totalUe_Cell++;
        }

        for(UserEquipment ue : hashUE.values()){
            if(ue.getListReachableCells().size() != 0){
                totalCell_Ue++;
            }
        }

        System.err.println("DEBUG: totalUe_Cell = " + totalUe_Cell + "\nDEBUG: totalCell_Ue = " + totalCell_Ue);

        /*
                "0.0 <= Lmax - Lmax(d)" + 
                "0.0 <= Lmax - Lmax(u)" + 
                "0.0 <= Lmin(d) - Lmin" + 
                "0.0 <= Lmin(u) - Lmin" + 
         */
        totalNewConstraints = 4;
        System.err.println("DEBUG: C1-4 = Adding " + totalNewConstraints + " new row(s).");
        GLPK.glp_add_rows(lp, totalNewConstraints);

        ind = GLPK.new_intArray(2);
        val = GLPK.new_doubleArray(2);
        rowName = "LmaxDL";
        GLPK.glp_set_row_name(lp, rowIdx, rowName);
        GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0, Double.MAX_VALUE);
        GLPK.intArray_setitem(ind, 1, LmaxCol);
        GLPK.doubleArray_setitem(val, 1, 1.0);
        GLPK.intArray_setitem(ind, 2, LmaxDLCol);
        GLPK.doubleArray_setitem(val, 2, -1.0);
        GLPK.glp_set_mat_row(lp, rowIdx, 2, ind, val);
        rowIdx++;

        ind = GLPK.new_intArray(2);
        val = GLPK.new_doubleArray(2);
        rowName = "LmaxUL";
        GLPK.glp_set_row_name(lp, rowIdx, rowName);
        GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0, Double.MAX_VALUE);
        GLPK.intArray_setitem(ind, 1, LmaxCol);
        GLPK.doubleArray_setitem(val, 1, 1.0);
        GLPK.intArray_setitem(ind, 2, LmaxULCol);
        GLPK.doubleArray_setitem(val, 2, -1.0);
        GLPK.glp_set_mat_row(lp, rowIdx, 2, ind, val);
        rowIdx++;

        ind = GLPK.new_intArray(2);
        val = GLPK.new_doubleArray(2);
        rowName = "LminDL";
        GLPK.glp_set_row_name(lp, rowIdx, rowName);
        GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0, Double.MAX_VALUE);
        GLPK.intArray_setitem(ind, 1, LminCol);
        GLPK.doubleArray_setitem(val, 1, -1.0);
        GLPK.intArray_setitem(ind, 2, LminDLCol);
        GLPK.doubleArray_setitem(val, 2, 1.0);
        GLPK.glp_set_mat_row(lp, rowIdx, 2, ind, val);
        rowIdx++;

        ind = GLPK.new_intArray(2);
        val = GLPK.new_doubleArray(2);
        rowName = "LminUL";
        GLPK.glp_set_row_name(lp, rowIdx, rowName);
        GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0, Double.MAX_VALUE);
        GLPK.intArray_setitem(ind, 1, LminCol);
        GLPK.doubleArray_setitem(val, 1, 1.0);
        GLPK.intArray_setitem(ind, 2, LminULCol);
        GLPK.doubleArray_setitem(val, 2, -1.0);
        GLPK.glp_set_mat_row(lp, rowIdx, 2, ind, val);
        rowIdx++;


        // 0.0 <= cb(d)*Lmax(d) - sum(m in Mb, lmb(d)*xmb), for all b in B
        System.err.println("\n\nDEBUG: C5: 0.0 <= cb(d)*Lmax(d) - sum(m in Mb, lmb(d)*xmb), for all b in B\n");
        totalNewConstraints = totalUe_Cell;
        System.err.println("DEBUG: C5 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);
            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(c.getTotalDLCapacity(frequency) > 0 && len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    rowName = "LmaxDL_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);

                    System.err.println("DEBUG: " + rowName + " - ind[" + j +  "] = " + LmaxDLCol);
                    System.err.println("DEBUG: " + rowName + " - val[" + j +  "] = " + c.getTotalDLCapacity(frequency));
                    GLPK.intArray_setitem(ind, j, LmaxDLCol);
                    GLPK.doubleArray_setitem(val, j, c.getTotalDLCapacity(frequency));
                    j++;

                    for(UserEquipment ue : c.getListReachableUE()){
                        System.err.println("DEBUG: " + rowName + " - ind[" + j + "] = colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()));
                        GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                        double lmb = (double) TransmissionLink.getRequiredMinNumPRBs(ue, c);
                        GLPK.doubleArray_setitem(val, j, -lmb);
                        System.err.println("DEBUG: " + rowName + " - val[" + j + "] = " + -lmb);
                        j++;
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        System.err.println("DEBUG: C5 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= cb(u)*Lmax(u) - sum(m in Mb, lmb(u)*xmb), for all b in B
        System.err.println("\n\nDEBUG: C6: 0.0 <= cb(u)*Lmax(u) - sum(m in Mb, lmb(u)*xmb), for all b in B\n");
        totalNewConstraints = totalUe_Cell;
        System.err.println("DEBUG: C6 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);
            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(c.getTotalULCapacity(frequency) > 0 && len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    rowName = "LmaxDL_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);

                    System.err.println("DEBUG: " + rowName + " - ind[" + j +  "] = " + LmaxULCol);
                    System.err.println("DEBUG: " + rowName + " - val[" + j +  "] = " + c.getTotalULCapacity(frequency));
                    GLPK.intArray_setitem(ind, j, LmaxULCol);
                    GLPK.doubleArray_setitem(val, j, c.getTotalULCapacity(frequency));
                    j++;

                    for(UserEquipment ue : c.getListReachableUE()){
                        System.err.println("DEBUG: " + rowName + " - ind[" + j + "] = colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()));
                        GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                        double lmb = (double) TransmissionLink.getRequiredMinNumPRBs(ue, c);
                        GLPK.doubleArray_setitem(val, j, -lmb);
                        System.err.println("DEBUG: " + rowName + " - val[" + j + "] = " + -lmb);
                        j++;
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        System.err.println("DEBUG: C6 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= sum_{m in Mb}(lmb(d)*xmb) - cb(d)*Lmin(d), for all b in B
        System.err.println("\n\nDEBUG: C7: 0.0 <= sum_{m in Mb}(lmb(d)*xmb) - cb(d)*Lmin(d), for all b in B\n");
        totalNewConstraints = totalUe_Cell;
        System.err.println("DEBUG: C7 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);

            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    rowName = "LminDL_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);

                    System.err.println("DEBUG: " + rowName + " - ind[" + j +  "] = " + LminDLCol);
                    System.err.println("DEBUG: " + rowName + " - val[" + j +  "] = " + -c.getTotalDLCapacity(frequency));
                    GLPK.intArray_setitem(ind, j, LminDLCol);
                    GLPK.doubleArray_setitem(val, j, -c.getTotalDLCapacity(frequency));
                    j++;

                    for(UserEquipment ue : c.getListReachableUE()){
                        System.err.println("DEBUG: " + rowName + " - ind[" + j + "] = colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()));
                        GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                        double lmb = ue.getTransmisisonLink(c.getId()).getCell2UETxRate();
                        GLPK.doubleArray_setitem(val, j, lmb);
                        System.err.println("DEBUG: " + rowName + " - val[" + j + "] = " + lmb);
                        j++;
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        System.err.println("DEBUG: C8 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= sum_{m in Mb}(lmb(d)*xmb) - cb(d)*Lmin(d), for all b in B
        System.err.println("\n\nDEBUG: C8: 0.0 <= sum_{m in Mb}(lmb(u)*xmb) - cb(u)*Lmin(u), for all b in B\n");
        totalNewConstraints = totalUe_Cell;
        System.err.println("DEBUG: C8 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);

            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    rowName = "LminUL_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);

                    System.err.println("DEBUG: " + rowName + " - ind[" + j +  "] = " + LminULCol);
                    System.err.println("DEBUG: " + rowName + " - val[" + j +  "] = " + -c.getTotalULCapacity(frequency));
                    GLPK.intArray_setitem(ind, j, LminULCol);
                    GLPK.doubleArray_setitem(val, j, -c.getTotalULCapacity(frequency));
                    j++;

                    for(UserEquipment ue : c.getListReachableUE()){
                        System.err.println("DEBUG: " + rowName + " - ind[" + j + "] = colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()));
                        GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                        double lmb = ue.getTransmisisonLink(c.getId()).getUe2CellTxRate();
                        GLPK.doubleArray_setitem(val, j, lmb);
                        System.err.println("DEBUG: " + rowName + " - val[" + j + "] = " + lmb);
                        j++;
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        System.err.println("DEBUG: C8 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= sum_{m in Mb}(lmb(d)*xmb) <= cb(d), for all b in B
        totalNewConstraints = totalUe_Cell;
        //System.err.println("DEBUG: C9 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);

            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    rowName = "CapacityDL_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_DB, 0.0, c.getTotalDLCapacity(frequency));

                    for(UserEquipment ue : c.getListReachableUE()){
                        //System.out.println("DEBUG: " + rowName + " - colId: " + colMap.get(c.getId() + " " + ue.getId()));
                        GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                        double lmb = (double) TransmissionLink.getRequiredMinNumPRBs(ue, c);
                        GLPK.doubleArray_setitem(val, j, lmb);
                        j++;
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }


        // 0.0 <= sum_{m in Mb}(lmb(u)*xmb) <= cb(u), for all b in B
        totalNewConstraints = totalUe_Cell;
        //System.err.println("DEBUG: C10 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);

            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    rowName = "CapacityUL_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_DB, 0.0, c.getTotalULCapacity(frequency));

                    for(UserEquipment ue : c.getListReachableUE()){
                        //System.out.println("DEBUG: " + rowName + " - colId: " + colMap.get(c.getId() + " " + ue.getId()));
                        GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                        double lmb = (double) TransmissionLink.getRequiredMinNumPRBs(ue, c);
                        GLPK.doubleArray_setitem(val, j, lmb);
                        j++;
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        //System.err.println("DEBUG: C10 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= sum_{b in B}(xmb) <= 1, for all b in B
        totalNewConstraints = totalCell_Ue;
        //System.err.println("DEBUG: C11 = Adding " + totalNewConstraints + " new row(s).");
        GLPK.glp_add_rows(lp, totalNewConstraints);

        for(UserEquipment ue : hashUE.values()){
            int len = ue.getListReachableCells().size();
            ind = GLPK.new_intArray(len);
            val = GLPK.new_doubleArray(len);
            int j=1;
            for(Cell c : ue.getListReachableCells()){
                dbg_counter++;
                GLPK.glp_set_row_name(lp, rowIdx, "assoc_" + ue.getId());
                GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_DB, 0, 1.0);
                //System.out.println("DEBUG: 2 - colId: " + colMap.get(c.getId() + " " + ue.getId()));
                GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                GLPK.doubleArray_setitem(val, j, 1.0);
                GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                rowIdx++;
                j++;
            }
        }

        //System.err.println("DEBUG: C11 = Adding " + dbg_counter + " new constraint(s).");


        // alpha*|Mb| <= sum_{b in B}(sum_{m in Mb}(xmb))
        double alpha = minLoadPercentage;
        totalNewConstraints = 1;
        //System.err.println("DEBUG: C5 = Adding " + totalNewConstraints + " new row(s).");
        GLPK.glp_add_rows(lp, totalNewConstraints);

        ind = GLPK.new_intArray(totalUe_Cell);
        val = GLPK.new_doubleArray(totalUe_Cell);
        rowName = "QoS";
        GLPK.glp_set_row_name(lp, rowIdx, rowName);
        GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, alpha*totalUe_Cell, Double.MAX_VALUE);

        dbg_counter = 0;
        for(Cell c : hashCell.values()){
            int len = c.getListReachableUE().size();
            int j=1;
            if(c.getTotalDLCapacity(frequency) > 0 && len > 1){
                for(UserEquipment ue : c.getListReachableUE()){
                    GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                    GLPK.doubleArray_setitem(val, j, 1.0);
                    j++;
                }
            }
        }
        GLPK.glp_set_mat_row(lp, rowIdx, totalUe_Cell, ind, val);
        dbg_counter++;
        rowIdx++;
        //System.err.println("DEBUG: C5 = Adding " + dbg_counter + " new constraint(s).");


        // Define objective
        GLPK.glp_set_obj_name(lp, "z");
        GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MIN);
        GLPK.glp_set_obj_coef(lp, LmaxCol, 1);
        GLPK.glp_set_obj_coef(lp, LminCol, -1);

        // Solve model
        parm = new glp_smcp();
        GLPK.glp_init_smcp(parm);
        ret = GLPK.glp_simplex(lp, parm);

        // Retrieve solution
        if (ret == 0) {
            write_lp_solution(lp);
        } else {
            System.out.println("The problem could not be solved");
        }

        // Free memory
        GLPK.glp_delete_prob(lp);
    }



    /**
     * LP model with timeframe allocation 
     */
    public static void solveILPModelResAllocDownlink(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell, double minLoadPercentage, int numTimeframes) {
        glp_prob lp;
        glp_smcp parm;
        SWIGTYPE_p_int ind;
        SWIGTYPE_p_double val;
        int ret;
        int totalNewConstraints = 0;
        int totalUe_Cell = 0;
        int totalCell_Ue = 0;
        int totalEffUePerCell = 0;

        System.out.println("\n\nRunning GLPK - LP model without timeframe allocation\n");
        System.out.println("Minimize z = Lmax - Lmin\n\n" +
                "subject to\n" +
                "0.0 <= cb*Lmax - db*sum{m in Mb}(sum{t in T}(ymbt)), for all b in B\n" +
                "0.0 <= db*sum_{m in Mb}(sum{t in T}(ymbt)) - cb*Lmin, for all b in B\n" +
                "0.0 <= db*sum_{m in Mb}(sum{t in T}(ymbt)) <= cb, for all b in B\n" +
                "lmb <= db*sum{t in T}(ymbt) , for all m in Mb, for all b in B\n" +
                "0.0 <= sum_{b in B}(xmb) <= 1, for all b in B\n" +
                "0.0 <= xmb - ymbt , for all m in Mb, for all b in B, for all t in T\n" +
                "alpha*|Mb| <= sum_{b in B}(sum_{m in Mb}(xmb)), for all b in B\n" +
                "xmb in {0,1}\n");

        // Create problem
        lp = GLPK.glp_create_prob();
        System.out.println("Problem created");
        GLPK.glp_set_prob_name(lp, "lbProblem");

        // Define columns
        int totalCols = 2; // Lmin and Lmax

        for(Cell c : hashCell.values()){
            totalCols += c.getListReachableUE().size()*(numTimeframes + 1); // xmb + ymbt
        }

        GLPK.glp_add_cols(lp, totalCols);

        final int LmaxCol = 1;
        String LmaxColName = "Lmax";
        System.out.println("DEBUG: colName(" + LmaxCol + ") = " + LmaxColName);
        GLPK.glp_set_col_name(lp, LmaxCol, LmaxColName);
        GLPK.glp_set_col_kind(lp, LmaxCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LmaxCol, GLPKConstants.GLP_DB, 0, 1.0);

        final int LminCol = 2;
        String LminColName = "Lmin";
        System.out.println("DEBUG: colName(" + LminCol + ") = " + LminColName);
        GLPK.glp_set_col_name(lp, LminCol, LminColName);
        GLPK.glp_set_col_kind(lp, LminCol, GLPKConstants.GLP_CV);
        GLPK.glp_set_col_bnds(lp, LminCol, GLPKConstants.GLP_DB, 0, 1.0);

        HashMap<String, Integer> colMap = new HashMap<String, Integer>();
        int colId = 3;

        for(Cell c : hashCell.values()){
            for(UserEquipment ue : c.getListReachableUE()){
                // Defining xmb
                String colName = "x_" + c.getId() + "_" + ue.getId();
                System.out.println("DEBUG: colName(" + colId + ") = " + colName);
                GLPK.glp_set_col_name(lp, colId, colName);
                GLPK.glp_set_col_kind(lp, colId, GLPKConstants.GLP_BV);
                colMap.put(c.getId() + " " + ue.getId(), colId);
                colId++;

                // Defining ymbt
                for(int t=1; t<=numTimeframes; t++){
                    colName = "y_" + c.getId() + "_" + ue.getId() + "_" + t;
                    System.out.println("DEBUG: colName(" + colId + ") = " + colName);
                    GLPK.glp_set_col_name(lp, colId, colName);
                    GLPK.glp_set_col_kind(lp, colId, GLPKConstants.GLP_BV);
                    colMap.put(c.getId() + " " + ue.getId() + " " + t, colId);
                    colId++;
                }
            }
        }

        System.out.println("DEBUG: max colId = " + (colId - 1));

        // Getting the frequency
        UserEquipment ms = (UserEquipment)hashUE.values().toArray()[0];
        double frequency = ms.getService().getMinFrequency();

        // Create constraints
        int rowIdx = 1;
        int dbg_counter = 0;

        for(Cell c : hashCell.values()){
            if(c.getTotalDLCapacity(frequency) > 0 && c.getListReachableUE().size() != 0)
                totalUe_Cell++;
        }

        for(UserEquipment ue : hashUE.values()){
            if(ue.getListReachableCells().size() != 0){
                totalCell_Ue++;
            }
        }

        for(Cell c : hashCell.values()){
            if(c.getTotalDLCapacity(frequency) > 0){
                totalEffUePerCell += c.getListReachableUE().size();
            }
        }

        System.err.println("DEBUG: totalUe_Cell = " + totalUe_Cell + "\nDEBUG: totalCell_Ue = " + totalCell_Ue);


        // 0.0 <= cb*Lmax - db*sum{m in Mb}(sum{t in T}(ymbt)), for all b in B
        System.err.println("\n\nDEBUG: C1: 0.0 <= cb*Lmax - db*sum{m in Mb}(sum{t in T}(ymbt)), for all b in B\n");
        totalNewConstraints = totalUe_Cell;
        System.err.println("DEBUG: C1 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);
            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(c.getTotalDLCapacity(frequency) > 0 && len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len * numTimeframes);
                    val = GLPK.new_doubleArray(len * numTimeframes);
                    String rowName = "Lmax_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);

                    System.err.println("DEBUG: " + rowName + " - ind[" + j +  "] = " + LmaxCol);
                    System.err.println("DEBUG: " + rowName + " - val[" + j +  "] = " + c.getTotalDLCapacity(frequency));
                    GLPK.intArray_setitem(ind, j, LmaxCol);
                    GLPK.doubleArray_setitem(val, j, c.getTotalDLCapacity(frequency));
                    j++;

                    // TODO: calculate db
                    double db = 1.0;

                    for(UserEquipment ue : c.getListReachableUE()){
                        for(int t=1; t<=numTimeframes; t++){
                            System.err.println("DEBUG: " + rowName + " - ind[" + j + "] = colMap.get(" + c.getId() + " " + ue.getId() + " " + t + ") = " + colMap.get(c.getId() + " " + ue.getId() + " " + t));
                            GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId() + " " + t));
                            GLPK.doubleArray_setitem(val, j, -db);
                            System.err.println("DEBUG: " + rowName + " - val[" + j + "] = " + -db);
                            j++;
                        }
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        System.err.println("DEBUG: C1 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= db*sum_{m in Mb}(sum{t in T}(ymbt)) - cb*Lmin, for all b in B
        System.err.println("\n\nDEBUG: C2: 0.0 <= db*sum_{m in Mb}(sum{t in T}(ymbt)) - cb*Lmin, for all b in B\n");
        totalNewConstraints = totalUe_Cell;
        System.err.println("DEBUG: C2 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);

            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    String rowName = "Lmin_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);

                    System.err.println("DEBUG: " + rowName + " - ind[" + j +  "] = " + LminCol);
                    System.err.println("DEBUG: " + rowName + " - val[" + j +  "] = " + -c.getTotalDLCapacity(frequency));
                    GLPK.intArray_setitem(ind, j, LminCol);
                    GLPK.doubleArray_setitem(val, j, -c.getTotalDLCapacity(frequency));
                    j++;

                    // TODO: calculate db
                    double db = 1.0;

                    for(UserEquipment ue : c.getListReachableUE()){
                        for(int t=1; t<=numTimeframes; t++){
                            System.err.println("DEBUG: " + rowName + " - ind[" + j + "] = colMap.get(" + c.getId() + " " + ue.getId() + " " + t + ") = " + colMap.get(c.getId() + " " + ue.getId() + " " + t));
                            GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId() + " " + t));
                            GLPK.doubleArray_setitem(val, j, db);
                            System.err.println("DEBUG: " + rowName + " - val[" + j + "] = " + db);
                            j++;
                        }
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        System.err.println("DEBUG: C2 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= db*sum_{m in Mb}(sum{t in T}(ymbt)) <= cb, for all b in B
        totalNewConstraints = totalUe_Cell;
        System.err.println("DEBUG: C3 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);

            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int len = c.getListReachableUE().size();
                int j=1;
                if(len > 1){
                    dbg_counter++;
                    ind = GLPK.new_intArray(len);
                    val = GLPK.new_doubleArray(len);
                    String rowName = "Capacity_" + c.getId();
                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_DB, 0.0, c.getTotalDLCapacity(frequency));

                    // TODO: calculate db
                    double db = 1.0;

                    for(UserEquipment ue : c.getListReachableUE()){
                        for(int t=1; t<=numTimeframes; t++){
                            System.out.println("DEBUG: " + rowName + " - colId: " + colMap.get(c.getId() + " " + ue.getId() + " " + t));
                            GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId() + " " + t));
                            GLPK.doubleArray_setitem(val, j, db);
                            j++;
                        }
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                    rowIdx++;
                }
            }
        }

        System.err.println("DEBUG: C3 = Adding " + dbg_counter + " new constraint(s).");


        // lmb <= db*sum{t in T}(ymbt) , for all m in Mb, for all b in B
        totalNewConstraints = totalEffUePerCell;
        System.err.println("DEBUG: C4 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);
            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                int j=1;

                // TODO: calculate db
                double db = 1.0;

                for(UserEquipment ue : c.getListReachableUE()){
                    dbg_counter++;
                    ind = GLPK.new_intArray(numTimeframes);
                    val = GLPK.new_doubleArray(numTimeframes);
                    String rowName = "ueMinBandwidth_" + ue.getId() + "_" + c.getId();

                    // TODO: Fix lmb
                    double lmb = TransmissionLink.getRequiredMinNumPRBs(ue, c);

                    GLPK.glp_set_row_name(lp, rowIdx, rowName);
                    GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, lmb, Double.MAX_VALUE);

                    for(int t=1; t<=numTimeframes; t++){
                        System.out.println("DEBUG: " + rowName + " - colId: " + colMap.get(c.getId() + " " + ue.getId() + " " + t));
                        GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId() + " " + t));
                        GLPK.doubleArray_setitem(val, j, db);
                        j++;
                    }

                    GLPK.glp_set_mat_row(lp, rowIdx, numTimeframes, ind, val);
                    rowIdx++;
                }
            }
        }

        System.err.println("DEBUG: C4 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= sum_{b in B}(xmb) <= 1, for all b in B
        totalNewConstraints = totalCell_Ue;
        System.err.println("DEBUG: C5 = Adding " + totalNewConstraints + " new row(s).");
        GLPK.glp_add_rows(lp, totalNewConstraints);

        for(UserEquipment ue : hashUE.values()){
            int len = ue.getListReachableCells().size();
            ind = GLPK.new_intArray(len);
            val = GLPK.new_doubleArray(len);
            int j=1;
            for(Cell c : ue.getListReachableCells()){
                dbg_counter++;
                GLPK.glp_set_row_name(lp, rowIdx, "assoc_" + ue.getId());
                GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_DB, 0, 1.0);
                System.out.println("DEBUG: 2 - colId: " + colMap.get(c.getId() + " " + ue.getId()));
                GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                GLPK.doubleArray_setitem(val, j, 1.0);
                GLPK.glp_set_mat_row(lp, rowIdx, len, ind, val);
                rowIdx++;
                j++;
            }
        }

        System.err.println("DEBUG: C5 = Adding " + dbg_counter + " new constraint(s).");


        // 0.0 <= xmb - ymbt , for all m in Mb, for all b in B, for all t in T
        totalNewConstraints = totalEffUePerCell * numTimeframes;
        System.err.println("DEBUG: C6 = Adding " + totalNewConstraints + " new row(s).");

        if(totalNewConstraints > 0){
            GLPK.glp_add_rows(lp, totalNewConstraints);
            dbg_counter = 0;
            for(Cell c : hashCell.values()){
                for(UserEquipment ue : c.getListReachableUE()){
                    for(int t=1; t<=numTimeframes; t++){
                        dbg_counter++;
                        ind = GLPK.new_intArray(2);
                        val = GLPK.new_doubleArray(2);
                        String rowName = "uniqueAssoc_" + ue.getId() + "_" + c.getId() + "_" + t;
                        GLPK.glp_set_row_name(lp, rowIdx, rowName);
                        GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, 0.0, Double.MAX_VALUE);
                        System.out.println("DEBUG: " + rowName + " - colId: " + colMap.get(c.getId() + " " + ue.getId() + " " + t));

                        // xmb
                        GLPK.intArray_setitem(ind, 1, colMap.get(c.getId() + " " + ue.getId()));
                        GLPK.doubleArray_setitem(val, 1, 1.0);

                        // ymbt
                        GLPK.intArray_setitem(ind, 1, colMap.get(c.getId() + " " + ue.getId() + " " + t));
                        GLPK.doubleArray_setitem(val, 1, -1.0);

                        GLPK.glp_set_mat_row(lp, rowIdx, numTimeframes, ind, val);
                        rowIdx++;
                    }
                }
            }
        }

        System.err.println("DEBUG: C6 = Adding " + dbg_counter + " new constraint(s).");


        // alpha*|Mb| <= sum_{b in B}(sum_{m in Mb}(xmb))
        double alpha = minLoadPercentage;
        totalNewConstraints = 1;
        System.err.println("DEBUG: C7 = Adding " + totalNewConstraints + " new row(s).");
        GLPK.glp_add_rows(lp, totalNewConstraints);

        ind = GLPK.new_intArray(totalUe_Cell);
        val = GLPK.new_doubleArray(totalUe_Cell);
        String rowName = "QoS";
        GLPK.glp_set_row_name(lp, rowIdx, rowName);
        GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, alpha*totalUe_Cell, Double.MAX_VALUE);

        dbg_counter = 0;
        for(Cell c : hashCell.values()){
            int len = c.getListReachableUE().size();
            int j=1;
            if(c.getTotalDLCapacity(frequency) > 0 && len > 1){
                for(UserEquipment ue : c.getListReachableUE()){
                    GLPK.intArray_setitem(ind, j, colMap.get(c.getId() + " " + ue.getId()));
                    GLPK.doubleArray_setitem(val, j, 1.0);
                    j++;
                }
            }
        }
        GLPK.glp_set_mat_row(lp, rowIdx, totalUe_Cell, ind, val);
        dbg_counter++;
        rowIdx++;
        System.err.println("DEBUG: C7 = Adding " + dbg_counter + " new constraint(s).");


        // Define objective
        GLPK.glp_set_obj_name(lp, "z");
        GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MIN);
        GLPK.glp_set_obj_coef(lp, 1, 1);
        GLPK.glp_set_obj_coef(lp, 2, -1);

        // Solve model
        parm = new glp_smcp();
        GLPK.glp_init_smcp(parm);
        ret = GLPK.glp_simplex(lp, parm);

        // Retrieve solution
        if (ret == 0) {
            write_lp_solution(lp);
        } else {
            System.out.println("The problem could not be solved");
        }

        // Free memory
        GLPK.glp_delete_prob(lp);
    }

    public static void printMatrixRow(HashMap<Integer, Double> row, int maxCol){
        for(int i=1; i<=maxCol; i++){
            if(row.containsKey(i))
                System.err.format("  %+6.1f", row.get(i));
            else
                System.err.format("        ", row.get(i));
        }
        System.err.print("\n");
    }

    /**
     * @param hashUE
     * @param hashCell
     * @param strategy
     * @param qosFactor
     * @param inputTest
     * @return
     */
    public static HashMap<String,HashSet<String>> balanceGLPK(HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, LBStrategy strategy, double qosFactor, String inputTest){
        HashMap<String,HashSet<String>> association = null;
        String modelFilename = inputTest + "_" + strategy.name() + ".lp";
        String output = inputTest + "_" + strategy.name() + "_glpk.out";
        String solution = inputTest + "_" + strategy.name() + "_glpk.sol";

        switch(strategy){
            case LP_GREEDY:
                //LoadBalancer.createILPModel(hashUE, hashCell, qosFactor, modelFilename);
                GlpkHelper.callGLPK(modelFilename, output, solution);
                association = GlpkHelper.parseGLPKSolution(hashUE, hashCell, modelFilename, solution);
                break;

            case BEST_DOWNLINK:
            case BEST_SINR:
            case ILP_SIMPLE_MODEL:
            case PICO_CELL_FIRST:
/*           case LP_DL_AND_UL:
            case LP_RES_ALLOC:*/
            default:
                association = null;
                break;
        }

        return association;
    }
}
