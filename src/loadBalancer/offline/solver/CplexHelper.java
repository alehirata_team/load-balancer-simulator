package loadBalancer.offline.solver;

import hetnet.AssociationNode;
import hetnet.cell.Cell;
import hetnet.cell.CellFactory;
import hetnet.userEquipment.UserEquipment;
import hetnet.userEquipment.UserEquipmentFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import loadBalancer.offline.strategy.LBStrategy;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CplexHelper {
    /**
     * @param hashUE
     * @param hashCell
     * @param strategy
     * @param qosFactor
     * @param inputTest
     * @return
     */
    public static HashMap<String,HashSet<String>> parseCPLEXSolution(HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, LBStrategy strategy, double qosFactor, String inputTest){
        HashMap<String,HashSet<String>> association = null;
        String modelFilename = inputTest + "_" + strategy.name() + ".lp";
        String solution = inputTest + "_" + strategy.name() + "_cplex.sol";

        switch(strategy){
        case ILP_SIMPLE_MODEL:
            association = CplexHelper.parseCPLEX_ILPSolution(hashUE, hashCell, modelFilename, solution);
            break;

        case LP_GREEDY:
            association = CplexHelper.parseCPLEX_LPSolution(false, hashUE, hashCell, modelFilename, solution);
            break;

        case BEST_DOWNLINK:
        case BEST_SINR:
        case PICO_CELL_FIRST:
            /*            case LP_DL_AND_UL:
            case LP_RES_ALLOC:*/
        default:
            association = null;
            break;
        }

        return association;
    }

    /**
     * @param cplexModel
     * @param output
     * @param solution
     */
    public static void callCPLEX(String cplexModel, String output, String solution){
        System.out.println("\nCalling CPLEX\n");
        Process process;
        try {
            //Delete if tempFile exists
            File fileTemp = new File(solution);
            if (fileTemp.exists()){
                fileTemp.delete();
            }

            // cplex -c "read model.lp" "optimize"
            process = new ProcessBuilder("cplex","-c", "set logfile " + output, "set timelimit 300", "read " + cplexModel, "optimize", "write "+ solution + " 0 sol").start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String,HashSet<String>> parseCPLEX_ILPSolution(HashMap<String,UserEquipment> hashUE,
            HashMap<String,Cell> hashCell, String glpkModel, String solution){
        return parseCPLEX_LPSolution(false, hashUE, hashCell, glpkModel, solution);
    }

    public static HashMap<String,HashSet<String>> parseCPLEX_LPSolution(boolean isILP, HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, String glpkModel, String solution){
        Document dom;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        HashMap<String, Double> hsol = new HashMap<String, Double>();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            dom = db.parse(solution);

            NodeList nl = dom.getElementsByTagName("variable");
            for(int i=0; i<nl.getLength(); i++){
                Element el = (Element) nl.item(i);
                System.out.println(el.getAttribute("name") + " = " + el.getAttribute("value"));
                hsol.put(el.getAttribute("name"), Double.valueOf(el.getAttribute("value")));
            }

            return isILP ? createAssociationFromCPLEX_ILPSolution(hsol) : createAssociationFromCPLEX_LPSolution(hsol);

        } catch (ParserConfigurationException pce) {
            System.out.println(pce.getMessage());
        } catch (SAXException se) {
            System.out.println(se.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        return null;
    }

    private static HashMap<String,HashSet<String>> createAssociationFromCPLEX_ILPSolution(HashMap<String, Double> hsol){
        HashMap<String,HashSet<String>> assoc = new HashMap<String,HashSet<String>>();
        Pattern patternNodeName = Pattern.compile("([a-z]|[A-Z]|[0-9])+_([a-z]|[A-Z])+_([a-z]|[A-Z]|[0-9])+");
        Pattern patternUEName = Pattern.compile("([a-z]|[A-Z])+_([a-z]|[A-Z]|[0-9])+$");
        for(String varname : hsol.keySet()){
            if(varname.startsWith("x_") && hsol.get(varname) > 0.0){
                Matcher matcherNodeName = patternNodeName.matcher(varname);
                Matcher matcherUEName = patternUEName.matcher(varname);
                if (matcherNodeName.find() && matcherUEName.find()){
                    String nodeName = matcherNodeName.group(0);
                    String ueName = matcherUEName.group(0);
                    if(assoc.containsKey(nodeName)){
                        assoc.get(nodeName).add(ueName);
                    } else {
                        HashSet<String> ueSet = new HashSet<String>();
                        ueSet.add(ueName);
                        assoc.put(nodeName, ueSet);
                    }
                }
            }
        }

        return assoc;
    }


    /**
     * @param hsol CPLEX solution map
     * @return Association map
     */
    private static HashMap<String,HashSet<String>> createAssociationFromCPLEX_LPSolution(HashMap<String, Double> hsol){
        HashMap<String,HashSet<String>> assoc = new HashMap<String,HashSet<String>>();
        SortedMap<Double,ArrayList<AssociationNode>> sortedUE = new TreeMap<Double, ArrayList<AssociationNode>>(Collections.reverseOrder());
        Pattern patternCellName = Pattern.compile("([a-z]|[A-Z]|[0-9])+_([a-z]|[A-Z])+_([a-z]|[A-Z]|[0-9])+");
        Pattern patternUEName = Pattern.compile("([a-z]|[A-Z])+_([a-z]|[A-Z]|[0-9])+$");
        //SortedSet<AssociationNode> sortedUE = new TreeSet<AssociationNode>(Collections.reverseOrder());
        Set<Cell> lNotAssocUE = new TreeSet<Cell>();

        for(String varname : hsol.keySet()){
            double val = hsol.get(varname);
            if(varname.startsWith("x_") && val > 0.0){
                Matcher matcherCellName = patternCellName.matcher(varname);
                Matcher matcherUEName = patternUEName.matcher(varname);
                if (matcherCellName.find() && matcherUEName.find()){
                    String cellName = matcherCellName.group(0);
                    String ueName = matcherUEName.group(0);
                    AssociationNode node = new AssociationNode(ueName, cellName, val);
                    if(sortedUE.containsKey(val)){
                        sortedUE.get(val).add(node);
                    } else {
                        ArrayList<AssociationNode> listNodes = new ArrayList<AssociationNode>();
                        listNodes.add(node);
                        sortedUE.put(val, listNodes);
                    }
                } else {
                    System.err.println("ERROR: varname='" + varname + "' could not be parsed.");
                }
            }
        }

        /*
        System.out.println("\n\nDEBUG: Reading solution");
        for(ArrayList<AssociationNode> listNode : sortedUE.values()) {
            for(AssociationNode node : listNode) {
                System.out.println(node.getUeName() + " -> " + node.getCellName() + " (" + node.getVal() + ")");
            }
        }
         */

        //System.out.println("\n\nDEBUG: Associating UEs");
        for(ArrayList<AssociationNode> listNode : sortedUE.values()) {
            for(AssociationNode node : listNode) {
                String ueName = node.getUeName();
                String cellName = node.getCellName();
                UserEquipment ue = UserEquipmentFactory.getHashUE().get(ueName);
                if(ue.getAssociatedCell() == null) {
                    Cell cell = CellFactory.getHashCell().get(cellName);
                    if(cell.associateUE(ueName)){
                        //System.out.println(ueName + " >>> " + cellName);
                        if(assoc.containsKey(cellName)){
                            assoc.get(cellName).add(ueName);
                        } else {
                            HashSet<String> ueSet = new HashSet<String>();
                            ueSet.add(ueName);
                            assoc.put(cellName, ueSet);
                        }
                    } else {
                        lNotAssocUE.add(cell);
                    }
                }
            }
        }

        System.out.println("\n\nNot associated UEs (" + lNotAssocUE.size() + "): " + lNotAssocUE.toString());
        return assoc;
    }
}
