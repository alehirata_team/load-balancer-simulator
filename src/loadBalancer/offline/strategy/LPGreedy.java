package loadBalancer.offline.strategy;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.HashMap;
import java.util.HashSet;

import loadBalancer.offline.solver.CplexHelper;

public class LPGreedy extends LPStrategy {
    public HashMap<String,HashSet<String>> solveLP(HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, LBStrategy strategy, double qosFactor, String inputTest){
        String modelFilename = inputTest + "_" + strategy.name() + ".lp";
        String output = inputTest + "_" + strategy.name() + "_cplex.log";
        String solution = inputTest + "_" + strategy.name() + "_cplex.sol";
        ILPSimpleModel.createILPModel(hashUE, hashCell, qosFactor, modelFilename);
        CplexHelper.callCPLEX(modelFilename, output, solution);

        return CplexHelper.parseCPLEX_ILPSolution(hashUE, hashCell, modelFilename, solution);
    }
}
