package loadBalancer.offline.strategy;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public abstract class Strategy {
    public abstract HashMap<String,HashSet<String>> solve(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell);

    public List<UserEquipment> getRandomUEList(List<UserEquipment> ueList){
        List<UserEquipment> newUEList = new ArrayList<>(ueList);
        Collections.shuffle(newUEList);
        return newUEList;
    }
}
