package loadBalancer.offline.strategy;

import hetnet.TransmissionLink;
import hetnet.cell.Cell;
import hetnet.cell.CellType;
import hetnet.userEquipment.UserEquipment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;

public class PicoCellFirst extends Strategy{
    public HashMap<String,HashSet<String>> solve(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell){
        final double FACTOR = 1.2;
        HashMap<String,HashSet<String>> assocMap = new HashMap<String,HashSet<String>>();
        double sinr_aux = 0.0;

        for(UserEquipment ue : this.getRandomUEList(new ArrayList<UserEquipment>(hashUE.values()))){
            if(ue.getListReachableCells().size() > 0){
                // Sorting cells by SINR in ascending order
                SortedMap<Double, Cell> sinrMap = new TreeMap<>();
                for(Cell cell : ue.getListReachableCells()){
                    sinr_aux = TransmissionLink.getSINR(ue, cell);
                    if(cell.getType() == CellType.PICO){
                        sinr_aux *= FACTOR; 
                    }
                    sinrMap.put(sinr_aux, cell);
                }

                String assocCellId = null;
                for(double sinr : sinrMap.keySet()){
                    if(sinrMap.get(sinr).associateUE(ue.getId())){
                        assocCellId = sinrMap.get(sinr).getId(); 
                        break;
                    }
                }

                // Create an association between Cell and UE
                if(assocCellId != null){
                    if(!assocMap.containsKey(assocCellId)){
                        HashSet<String> s = new HashSet<String>();
                        s.add(ue.getId());
                        assocMap.put(assocCellId, s);
                    } else {
                        assocMap.get(assocCellId).add(ue.getId());
                    }
                }
            }
        }

        return assocMap;
    }
}
