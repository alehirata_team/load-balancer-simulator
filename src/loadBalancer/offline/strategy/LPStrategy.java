package loadBalancer.offline.strategy;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.HashMap;
import java.util.HashSet;

public abstract class LPStrategy {
    public abstract HashMap<String,HashSet<String>> solveLP(HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, LBStrategy strategy, double qosFactor, String inputTest);
}
