package loadBalancer.offline.strategy;

import hetnet.cell.Cell;
import hetnet.cell.RangeExpansionInfo;
import hetnet.userEquipment.UserEquipment;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Primal-Dual Distributed Algorithm
 * [2] Q. Ye, B. Rong, Y. Chen, M. Al-Shalash, C. Caramanis, and J. Andrews,
 * "User Association for Load Balancing in Heterogeneous Cellular Networks,"
 * Wireless Communications, IEEE Transactions on, vol. 12, no. 6, pp. 2706-2716,
 * June 2013.
 */
public class PrimalDualDistributed extends Strategy{
    public HashMap<String,HashSet<String>> solve(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell){
        HashMap<String,HashSet<String>> assocMap = new HashMap<String,HashSet<String>>();

        for(UserEquipment ue : hashUE.values()){
            if(ue.getListReachableCells().size() > 0){
                // Sorting cells by SINR in descending order
                SortedMap<Double, Cell> sinrMap = new TreeMap<>(Collections.reverseOrder());
                for(Cell cell : ue.getListReachableCells()){
                    sinrMap.put(getUtilityCellValue(ue, cell), cell);
                }

                String assocCellId = null;
                for(double sinr : sinrMap.keySet()){
                    Cell cell = sinrMap.get(sinr);
                    if(cell.associateUE(ue.getId())){
                        //updateRangeExpansionInfo(ue, cell);
                        assocCellId = cell.getId(); 
                        break;
                    }
                }

                // Create an association between Cell and UE
                if(assocCellId != null){
                    if(!assocMap.containsKey(assocCellId)){
                        HashSet<String> s = new HashSet<String>();
                        s.add(ue.getId());
                        assocMap.put(assocCellId, s);
                    } else {
                        assocMap.get(assocCellId).add(ue.getId());
                    }
                }
            }
        }

        return assocMap;
    }
    
    // User algorithm
    private double getUtilityCellValue(UserEquipment ue, Cell cell){
        double capacity = ue.getTransmisisonLink(cell.getId()).getEffCell2UETxRate();
        RangeExpansionInfo rangeExp = cell.getRangeExpansionInfo(ue.getService().getMinFrequency());
        double lagrangeMultiplier = (rangeExp != null) ? rangeExp.getLagrangeMultiplier() : 0.0;

        return Math.log(capacity) - lagrangeMultiplier;
    }
    
    // BS algorithm
    /*private void updateRangeExpansionInfo(UserEquipment ue, Cell cell){
        double frequency = ue.getService().getMinFrequency();
        RangeExpansionInfo rangeExp = cell.getRangeExpansionInfo(frequency);
        double totalUsers = cell.getAssocUE().get(frequency).size();
        rangeExp.setEffectiveLoad(Math.min((double)UserEquipmentFactory.getTotalUE(), Math.exp(rangeExp.getLagrangeMultiplier() - 1)));
        
        double newLagrangeMultiplier = 0.0;
        double stepSize = 0.5;
        
        
        rangeExp.setLagrangeMultiplier(newLagrangeMultiplier);
    }*/
}
