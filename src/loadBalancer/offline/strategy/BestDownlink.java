package loadBalancer.offline.strategy;

import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;

public class BestDownlink extends Strategy{
    public HashMap<String,HashSet<String>> solve(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell){
        HashMap<String,HashSet<String>> assocMap = new HashMap<String,HashSet<String>>();

        for(UserEquipment ue : this.getRandomUEList(new ArrayList<UserEquipment>(hashUE.values()))){
            if(ue.getListReachableCells().size() > 0){
                // Sorting cells by DownLink in descending order
                SortedMap<Double, Cell> dlMap = new TreeMap<>(Collections.reverseOrder());
                for(Cell cell : ue.getListReachableCells()){
                    dlMap.put(ue.getTransmisisonLink(cell.getId()).getEffCell2UETxRate(), cell);
                }

                String assocCellId = null;
                for(double dl : dlMap.keySet()){
                    if(dlMap.get(dl).associateUE(ue.getId())){
                        assocCellId = dlMap.get(dl).getId(); 
                        break;
                    }
                }

                // Create an association between Cell and UE
                if(assocCellId != null){
                    if(!assocMap.containsKey(assocCellId)){
                        HashSet<String> s = new HashSet<String>();
                        s.add(ue.getId());
                        assocMap.put(assocCellId, s);
                    } else {
                        assocMap.get(assocCellId).add(ue.getId());
                    }
                }
            }
        }

        return assocMap;
    }
}
