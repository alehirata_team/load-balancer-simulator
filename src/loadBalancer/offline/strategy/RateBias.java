package loadBalancer.offline.strategy;

import hetnet.TransmissionLink;
import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;

import util.Properties;

/**
 * Rate Biasing (Range Expansion)
 * [2] Q. Ye, B. Rong, Y. Chen, M. Al-Shalash, C. Caramanis, and J. Andrews,
 * "User Association for Load Balancing in Heterogeneous Cellular Networks,"
 * Wireless Communications, IEEE Transactions on, vol. 12, no. 6, pp. 2706-2716,
 * June 2013.
 */
public class RateBias extends Strategy{

    private double macroCellBias;
    private double picoCellBias;
    private double femtoCellBias;

    public RateBias(){
        super();
        this.macroCellBias = Double.valueOf(Properties.get("rateBiasMacroCell"));
        this.picoCellBias = Double.valueOf(Properties.get("rateBiasPicoCell"));
        this.femtoCellBias = Double.valueOf(Properties.get("rateBiasFemtoCell"));
        System.out.println("RATE BIAS (macro=" + this.macroCellBias + ", pico=" + this.picoCellBias + ", femto=" + this.femtoCellBias + ")");
    }

    public HashMap<String,HashSet<String>> solve(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell){
        HashMap<String,HashSet<String>> assocMap = new HashMap<String,HashSet<String>>();

        for(UserEquipment ue : this.getRandomUEList(new ArrayList<UserEquipment>(hashUE.values()))){
            if(ue.getListReachableCells().size() > 0){
                // Sorting cells by SINR in descending order
                SortedMap<Double, Cell> sinrMap = new TreeMap<>(Collections.reverseOrder());
                for(Cell cell : ue.getListReachableCells()){
                    sinrMap.put(getSINRBias(ue, cell), cell);
                }

                String assocCellId = null;
                for(double sinr : sinrMap.keySet()){
                    Cell cell = sinrMap.get(sinr);
                    if(cell.associateUE(ue.getId())){
                        assocCellId = cell.getId(); 
                        break;
                    }
                }

                // Create an association between Cell and UE
                if(assocCellId != null){
                    if(!assocMap.containsKey(assocCellId)){
                        HashSet<String> s = new HashSet<String>();
                        s.add(ue.getId());
                        assocMap.put(assocCellId, s);
                    } else {
                        assocMap.get(assocCellId).add(ue.getId());
                    }
                }
            }
        }

        return assocMap;
    }

    // User algorithm
    private double getSINRBias(UserEquipment ue, Cell cell){
        double bias = 0.0;

        switch(cell.getType()){
        case MACRO:
            bias = macroCellBias;
            break;
        case PICO:
            bias = picoCellBias;
            break;
        case FEMTO:
            bias = femtoCellBias;
            break;
        default:
        }

        return Math.pow(1+TransmissionLink.getSINR(ue, cell), bias);
    }
}
