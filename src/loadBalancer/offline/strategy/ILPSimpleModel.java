package loadBalancer.offline.strategy;

import hetnet.TransmissionLink;
import hetnet.cell.Cell;
import hetnet.userEquipment.UserEquipment;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;

import loadBalancer.offline.solver.CplexHelper;
import loadBalancer.offline.solver.GlpkHelper;

public class ILPSimpleModel extends LPStrategy {
    public HashMap<String,HashSet<String>> solveLP(HashMap<String,UserEquipment> hashUE, 
            HashMap<String,Cell> hashCell, LBStrategy strategy, double qosFactor, String inputTest){
        String modelFilename = inputTest + "_" + strategy.name() + ".lp";
        String output = inputTest + "_" + strategy.name() + "_cplex.log";
        String solution = inputTest + "_" + strategy.name() + "_cplex.sol";
        createILPModel(hashUE, hashCell, qosFactor, modelFilename);
        CplexHelper.callCPLEX(modelFilename, output, solution);

        return CplexHelper.parseCPLEX_ILPSolution(hashUE, hashCell, modelFilename, solution);
    }

    /**
     * LP model without timeframe allocation 
     */
    public static void createLPModel(boolean isILP, HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell, double minLoadPercentage, String outFileName) {
        String rowName = "";
        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(outFileName), "utf-8"));
            //writer.write("Something");


            System.out.println("\n\nRunning GLPK - LP model without timeframe allocation\n");
            System.out.println("Minimize z = Lmax - Lmin\n\n" +
                    "subject to\n" +
                    "cb*Lmax - sum(m in Mb, lmb*xmb) >= 0.0, for all b in B\n" +
                    "sum_{m in Mb}(lmb*xmb) - cb*Lmin >= 0.0, for all b in B\n" +
                    "sum_{b in B}(xmb) <= 1, for all m in Mb\n" +
                    "sum_{m in Mb}(lmb*xmb) <= cb, for all b in B\n" +
                    "sum_{b in B}(sum_{m in Mb}(xmb)) >= alpha*|Mb|, for all b in B\n" +
                    "xmb in {0,1}\n");

            String LmaxColName = "Lmax";
            String LminColName = "Lmin";

            // Getting the frequency
            UserEquipment ms = (UserEquipment)hashUE.values().toArray()[0];
            double frequency = ms.getService().getMinFrequency();


            writer.write("Minimize\n");
            writer.write("obj: " + LmaxColName + " - " + LminColName + "\n");
            writer.write("Subject To\n");

            //////////////////////////////////////////////////////////////////
            // Create constraints                                           //
            //////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////
            // 0.0 <= cb*Lmax - sum(m in Mb, lmb*xmb), for all b in B       //
            //////////////////////////////////////////////////////////////////
            {
                //System.err.println("\n\nDEBUG: C1: 0.0 <= cb*Lmax - sum(m in Mb, lmb*xmb), for all b in B");
                HashMap<String, HashMap<String, Double>> constraintsMap = new HashMap<String, HashMap<String, Double>>();

                //GLPK.glp_add_rows(lp, totalNewConstraints);

                for(Cell c : hashCell.values()){
                    if(c.getTotalDLCapacity(frequency) > 0 && c.getListReachableUE().size() > 0){
                        HashMap<String, Double> row = new HashMap<String, Double>();
                        rowName = "Lmax_" + c.getId();

                        for(UserEquipment ue : c.getListReachableUE()){
                            double lmb = (double) TransmissionLink.getRequiredMinNumPRBs(ue, c);
                            if(lmb != 0){
                                row.put("x_" + c.getId() + "_" + ue.getId(), -lmb);
                                //System.err.println("DEBUG: " + rowName + " - colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()) + "\t lmb = " + lmb);
                            }
                        }

                        if(row.size() > 0){
                            row.put(LmaxColName, c.getTotalDLCapacity(frequency));
                            constraintsMap.put(rowName, row);
                        }
                    }
                }

                if(!constraintsMap.isEmpty()){
                    for(String rName : constraintsMap.keySet()){
                        HashMap<String, Double> row = constraintsMap.get(rName);
                        System.out.println(GlpkHelper.getGlpkConstraint(rName, row) + ">= 0");
                        writer.write(GlpkHelper.getGlpkConstraint(rName, row) + ">= 0\n");
                    }
                }
                constraintsMap.clear();
            }

            //////////////////////////////////////////////////////////////////
            // 0.0 <= sum_{m in Mb}(lmb*xmb) - cb*Lmin, for all b in B      //
            //////////////////////////////////////////////////////////////////
            {
                HashMap<String, HashMap<String, Double>> constraintsMap = new HashMap<String, HashMap<String, Double>>();

                for(Cell c : hashCell.values()){
                    if(c.getListReachableUE().size() > 0){
                        HashMap<String, Double> row = new HashMap<String, Double>();
                        rowName = "Lmin_" + c.getId();

                        for(UserEquipment ue : c.getListReachableUE()){
                            double lmb = (double) TransmissionLink.getRequiredMinNumPRBs(ue, c);
                            if(lmb != 0){
                                row.put("x_" + c.getId() + "_" + ue.getId(), lmb);
                                //System.err.println("DEBUG: " + rowName + " - colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()) + "\t lmb = " + lmb);
                            }
                        }

                        if(row.size() > 0){
                            row.put(LminColName, -c.getTotalDLCapacity(frequency));
                            constraintsMap.put(rowName, row);
                        }
                    }
                }

                if(!constraintsMap.isEmpty()){
                    for(String rName : constraintsMap.keySet()){
                        HashMap<String, Double> row = constraintsMap.get(rName);
                        System.out.println(GlpkHelper.getGlpkConstraint(rName, row) + ">= 0");
                        writer.write(GlpkHelper.getGlpkConstraint(rName, row) + ">= 0\n");
                    }
                }
                //System.err.println("DEBUG: C2 = Adding " + dbg_counter + " new constraint(s).");
                constraintsMap.clear();
            }

            //////////////////////////////////////////////////////////////////
            // 0.0 <= sum_{b in B}(xmb) <= 1, for all m in Mb               //
            //////////////////////////////////////////////////////////////////
            {
                HashMap<String, HashMap<String, Double>> constraintsMap = new HashMap<String, HashMap<String, Double>>();
                //System.err.println("\n\nDEBUG: C3 = Adding " + totalNewConstraints + " new row(s).");
                for(UserEquipment ue : hashUE.values()){
                    if(ue.getListReachableCells().size() > 0){
                        HashMap<String, Double> row = new HashMap<String, Double>();
                        rowName = "assoc_" + ue.getId();

                        for(Cell c : ue.getListReachableCells()){
                            row.put("x_" + c.getId() + "_" + ue.getId(), 1.0);
                        }

                        if(row.size() > 0){
                            constraintsMap.put(rowName, row);
                        }
                    }
                }

                if(!constraintsMap.isEmpty()){
                    for(String rName : constraintsMap.keySet()){
                        HashMap<String, Double> row = constraintsMap.get(rName);
                        System.out.println(GlpkHelper.getGlpkConstraint(rName, row) + "<= 1");
                        writer.write(GlpkHelper.getGlpkConstraint(rName, row) + "<= 1\n");
                    }
                }
            }

            //System.err.println("DEBUG: C3 = Adding " + dbg_counter + " new constraint(s).");


            //////////////////////////////////////////////////////////////////
            // 0.0 <= sum_{m in Mb}(lmb*xmb) <= cb, for all b in B          //
            //////////////////////////////////////////////////////////////////
            {
                //System.err.println("\n\nDEBUG: C4: 0.0 <= sum_{m in Mb}(lmb*xmb) <= cb, for all b in B");
                HashMap<String, HashMap<String, Double>> constraintsMap = new HashMap<String, HashMap<String, Double>>();
                HashMap<String, Double> boundMap = new HashMap<String, Double>();

                for(Cell c : hashCell.values()){
                    if(c.getListReachableUE().size() > 0){
                        HashMap<String, Double> row = new HashMap<String, Double>();
                        rowName = "Capacity_" + c.getId();

                        for(UserEquipment ue : c.getListReachableUE()){
                            double lmb = (double) TransmissionLink.getRequiredMinNumPRBs(ue, c);
                            if(lmb != 0){
                                row.put("x_" + c.getId() + "_" + ue.getId(), lmb);
                                //System.err.println("DEBUG: " + rowName + " - colMap.get(" + c.getId() + " " + ue.getId() + ") = " + colMap.get(c.getId() + " " + ue.getId()) + "\t lmb = " + lmb);
                            }
                        }

                        if(row.size() > 0){
                            boundMap.put(rowName, c.getTotalDLCapacity(frequency));
                            constraintsMap.put(rowName, row);
                        }
                    }
                }

                if(!constraintsMap.isEmpty()){
                    for(String rName : constraintsMap.keySet()){
                        HashMap<String, Double> row = constraintsMap.get(rName);
                        System.out.println(GlpkHelper.getGlpkConstraint(rName, row) + "<= " + boundMap.get(rName));
                        writer.write(GlpkHelper.getGlpkConstraint(rName, row) + "<= " + boundMap.get(rName) + "\n");
                    }
                }
                //System.err.println("DEBUG: C4 = Adding " + dbg_counter + " new constraint(s).");
                constraintsMap.clear();
            }

            //System.err.println("DEBUG: C4 = Adding " + dbg_counter + " new constraint(s).");


            //////////////////////////////////////////////////////////////////
            // sum_{b in B}(sum_{m in Mb}(xmb)) >= alpha*|Mb|               //
            //////////////////////////////////////////////////////////////////
            {
                double alpha = minLoadPercentage;
                rowName = "QoS";
                //GLPK.glp_set_row_bnds(lp, rowIdx, GLPKConstants.GLP_LO, alpha*totalUe_Cell, Double.MAX_VALUE);
                HashMap<String, Double> row = new HashMap<String, Double>();
                for(Cell c : hashCell.values()){
                    int len = c.getListReachableUE().size();
                    if(c.getTotalDLCapacity(frequency) > 0 && len >= 1){
                        for(UserEquipment ue : c.getListReachableUE()){
                            row.put("x_" + c.getId() + "_" + ue.getId(), 1.0);
                        }
                    }
                }

                int totalUe = 0;
                for(UserEquipment ue : hashUE.values()){
                    if(ue.getListReachableCells().size() != 0){
                        totalUe++;
                    }
                }

                System.out.println(GlpkHelper.getGlpkConstraint(rowName, row) + ">= " + (alpha*totalUe));
                writer.write(GlpkHelper.getGlpkConstraint(rowName, row) + ">= " + (alpha*totalUe) + "\n");
            }

            writer.write("Bounds\n");
            writer.write("0 <= " + LmaxColName + " <= 1\n");
            writer.write("0 <= " + LminColName + " <= 1\n");

            String decisionVars = (isILP) ? "Binary\n" :  "";
            for(Cell c : hashCell.values()){
                for(UserEquipment ue : c.getListReachableUE()){
                    String xij ="x_" + c.getId() + "_" + ue.getId();
                    writer.write("0 <= "  + xij + " <= 1\n");
                    decisionVars += "\n" + xij;
                }
            }
            writer.write("Semi-continuous\n");
            writer.write(LmaxColName + "\n" + LminColName + "\n");
            writer.write(decisionVars + "\n");

            writer.write("End\n");

        } catch (IOException ex) {
            // report
        } finally {
            try {writer.close();} catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * ILP model without timeframe allocation 
     */
    public static void createILPModel(HashMap<String,UserEquipment> hashUE, HashMap<String,Cell> hashCell, double minLoadPercentage, String outFileName) {
        createLPModel(true, hashUE, hashCell, minLoadPercentage, outFileName);
    }


}
